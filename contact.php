<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
        <title>ContractComplete - Tendering & Contract Management Software</title>
		<?php
			include 'css_common.php';
		?>
    </head>
    <body>
        

		<?php
			include 'https_redirect.php';
			include 'header.html';
		?>
        
		
        <!--================Made Life Area =================-->
        <section class="made_life_area p_120" style="background-color:#f3f3f3;margin-top:100px">
        	<div class="container">
        		<div class="work_inner row large-screen-only" style="padding-bottom:25px">
					<div class="main_title" style="width:100%">
						<h2>We would love to hear from you</h2>
						<p>Feel free to reach out with your questions or concerns.  We are availably by phone between 9am and 5pm eastern time, and can be reached at info@contractcomplete.com.</p>
					</div>
					<iframe width="70%" style="margin:auto" height="600" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=151%20Charles%20Street%20W%2C%20Suite%20100+(ContractComplete)&amp;ie=UTF8&amp;t=&amp;z=17&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/coordinates.html">find my coordinates</a></iframe><br />
				</div>
						<div style="clear:both"></div>
        	</div>
			<div style="clear:both;height:180px" class="small-screen-only"></div>
        </section>
        <!--================End Made Life Area =================-->
		
        

		<?php
			include 'footer.php';
		?>
        
    </body>
</html>