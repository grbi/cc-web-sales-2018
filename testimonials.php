<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
		<link rel="stylesheet" href="css/font-awesome.min.css" />
        <title>ContractComplete - Testimonials</title>
		<?php
			include 'https_redirect.php';
			include 'css_common.php';
		?>
    </head>
    <body>

<?php 
	$safari = true;
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')) {
		$safari=false;
	}
	include 'header.html'; 
?>
        
        
		<?php
			include 'signup-prompt.php';
        ?>

        <!--================Home Banner Area =================-->
        <section class="banner_area">
			<div class="video-wrapper" style="position:absolute;top:0px;left:0px;width:100%;overflow:hidden;height:600px">
				<!--<img src="img/consultant-banner.jpeg" style="min-width:100%;min-height:600px;height:600px"></img>-->
			</div>
            <div class="banner_inner d-flex align-items-center">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container" style="padding-top:150px;height:600px">
					<div class="banner_content text_center consultants-banner" style="width:500px;float:left">
						<h2><b>Your success</b><br/>is our success.</h2><br/>
						<h3>
							We strive to make a difference in the way you<br/>work, and are happy to serve our clients on a<br/>daily basis by simplifying and expediting<br/>contract processes.<br/><br/>
							<b>Here's what our users have to say.</b>
						</h3>
						<a href="https://connect.contractcomplete.com/75-percent-off" class="tickets_btn_nav" style="margin-top:15px">Get Offer</a>
						<div class="small-screen-only" style="width:100%;clear:both;height:100px">&nbsp;</div>
					</div>
					<div   class="banner-image-section" style="max-width:50%;float:right;display:table-cell;vertical-align:middle;height:600px;padding-top:50px">
						<img src="img/Woman with laptop.<?php echo ($safari ?  'webp' : 'png'); ?>" style="border-radius:10px;margin-top: -60px;margin-left: -200px;box-shadow:none;-webkit-box-shadow:none;" class="home-banner-img top-banner-image"></img>
						<img src="img/open_quote.<?php echo ($safari ?  'webp' : 'png'); ?>" style="border-radius:10px;box-shadow:none;-webkit-box-shadow:none;margin-top: -700px;margin-left: 280px;" class="home-banner-img top-banner-image" />
						<img src="img/close_quote.<?php echo ($safari ?  'webp' : 'png'); ?>" style="border-radius:10px;box-shadow:none;-webkit-box-shadow:none;margin-top: -380px;" class="home-banner-img top-banner-image" />
						<div class="dot dot1" style="">
							&nbsp;
						</div>
					</div>
				</div>
            </div>
        </section>
		<div class="alert-banner" style="background-color:#0070bb">
			To help businesses during these challenging times, we are offering 3 months at 75% off.&nbsp;&nbsp;<a target="_blank" style="color:#C02942" href="https://connect.contractcomplete.com/75-percent-off"><u>Find out more</u></a>
		</div>
        

         <!--================Feature 2 Area =================-->
         <section class="made_life_area p_120" style="background-color:white">
         	<div class="container" style="text-align:center">
         		<div class="row made_life_inner" style="text-align:center">
					<div class="testimonial-spacer">&nbsp;</div>
					<div style="clear:both"></div>
					<?php include 'testimonial_adesso2.html'; ?>
					<div class="testimonial-spacer">&nbsp;</div>
					<?php include 'testimonial_aboud.html'; ?>
					<div class="testimonial-spacer">&nbsp;</div>
					<?php include 'testimonials_falcon.html'; ?>
					<div class="testimonial-spacer">&nbsp;</div>
					<?php include 'testimonial_adesso.html'; ?>
					<div class="testimonial-spacer">&nbsp;</div>
					<?php include 'testimonial_sbk.html'; ?>
					<div class="testimonial-spacer">&nbsp;</div>
					<?php include 'testimonial_geoscape.html'; ?>
					<div class="testimonial-spacer">&nbsp;</div>
					<?php include 'testimonial_torres.html'; ?>
         		</div>
         	</div>
         </section>
         <!--================End Feature 2 Area =================-->
		 
		 <?php
			include 'inline_signup.html';
		 ?>

<?php 

include 'common_scripts.html';
include 'footer.php'; 

?>
		 
		 <script type="text/javascript">
			var w = window;
			var loadIframe = function(){
				var vidDefer = document.getElementsByTagName('iframe');
				for (var i=0; i<vidDefer.length; i++) {
					if(vidDefer[i].getAttribute('data-src')) {
						vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
					} 
				} 
			}
			
			if (w.addEventListener) { w.addEventListener("load", loadIframe, false); }
			else if (w.attachEvent) { w.attachEvent("onload",loadIframe); }
		 </script>
		 