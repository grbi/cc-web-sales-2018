<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
        <title>ContractComplete - Tutorial &amp; Walkthrough Videos</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="vendors/linericon/style.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
        <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="vendors/animate-css/animate.css">
        <!-- main css -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="vendors/jquery-ui/jquery-ui.css">
    </head>
    <body>
        
		<?php 
			include 'header.html';
		?>
		
		
        <!--================Home Banner Area =================-->
        <section class="banner_area large-screen-only">
			<div class="item" style="display:table-cell;text-align:center;vertical-align:middle;height:600px;width:100vw;padding-top: 65px;background-color: #f3f3f3;">
					<div class="carousel-padded-item-alt" style="display:inline-block;max-width:500px;margin-right:60px;text-align:left;padding-top:100px">
						<h1>VIDEOS AND TUTORIALS</h1>
						<div class="carousel-subheader">Step by step. From tendering to payment certificates.</div><br/>
						<a href="https://connect.contractcomplete.com/get-a-demo" class="tickets_btn" style="width: 200px">Request a Demo Instead</a>
					</div>				
					<img src="img/youtube-cartoon.webp" style="display:inline-block; max-width:500px;max-height:500px;margin-top:-140px"></img>
			</div>
        </section>
        <section class="banner_area small-screen-only" style="width:100%">
			<div class="item" style="display:block;text-align:center;vertical-align:middle;height:600px;width:100%;padding-top:140px;background-color: #f3f3f3;">
					<div class="carousel-padded-item-alt" style="display:inline-block;max-width:100%;text-align:center;padding-top:100px">
						<img src="img/youtube-cartoon.webp" style="display:inline-block; max-width:80%;max-height:200px;margin-top:-80px"></img>
						<h1>VIDEOS</h1>
						<div class="carousel-subheader" style="text-align:center">Step by step. From tendering to payment certificates.</div><br/>
						<a href="https://connect.contractcomplete.com/get-a-demo" target="_blank" class="tickets_btn" style="width: 200px">Request a Demo Instead</a>
					</div>
			</div>
        </section>
        <!--================End Home Banner Area =================-->
        
        <!--================Work Area =================-->
        <section class="work_area p_120">
        	<div class="container">
        		<div class="main_title" style="padding-top:40px;">
        			<h2>The Basics</h2>
        		</div>
        		<div class="work_inner row" style="margin-bottom:10px">
        			<div class="col-lg-3 price_item video-wrapper">
						<article class="video-gallery-item price_footer">
							<figure>
								<a target="_blank" class="fancybox fancybox.iframe" href="//www.youtube.com/embed/_E5MPPJBxFk">
								<img class="videoThumb" src="//i1.ytimg.com/vi/_E5MPPJBxFk/mqdefault.jpg"></a>
							</figure>
							<h2 class="videoTitle">Requesting Help</h2>
							<p>This video shows how to get support in ContractComplete if you get stuck.</p>
						</article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
						<article class="video-gallery-item price_footer">
							<figure>
								<a target="_blank" class="fancybox fancybox.iframe" href="//www.youtube.com/embed/M4GJPqmyDJ4">
								<img class="videoThumb" src="//i1.ytimg.com/vi/M4GJPqmyDJ4/mqdefault.jpg"></a>
							</figure>
							<h2 class="videoTitle">Company Profile Setup</h2>
							<p>This video shows how to setup your company's profile in ContractComplete.</p>
						</article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
						<article class="video-gallery-item price_footer">
							<figure>
								<a target="_blank" class="fancybox fancybox.iframe" href="//www.youtube.com/embed/tYdq7F2nEs8">
								<img class="videoThumb" src="//i1.ytimg.com/vi/tYdq7F2nEs8/mqdefault.jpg"></a>
							</figure>
							<h2 class="videoTitle">Adding Users</h2>
							<p>This video shows how to add additional users to an existing ContractComplete account.</p>
						</article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
						<article class="video-gallery-item price_footer">
							<figure>
								<a target="_blank" class="fancybox fancybox.iframe" href="//www.youtube.com/embed/wKMMCzzVx6A">
								<img class="videoThumb" src="//i1.ytimg.com/vi/wKMMCzzVx6A/mqdefault.jpg"></a>
							</figure>
							<h2 class="videoTitle">The Action Box</h2>
							<p>See this video to learn about the ContractComplete action box and how it can help you.</p>
						</article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
						<article class="video-gallery-item price_footer">
							<figure>
								<a target="_blank" class="fancybox fancybox.iframe" href="//www.youtube.com/embed/M8gnkGuN-tY">
								<img class="videoThumb" src="//i1.ytimg.com/vi/M8gnkGuN-tY/mqdefault.jpg"></a>
							</figure>
							<h2 class="videoTitle">Your First Contract</h2>
							<p>This video shows how to set up a contract to be tendered, or one that has already been awarded.</p>
						</article>
        			</div>
        		</div>
				<div style="clear:both"></div>
			</div>
		</section>
		<section class="work_area p_120 stripe-odd">
			<div class="container">
        		<div class="main_title">
        			<h2>Contract Setup</h2>
        		</div>
        		<div class="work_inner row" style="margin-bottom:10px">
        			<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer consultant-focused">
						<img src="img/consultants.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/qBMn5pul8Zc">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/qBMn5pul8Zc/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Import a Tender Form</h2>
						<p>This video shows how to import a tender form from a spreadsheet.</p>
					  </article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer consultant-focused">
						<img src="img/consultants.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/itFYtmb73X4">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/itFYtmb73X4/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Using an Existing BoQ/Schedule of Prices</h2>
						<p>This video shows how to use the BoQ from another contract by cloning it.</p>
					  </article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer consultant-focused">
						<img src="img/consultants.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/BA7F6oT48oo">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/BA7F6oT48oo/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Adding Bidders</h2>
						<p>This video shows consultants and owners how to add bidders to their contracts prior to tendering</p>
					  </article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer consultant-focused">
						<img src="img/consultants.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/BvU4srAjUJM">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/BvU4srAjUJM/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Stakeholders and Consultants</h2>
						<p>This video shows consultants and owners how to add other stakeholders to their contracts</p>
					  </article>
        			</div>
				</div>
        		<div class="work_inner row" style="margin-bottom:10px">
        			<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer consultant-focused">
						<img src="img/consultants.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/BsQYQypx6ns">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/BsQYQypx6ns/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Preparing Documents Before Tendering</h2>
						<p>This video shows consultants and owners how to upload documents and request specific uploads from bidders</p>
					  </article>
        			</div>
        		</div>
				<div style="clear:both"></div>
			</div>
		</section>
		<section class="work_area p_120">
			<div class="container">
        		<div class="main_title">
        			<h2>The Bidding Process</h2>
        		</div>
        		<div class="work_inner row" style="margin-bottom:10px">
        			<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer consultant-focused">
						<img src="img/consultants.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/bkdtZVoCjys">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/bkdtZVoCjys/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Starting the Tendering Process</h2>
						<p>This video shows consultants and owners how to begin tendering a contract.</p>
					  </article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer consultant-focused">
						<img src="img/consultants.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/tVhD5OVYrtM">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/tVhD5OVYrtM/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Addenda &amp; Contract Changes</h2>
						<p>This video shows consultants and owners how to make changes during tendering.</p>
					  </article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer consultant-focused">
						<img src="img/consultants.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/cc-H8kYUG_A">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/cc-H8kYUG_A/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Addenda II: Addendum Builder Tool</h2>
						<p>This video shows consultants and owners how to use the new addendum builder feature!</p>
					  </article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer bidder-focused">
						<img src="img/bidders.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/RPV9kaBjde0">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/RPV9kaBjde0/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Opening a Bid for the First Time</h2>
						<p>This video shows bidders how to start their bid in ContractComplete</p>
					  </article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer bidder-focused">
						<img src="img/bidders.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/uSxMISF1E8E">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/uSxMISF1E8E/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Filling out the Tender Form</h2>
						<p>This video shows how to fill out the tender form for a new bid in ContractComplete.</p>
					  </article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer bidder-focused">
						<img src="img/bidders.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/YI_4w6MmON8">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/YI_4w6MmON8/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Uploading Requested Documents</h2>
						<p>This video shows bidders how to upload documents as part of their bid submission.</p>
					  </article>
        			</div>
        			<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer bidder-focused">
						<img src="img/bidders.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/Os6E3NI53iw">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/Os6E3NI53iw/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Acknowledging Addenda and Submitting your Bid</h2>
						<p>This video shows bidders how to acknowledged addenda and submit their bid in ContractComplete.</p>
					  </article>
        			</div>
				</div>
				<div style="clear:both;width:100%"></div>
			</div>
		</section>
		<section class="work_area p_120 stripe-odd">
			<div class="container">
				<div class="main_title" style="margin:auto">
					<h2>Post Tendering</h2>
				</div>
        		<div class="work_inner row" style="margin-bottom:10px">
					<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer consultant-focused">
						<img src="img/consultants.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/wQIQZg3tA6Y">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/wQIQZg3tA6Y/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Handling Incomplete Submissions</h2>
						<p>This tutorial covers the various ways to deal with incomplete bid submissions in ContractComplete.</p>
					  </article>
					</div>
					<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer consultant-focused">
						<img src="img/consultants.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/aLj_zQXEDbM">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/aLj_zQXEDbM/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Reviewing Bids</h2>
						<p>This video covers bid review in ContractComplete.</p>
					  </article>
					</div>
					<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer consultant-focused">
						<img src="img/consultants.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/f97FgELVBTY">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/f97FgELVBTY/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Post-Tender Contract Negotiation</h2>
						<p>This video covers negotiation on scope, pricing, and quantity after tendering has complete.</p>
					  </article>
					</div>
				</div>
				<div style="clear:both;width:100%"></div>
			</div>
		</section>
		<section class="work_area p_120">
			<div class="container">
				<div class="main_title" style="margin:auto">
					<h2>Awarded Contracts</h2>
				</div>
        		<div class="work_inner row" style="margin-bottom:10px">
					<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer price_footer">
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/jy4_gehQkjs">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/jy4_gehQkjs/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Change Orders</h2>
						<p>This video covers proposing a change in ContractComplete as well as creating a formal change order.</p>
					  </article>
					</div>
					<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer contractor-focused">
						<img src="img/contractors.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/4ppu_tgT-9U">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/4ppu_tgT-9U/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Invoicing  Part 1 - Reporting Progress</h2>
						<p>This video covers creating a progress report in ContractComplete, which is a prerequisite for invoicing.</p>
					  </article>
					</div>
					<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer contractor-focused">
						<img src="img/contractors.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/a1RPpM9o1lA">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/a1RPpM9o1lA/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">ContractComplete Tutorial: Invoicing  Part 2 - Progress Payments</h2>
						<p>This tutorial covers invoicing for progress payments.</p>
					  </article>
					</div>
					<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer contractor-focused">
						<img src="img/contractors.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/A8YK3h75ZJA">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/A8YK3h75ZJA/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Invoicing  Part 3 - Holdback Invoicing</h2>
						<p>This tutorial covers how to request the release of one or more holdbacks in ContractComplete.</p>
					  </article>
					</div>
					<div class="col-lg-3 price_item video-wrapper">
					  <article class="video-gallery-item price_footer">
						<img src="img/consultants.webp" class="target-focus"/>
						<figure>
						  <a class="fancybox fancybox.iframe" href="//www.youtube.com/embed/tMaPwht5GaM">
						  <img class="videoThumb" src="//i1.ytimg.com/vi/tMaPwht5GaM/mqdefault.jpg"></a>
						</figure>
						<h2 class="videoTitle">Payment Certificates</h2>
						<p>This tutorial covers how to approve and invoice and issue a payment certificate.</p>
					  </article>
					</div>
				</div>
        	</div>
        </section>
        <!--================End Work Area =================-->
        
		<?php
			include 'signup-prompt.php';
        ?>
	   
		<?php
			include 'footer.php';
		?>
        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/stellar.js"></script>
        <script src="vendors/lightbox/simpleLightbox.min.js"></script>
        <script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="vendors/isotope/isotope-min.js"></script>
        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="js/mail-script.js"></script>
        <script src="vendors/counter-up/jquery.waypoints.min.js"></script>
        <script src="vendors/counter-up/jquery.counterup.min.js"></script>
        <script src="js/theme.js"></script>
        <script src="vendors/jquery-ui/jquery-ui.js"></script>
		<?php
			include 'common_scripts.html';
		?>
    </body>
</html>