<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
		<meta name="description" content="Fast, Streamlined Bidding for Unit Price Contracts. Administer your unit price contracts like a pro with streamlined bidding, addenda, change orders, RFIs, submittals, invoicing, and more!  See how we take the stress out of your day." >
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
		<link rel="stylesheet" href="css/font-awesome.min.css" />
        <title>ContractComplete - Pricing</title>
		<?php
			include 'https_redirect.php';
			include 'css_common.php';
		?>
    </head>
    <body>

<?php 
	$safari = true;
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')) {
		$safari=false;
	}
	include 'header.html'; 
?>
        
		

		
		
	
	
        
		<?php
			include 'signup-prompt.php';
        ?>

        <!--================Home Banner Area =================-->
        <section class="banner_area">
			<div class="video-wrapper" style="position:absolute;top:0px;left:0px;width:100%;overflow:hidden;height:600px">
				<!--<img src="img/consultant-banner.jpeg" style="min-width:100%;min-height:600px;height:600px"></img>-->
			</div>
            <div class="banner_inner d-flex align-items-center pricing-hero">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container" style="padding-top:100px;height:600px">
					<div class="banner_content text_center consultants-banner pricing-text" style="width:500px;float:left">
						<div> <!-- just a wrapper for flex layout -->
							<div class="small-screen-only" style="width:100%;clear:both;height:125px">&nbsp;</div>
							<h2>Finally a Contract Admin<br/> Tool with <b/>Fair Pricing</b></h2><br/>
							<h3>
								All ContractComplete monthly plans come with unlimited contracts!
							</h3>
							<a href="https://connect.contractcomplete.com/get-a-demo" target="_blank" class="tickets_btn_nav" style="margin-top:16px">I'm Interested</a>
							<div class="small-screen-only" style="width:100%;clear:both;height:100px">&nbsp;</div>
						</div>
					</div>
					<div   class="banner-image-section" style="max-width:50%;float:right;display:table-cell;vertical-align:middle;height:600px;padding-top:50px">
						<img src="img/pricing-construction-worker.png" style="border-radius:10px;box-shadow:none;-webkit-box-shadow:none;height:100%" class="home-banner-img top-banner-image"></img>
						<div class="dot dot1" style="left:100px;margin-top: -400px;">
							&nbsp;
						</div>
						<div class="rating top-banner-image">
							<div  class="rating-stars" style="color:#e7ae4c">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
							<div>
								By Capterra <a target="_blank" href="https://www.capterra.com/p/166394/ContractComplete/"><i class="fa fa-location-arrow" style="font-size: 24px;color:#D95B43"></i></a>
							</div>
						</div>
					</div>
				</div>
            </div>
        </section>
		<div class="alert-banner">
			Did you know ContractComplete is Free for Bidders and Collaborating Stakeholders?  <a target="_blank" style="color:#C02942" href="/faq.php"><u>Find out more</u></a>.
		</div>

		 <!-- Pricing area -->
         <section class="made_life_area p_120" style="padding-bottom:40px;padding-top:40px;background-color:white;text-align:center">
			<h1 style="color:#0070bb">Perfect Pricing for your Business</h1>
			<h2 style="color:black;font-size: 20px;">No hidden fees.  All subscription plans are <b style="color:black">billed monthly</b>.</h2>
			<div class="center-wrapper" style="margin-top:30px;max-width: 1200px;">
				<font class="usd-info">Pricing in USD</font>
				<div class="pricing-block first">
					<div class="pricing-title" style="background:url('img/gray price.<?php echo ($safari ?  'webp' : 'png'); ?>');">
						<font class="cc">ContractComplete</font><br/>Studio
					</div>
					<p class="pricing-description" style="color:#999999">For smaller firms<br/>looking to gain a<br/>competitive edge</p>
					<font style="display:block" class="pricing-buy">$199/mo</font>
					<div class="pricing-info">
						INCLUDES:<br/>
						1 Pro User<br/>
						1 Read Only User
					</div>
					<div class="pricing-buy">
						<a href="https://connect.contractcomplete.com/get-a-demo" target="_blank" class="tickets_btn_nav grey-nav-button" style="margin-top:16px">I'm Interested</a>
					</div>
				</div>
				<div class="pricing-block second">
					<div class="pricing-title" style="background:url('img/blue price bg.<?php echo ($safari ?  'webp' : 'png'); ?>');">
						<font class="cc">ContractComplete</font><br/>Team
					</div>
					<p class="pricing-description" style="color:#0070bb">For medium sized<br/>companies focusing on<br/>top line earnings</p>
					<font style="display:block" class="pricing-buy">$249/mo</font>
					<div class="pricing-info">
						INCLUDES:<br/>
						3 Pro Users<br/>
						3 Read Only Users
					</div>
					<div class="pricing-buy">
						<a href="https://connect.contractcomplete.com/get-a-demo" target="_blank" class="tickets_btn_nav" style="margin-top:16px">I'm Interested</a>
					</div>
				</div>
				<div class="pricing-block third">
					<div class="pricing-title" style="background:url('img/green price bg.<?php echo ($safari ?  'webp' : 'png'); ?>');">
						<font class="cc">ContractComplete</font><br/>Enterprise
					</div>
					<p class="pricing-description" style="color:#019694">For established firms<br/>looking to improve<br/>collaboration accross<br/>large teams.</p>
					<font style="display:block" class="pricing-buy">$549/mo</font>
					<div class="pricing-info">
						INCLUDES:<br/>
						10 Pro Users<br/>
						10 Read Only Users
					</div>
					<div class="pricing-buy">
						<a  href="https://connect.contractcomplete.com/get-a-demo" target="_blank" class="tickets_btn_nav green-nav-button" style="margin-top:16px">I'm Interested</a>
					</div>
				</div>
				<div class="pricing-block last">
					<div class="pricing-title">
						Extra Users
					</div>
					<p class="pricing-description" style="margin-top: -70px;"><font style="font-size: 24px;color: #019694;font-weight: 400;line-height: 26px;">Need<br/>something<br/>in between?</font><br/><br/>No problem.<br/>Pick a plan, then<br/>add users.</p>
					<div class="pricing-buy">
						<font style="display: block;margin-top: 70px;">$39/mo</font>
					</div>
					<div class="pricing-info">
						Add standard<br/>users for <b style="color:black">$39/mo</b><br/><br/>
						Add read only<br/>users for <b style="color:black">$10/mo</b>
					</div>
				</div>
			</div>
			
			<div style="clear:both">&nbsp;</div>
			<section class="made_life_area p_120" style="padding-bottom:40px;width:100%;padding-top:40px;background-color:white;text-align:center">
				<div class="pricing-block" style="width:auto;display:inline-block;float:none">
					<div class="pricing-title" style="color: black;background-color: #deecf6;">
						Pay Per Contract
					</div>
					<p class="pricing-description" style="margin-top: -70px;"><font style="font-size: 24px;color: #019694;font-weight: 400;line-height: 26px;">Not Administering<br/>Enough Contracts?</font><br/><br/>No problem.<br/>We have a plan<br/>for you.</p>
					<div class="pricing-buy">
						<font style="display: block;margin-top: 70px;">$495/per contract</font>
					</div>
					<div class="pricing-info">
						Includes all features included in the montly plan<br/>(up to $5,000,000 contract value)
					</div>
					<div class="pricing-buy">
						<a href="https://connect.contractcomplete.com/get-a-demo" target="_blank" class="tickets_btn_nav grey-nav-button" style="margin-top:16px">I'm Interested</a>
					</div>
				</div>
			</section>
			
			<div style="clear:both">&nbsp;</div>
			<div style="height:24px"></div>
		<section class="made_life_area p_120" style="clear: both;padding-bottom:50px;text-align:center">
		
				<h2 style="font-size: 40px;margin: 20px;">What You're Getting:</h2>
				<!-- Flickity HTML init -->
			<div class="gallery js-flickity small-screen-only"
			  data-flickity-options='{ "wrapAround": true }'>
			  <div class="gallery-cell" style="width:100%">
					<table class="stress-table table-responsive" style="margin-top:80px;padding-left:20px;padding-right:20px">
						<tr class="img-row">
							<td style="text-align:center"><img alt="Budgeting graphic" src="img/asset1.png" /></td>
						</tr>
						<tr class="img-row">
							<td style="height: 50px;">&nbsp;</td>
						</tr>
						<tr class="title-row">
							<td style="padding-left:30px;padding-right:30px;text-align:center"><b>UNLIMITED CONTRACTS<br/>&amp; DOCUMENTS</b></td>
						</tr>
						<tr class="descriptions-row">
							<td>On all monthly subscription plans.  Because the more you use ContractComplete, the more you save.</td>
						</tr>
					</table>
			  </div>
			  <div class="gallery-cell" style="width:100%">
					<table class="stress-table table-responsive" style="margin-top:80px;padding-left:20px;padding-right:20px">
						<tr class="img-row">
							<td style="text-align:center"><img  alt="Bidding graphic" src="img/asset3.png" /></td>
						</tr>
						<tr class="img-row">
							<td style="height: 50px;">&nbsp;</td>
						</tr>
						<tr class="title-row">
							<td style="padding-left:30px;padding-right:30px;text-align:center"><b>FULL ACCESS<br/>TO ALL FEATURES</b></td>
						</tr>
						<tr class="descriptions-row">
							<td>Includes all <a href="budgeting.php">Budgeting</a>, <a href="bidding.php">Bidding</a>, <a href="projectmanagement.php">contract admin</a> and <a href="progressbilling.php">progress billing</a> features.</td>
						</tr>
					</table>
			  </div>
			  <div class="gallery-cell" style="width:100%">
					<table class="stress-table table-responsive" style="margin-top:80px;padding-left:20px;padding-right:20px">
						<tr class="img-row">
							<td style="text-align:center"><img  alt="Contract admin graphic" src="img/asset4.png" /></td>
						</tr>
						<tr class="img-row">
							<td style="height: 50px;">&nbsp;</td>
						</tr>
						<tr class="title-row">
							<td style="padding-left:30px;padding-right:30px;text-align:center"><b>UNLIMITED SUPPORT<Br/>WHEN NEEDED</b></td>
						</tr>
						<tr class="descriptions-row">
							<td>Because we want our customers to win big with ContractComplete.</td>
						</tr>
					</table>
			  </div>
			</div>
			<div style="text-align: center;margin: auto" class="large-screen-only">
				<table class="stress-table table-responsive" style="margin-top:0px;padding-left:100px;padding-right:100px">
					<tr class="img-row">
						<td><img  alt="Budgeting graphic" src="img/asset1.png" /></td>
						<td><img  alt="Bidding graphic" src="img/asset3.png" /></td>
						<td><img  alt="Project management graphic" src="img/asset4.png" /></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr class="title-row">
						<td style="padding-left:30px;padding-right:30px"><b>UNLIMITED CONTRACTS<br/>&amp; DOCUMENTS</b></td>
						<td><b>FULL ACCESS<br/>TO ALL FEATURES</b></td>
						<td><b>UNLIMITED SUPPORT<Br/>WHEN NEEDED</b></td>
					</tr>
					<tr class="descriptions-row" style="font-size: 19px">
						<td>On all monthly subscription plans.  Because the more you use ContractComplete, the more you save.</td>
						<td>Includes all <a href="budgeting.php">Budgeting</a>, <a href="bidding.php">Bidding</a>, <a href="projectmanagement.php">contract admin</a> and <a href="progressbilling.php">progress billing</a> features.</td>
						<td>Because we want our customers to win big with ContractComplete.</td>
					</tr>
				</table>
			</div>
		</section>
			
			<div style="clear:both">&nbsp;</div>
			<div style="height:24px"></div>
			<h2 style="color:#0070bb;margin-bottom: 30px;">Pricing FAQ</h2>
			<div class="pricing-faq">
				<p class="pricing-question">Who should purchase ContractComplete?</p>
				<p class="pricing-answer">ContractComplete is for both contract administrators (civil engineers, landscape architects, prime consultants) and contractors working with unit price contracts.  The value delivered is different depending on which role best describes your company, but ContractComplete is a flexible tool benefiting all parties in the value chain.</p>
				<p class="pricing-question">Are there any additional fees not mentioned above?</p>
				<p class="pricing-answer">No.  Integrity and transparency are core values for us.  We will not charge you beyond your monthly subscription fee (should you choose a subscription), or the flat, per contract fee (Should you choose to purchase per contract).</p>
				<p class="pricing-question">Which plan is appropriate for me?</p>
				<p class="pricing-answer">A per contract plan is only economical for customers managing a small number of contracts per year (for example 2 or 3).  For a higher number of contracts, a monthly plan is recommended.  Many of our low volume users enjoy using ContractComplete enough that they quickly move as many of their contracts as possible into the system.</p>
				<p class="pricing-question">I'm a contract administrator.  Do my contractors have to pay anything to collaborate on ContractComplete?</p>
				<p class="pricing-answer">Absolutely not!  As a paying ContractComplete customer you can add bidders and contractors to work with you on your contracts at no extra cost.</p>
				<p class="pricing-question">Does ContractComplete offer a free trial?</p>
				<p class="pricing-answer">Yes!  Trials are available on request.  We will work with you to ensure you get the most benefit possible out of your free trial.</p>
				<p class="pricing-question">Do I need to pay for training?</p>
				<p class="pricing-answer">ContractComplete is designed to be as easy as possible to use and requires little to no training beyond some initial guidance. That said, training and support is provided free of charge where it is required.</p>
				<p class="pricing-question">What is the payback period on my investment?</p>
				<p class="pricing-answer">This depends on how many contracts you are using ContractComplete to manage.  However the vast majority of new customers say they experience a significant return immediately after their first bid.</p>
				<p class="pricing-question">How long does onboarding take?</p>
				<p class="pricing-answer">That depends entirely on your schedule.  User experience is one of our top priorities and ContractComplete is very easy to learn.  So typically, we can complete onboarding with a single session lasting less than one hour. For more information about onboarding, please see <a target="_blank" href="http://localhost:8080/cc-sales/faq.php">our FAQ</a>.</p>
			</div>
			<div style="clear:both">&nbsp;</div>
         </section>
		 
		<?php
			include 'common_scripts.html';
		?>
		 
		 <?php
			include 'inline_signup.html';
		 ?>

<?php include 'footer.php'; ?>
		 
		 <script type="text/javascript">
			var w = window;
			var loadIframe = function(){
				var vidDefer = document.getElementsByTagName('iframe');
				for (var i=0; i<vidDefer.length; i++) {
					if(vidDefer[i].getAttribute('data-src')) {
						vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
					} 
				} 
			}
			
			if (w.addEventListener) { w.addEventListener("load", loadIframe, false); }
			else if (w.attachEvent) { w.attachEvent("onload",loadIframe); }
		 </script>
		 