<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
		<link rel="stylesheet" href="css/font-awesome.min.css" />
        <title>ContractComplete - SGM Case Study</title>
		<?php
			include 'https_redirect.php';
			include 'css_common.php';
		?>
    </head>
    <body>

<?php 
	$safari = true;
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')) {
		$safari=false;
	}
	include 'header.html'; 
?>
        
        
		<?php
			include 'signup-prompt.php';
        ?>

		<div style="clear:both;height:150px"></div>
		<div class="case-study-wrapper">
			<div class="case-study-title-block">
				<img src="img/sgm_glenwood.jpg" style="max-width: 100%;" />
				<div style="display: inline-block;text-align:left;padding:20px;color:white">
					<h1 style="color:white">SGM</h1>
					<h2>Case Study</h2>
					<font style="font-size:24px">&quot;Bid reviews in minutes - not days.&quot;</font><br/>
					<div style="width:100%;text-align:right">
						<font style="color:black">Jason Reimer, P. Engineer</font>
					</div>
				</div>
			</div>
			<div style="clear:both;height:75px"></div>
			<div class="case-study-column col-left">
				<h2>Overview</h2>
				<p>
As a 125-person civil engineering firm with offices across Western
Colorado, SGM needed a contract and bidding software solution that
was powerful enough to handle the largest bidding jobs but easy
enough to integrate efficiently. Prior to ContractComplete, SGM’s
bidding and tendering process was manual and time-consuming,
relying on plan rooms which were difficult to manage.
SGM immediately saw a huge difference in efficiency and ease of use
with ContractComplete.
				</p>
				<p>
The following is a Case Study as to how ContractComplete is well on
its way to becoming the key tool in SGM’s contract and bidding
administration process.
				</p>
				<ul class="cb_list">
					<li>
ContractComplete provides contract and bidding automation
software for the Architect-Engineer-Construction (AEC) industry
with a distinct focus on unit price contracts.
					</li>
					<li>
ContractComplete is used by companies across Canada, United
States and Australia.
					</li>
					<li>
ContractComplete users have stated they save roughly 3-4 full
business days or $4,000 per month.
					</li>
					<li>
ContractComplete works with both the public and private sector
and has the highest available security platforms.
					</li>
				</ul>
			</div>
			<div class="case-study-column col-right">
				<h2>About <img src="img/sgm-logo.png" style="height: 30px; margin-top: -10px;" alt="SGM Logo" /></h2>
				<p>
SGM is a full-service engineering, consulting and surveying firm who
has served public and private sector clients across of Western
Colorado for 35 years. SGM is a successful company of experienced
engineers and surveyors who believe in integrity and professionalism.
Their employees bring diversity and fresh ideas to their fast-paced
environment, and are dedicated to providing the best engineering and
surveying services to municipalities, the public sector, residential and
commercial developments throughout Western and Southwestern
Colorado.
				</p>
				<p class="case-study-accent">
SGM engineers specialize in drinking water and wastewater,
structural, and municipal services, in addition to construction
and land development services for both public and private
sectors. SGM prides itself on being a top tier civil engineering
company in terms of quality, client commitment and pride of
workman-ship.
				</p>
				<p>
<i>They have over 100 team members and growing!</i>
				</p>
				<br/><br/><br/>
			</div>
			<hr>
			<div class="case-study-column col-left">
				<h2>The Challenge</h2>
				<p>
<img src="img/jason_photo.png" style="float: right; padding-left: 20px;" alt="Jason" />
Jason Reimer, a Professional
Engineer with SGM, was on the hunt
to find a system to complement SGM’s
current tech stack. SGM has streamlined
design and document management;
with systems in place, but their
bidding processes were manual and
time consuming.
				</p>
				<p>
SGM needed a contract and bidding management software solution
that could automate their manual processes. As is common in the
AEC industry, bidding phase budgets are stretched too thin and often
exceeded due to contractor coordination and clarifications resulting in
addenda. All these items provide value to the development of a
project, but take time. Time is money.
				</p>
				<p>
Jason was looking for software that was packed with features and
automations, but ease of use was also critically important. He had
looked at many other systems, but they just didn’t do what he was
looking for.
				</p>
				<table class="challenge">
					<tr>
						<td>
							<img src="img/challenge1.png" alt="Challenge 1" />
						</td>
						<td>
							<h3>Challenge 1: Getting Enough Bidders</h3>
							<p>
It’s always difficult to know who will actually submit a bid
making it difficult to ensure clients receive enough bids.
							</p>
						</td>
					</tr>
				</table>
				<table class="challenge">
					<tr>
						<td>
							<img src="img/challenge2.png" alt="Challenge 2" />
						</td>
						<td>
							<h3>Challenge 2: Going Over Budgeted Time</h3>
							<p>
The bidding process always seems to take longer than
expected with all the bidder questions and addenda
involved.

							</p>
						</td>
					</tr>
				</table>
				<table class="challenge">
					<tr>
						<td>
							<img src="img/challenge3.png" alt="Challenge 3" />
						</td>
						<td>
							<h3>Challenge 3: Time Consuming Paperwork</h3>
							<p>
The bid review process, change orders and payment
applications are time consuming, manual and tedious.
							</p>
						</td>
					</tr>
				</table>
				<p>
Overall, SGM wanted to implement a software solution that could save
them time, money, and resources. ContractComplete was able to
meet their needs and is providing excellent results month after month.
				</p>
				<p class="case-study-accent">
SGM looks forward to continuing to use ContractComplete and
using more and more features in the near future.
				</p>
				<q>
When you do the math, this is a no-brainer.
There is no other lower hanging fruit in the
design-bid-build process to save money and
time.
				</q>
			</div>
			<div class="case-study-column col-right">
				<div class="case-study-accent-grey">
					<h2>The First Project</h2>
					<p>
&quot;The first project with ContractComplete was a complete
success. I was able to rebid a project with over 90 line items,
and after the deadline easily review and compare multiple
contractor’s prices in less than 15 minutes, including final
highs, lows, averages with a click of a button. In this
industry, we are constantly looking for cost-effective and
time saving tools to do more with less time and energy.
It typically takes us a day or two to turn around bid results,
but with ContractComplete, you can do it all in less than 15
minutes.</font>&quot;

					</p>
					<table class="challenge">
						<tr>
							<td style="padding: 6px;padding-right: 24px;">
								<div class="number-circle" style="background-color:#0aa89f">1</div>
							</td>
							<td>
								<h3>The Bidding Process</h3>
								<p>
Jason was able to see which bidders were
engaged, easily handle questions and issue
addenda to answer questions almost
instantaneously.
								</p>
							</td>
						</tr>
					</table>

					<table class="challenge">
						<tr>
							<td style="padding: 6px;padding-right: 24px;">
								<div class="number-circle" style="background-color:black">2</div>
							</td>
							<td>
								<h3>Bid Review</h3>
								<p>
A process that usually takes 8-12 hours took
Jason 15 minutes, including highs, lows and
average prices.
								</p>
							</td>
						</tr>
					</table>

					<table class="challenge">
						<tr>
							<td style="padding: 6px;padding-right: 24px;">
								<div class="number-circle" style="background-color:grey">3</div>
							</td>
							<td>
								<h3>Contract Admin</h3>
								<p>
Jason’s first contract was easily awarded to the
low bidder. Even the most “old school”
contractors were able to submit bids online. The
flexibility of the tools allows you to still manage
the process offline, import documents later, and
provide contract document tracking from start to
finish.
								</p>
							</td>
						</tr>
					</table>
					<p>
With over $5000 saved in just the bidding phase, there is
value that can be added back into the design phase or
moved into the construction administration phase to provide
clients more bang for their buck.
					</p>
				</div>
				<div class="case-study-accent-green" style="margin-top:24px">
					<h2>Future Plans</h2>
					<p>
Jason’s experience using ContractComplete was so positive,
he plans to incorporate the contract bidding and automation
software into the workflow of the firm’s branches across
Colorado as soon as possible. With the ability to set multiple
company divisions within one account, SGM principals can
have full transparency across all of their projects. They can
easily see how projects are progressing, from bidding stage
all the way through to the final payment app.
					</p>
					<ul class="cb_list_white">
						<li>
Setup Company Divisions for all 7 offices, allowing
Principals to oversee bidding and contract activities.
						</li>
						<li>
Invite SGM Project Managers, Clients and Bidders to
collaborate. Increasing company wide transparency.
						</li>
						<li>
Use historical contract prices to facilitate construction
budgets in the future.
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div style="clear:both"></div>
         <!--================End Feature 2 Area =================-->
		 
		 <?php
			include 'inline_signup.html';
		 ?>

<?php 

include 'common_scripts.html';
include 'footer.php'; 

?>
		 
		 <script type="text/javascript">
			var w = window;
			var loadIframe = function(){
				var vidDefer = document.getElementsByTagName('iframe');
				for (var i=0; i<vidDefer.length; i++) {
					if(vidDefer[i].getAttribute('data-src')) {
						vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
					} 
				} 
			}
			
			if (w.addEventListener) { w.addEventListener("load", loadIframe, false); }
			else if (w.attachEvent) { w.attachEvent("onload",loadIframe); }
		 </script>
		 