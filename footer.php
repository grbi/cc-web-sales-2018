<?php 
	$safari = true;
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')) {
		$safari=false;
	}
?>

        <div id="mask" style="display:none"></div>
		<!--
		<div style="width:100%;text-align:center;position:absolute;pointer-events:none">
			<img style="margin-top: -280px;margin-left:auto;margin-right:auto;max-width:100%" src="img/big_circle_arrow.<?php echo ($safari ?  'webp' : 'png'); ?>" />
		</div>
		-->
		<!--================Footer Area =================-->
        <footer class="footer_area" style="">
        	<div class="container">
        		<div class="row footer_inner">
        			<div class="col-lg-3 col-sm-6">
        				<img style="height:100px" src="img/logo.<?php echo ($safari ?  'webp' : 'png'); ?>" alt="">
        			</div>
        			<div class="col-lg-3">
        				<aside class="f_widget ab_widget">
        					<div class="f_title">
        						<h3>Contact Us</h3>
        					</div>
        					<p>ContractComplete<br/>295 Hagey Blvd<br/>Waterloo, Ontario N2L 6R5<br/>1 800 515 1005<br/>support@contractcomplete.com</p>

        				</aside>
        			</div>
					<div class="col-lg-3">
						<div style="width: 300px;position: relative;" class="footer-map"><iframe width="220" height="220" src="https://maps.google.com/maps?width=220&amp;height=220&amp;hl=en&amp;q=295%20Hagey%20Blvd%2C%20waterloo+(ContractComplete)&amp;ie=UTF8&amp;t=&amp;z=10&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><div style="position: absolute;width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div><br />
					</div>
        			<!--<div class="col-lg-5 col-sm-6">
        				<aside class="f_widget news_widget">
        					<div class="f_title">
        						<h3>Request a Demo</h3>
        					</div>
        					<p>We'd love to hear from you</p>
        					<div id="mc_embed_signup">
								<!-- https://api.contractcomplete.com/api/util/demorequest2
                                <form class="subscribe_form relative" id="requst-demo-form"  onsubmit="return requestDemo();">
                                	<div class="input-group d-flex flex-row">
                                        <input id="request-demo-email" name="EMAIL" placeholder="Enter email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
                                        <button class="btn sub-btn"><span class="lnr lnr-arrow-right"></span></button>		
                                    </div>				
                                    <div class="mt-10 info"></div>
                                </form>
                            </div>
        				</aside>
        			</div>-->
        			<div class="col-lg-3">
        				<aside class="f_widget social_widget">
        					<div class="f_title">
        						<h3>Follow Us</h3>
        					</div>
        					<p>Check us out on social media</p>
        					<ul class="list">
        						<li><a target="_blank" href="https://www.facebook.com/contractcomplete"><i class="fa fa-facebook"></i></a></li>
        						<li><a target="_blank" href="https://twitter.com/contractcmplete"><i class="fa fa-twitter"></i></a></li>
        						<li><a target="_blank" href="https://www.linkedin.com/company/contractcomplete/"><i class="fa fa-linkedin"></i></a></li>
        						<li><a target="_blank" href="http://www.instagram.com/contractcomplete"><i class="fa fa-instagram"></i></a></li>
        					</ul>
        				</aside><br/><br/>
        					<div class="f_title">
        						<h3><a style="color:#1b6b76;" target="_blank" href="privacy_policy.html">Terms of Service</a></h3>
								<a style="color:#1b6b76;" href="about.php">About Us</a>
        					</div>
        					<div style="clear:both"></div>
        			</div>
        		</div>
        	</div>
        </footer>
        	<p style="display: block;margin: auto;width: 100%;text-align: center;padding: 22px;margin-top: 20px;color: black;"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				Copyright &copy; <?php echo date("Y"); ?></script> ContractComplete. All rights reserved.
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
			<div style="clear:both">&nbsp;</div>
<?php
$nochat = '0';
if(array_key_exists ('nochat', $_GET)) $nochat = $_GET['nochat'];
 if ($nochat != '1'){
?>
		
<!-- Start of HubSpot Embed Code -->
  <script type="text/javascript">
			
			
		  (function(w, d, s) {
			function go(){
			  var js, fjs = d.getElementsByTagName(s)[0], load = function(url, id) {
			  if (d.getElementById(id)) {return;}
			  js = d.createElement(s); js.src = url; js.id = id;
			  fjs.parentNode.insertBefore(js, fjs);
			};
			  load('//js.hs-scripts.com/2095312.js', 'hs-script-loader');
			}
			if (w.addEventListener) { w.addEventListener("load", go, false); }
			else if (w.attachEvent) { w.attachEvent("onload",go); }
		  }(window, document, 'script'));
</script>
<!-- End of HubSpot Embed Code -->
        <!--================End Footer Area =================-->
<?php
 }
?>
