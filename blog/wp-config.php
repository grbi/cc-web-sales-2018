<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress2');

/** MySQL database username */
define('DB_USER', 'wordpress_user');

/** MySQL database password */
define('DB_PASSWORD', 'c0Ntract');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'UrmEt17pxX3MoxspylcyxR]=6Ld~(N<}GOL1]p,)6LXG}/w9.SiS;P]7,g{IWV>&');
define('SECURE_AUTH_KEY',  'A0 c/|F`@q41RE4gNK+T>*|Z[phjhQhz5g7A8:[8kOzeQek(YMoqhSROCQQCD8]3');
define('LOGGED_IN_KEY',    'c!4>j1/2qMQcM[?_*#x-Ro}k8,tM rfDlkM|xgx(;zgXyE4yB .E| &YTUBMv@jE');
define('NONCE_KEY',        'uV?WPt*f|pmck&Zsu;0@qX07b@4o{pvvt1xWtSiF#=@#w ^GxXpl=C0-Z^vU$Esz');
define('AUTH_SALT',        'VF(.L;#I?2s 6]4Mfn$Z>!4p1cMC(0wfbvv-vACX;Rp2X.u@_=xNP`vN^aFQ=@XB');
define('SECURE_AUTH_SALT', 'X&1G{Ri:GQS3]YQ(:T3z=z2|IW2,%nsU97[jgKs M1Q65>mq/P;NX7:>5E%1;r}Z');
define('LOGGED_IN_SALT',   '~|[V>B%NO%!$H|:e1jjfb$! GU~5M?mi;yv4P2xV!VeX,D5CIR)rc-DP#^xt9uB:');
define('NONCE_SALT',       'Lj#Uol@TB#1/s7^;:twSR Rl>t@7b&]CC<ifZiHnE]gW:]`);.E/@TIu{Do}T !E');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
