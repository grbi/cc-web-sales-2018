<?php
/**
 * The main template file.
 *
 * @package VT Blogging Pro
 */

get_header(); ?>

	<div id="primary" class="content-area clear">
			
		<?php get_template_part('template-parts/featured', 'slider'); ?>
		
		<main id="main" class="site-main clear wow fadeInUp" data-wow-duration=".8s" data-wow-delay=".2s">

			<div id="recent-content" class="content-loop">

				<?php if ( get_theme_mod( 'blog-page-layout' ) == 'layout-1' ) :

						get_template_part( 'layouts/blog', 'list-layout-1' ); 

					elseif ( get_theme_mod( 'blog-page-layout' ) == 'layout-2' ): 

						get_template_part( 'layouts/blog', 'list-layout-2' );
						
					elseif ( get_theme_mod( 'blog-page-layout' ) == 'standard-list' ): 

						get_template_part( 'layouts/blog', 'standard-list' );
						
					elseif ( get_theme_mod( 'blog-page-layout' ) == '1stfull' ): 

						get_template_part( 'layouts/blog', 'list-1st-fullpost' ); 
						
					else :

						get_template_part( 'layouts/blog', 'standard' );

					endif;
				 
				?>
	
			</div><!-- #recent-content -->		

		</main><!-- .site-main -->

		<?php get_template_part( 'template-parts/pagination', '' ); ?>

	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>