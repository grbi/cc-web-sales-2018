<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package VT Blogging Pro
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" >

			<section class="error-404 not-found">
				<header class="page-header">
					<div><i class="fa fa-ban"></i></div>
					<h1 class="page-title"><?php echo wp_kses_post( get_theme_mod( 'error-page-heading', __( 'Oops! <span>404</span> Error - Page Not Found', 'vt-blogging-pro' ) ) ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php echo wp_kses_post( get_theme_mod( 'error-page-msg', __( 'Sorry, but it looks like nothing was found at this location. You may try one of the links below or do a search.', 'vt-blogging-pro' ) ) ); ?></p>
					<p>
					<?php
						get_search_form(); 
					?>
					</p>

					<?php
						the_widget( 'WP_Widget_Recent_Posts' );

						// Only show the widget if site has multiple categories.
						if ( vt_blogging_pro_categorized_blog() ) :
					?>

					<div class="widget widget_categories">
						<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'vt-blogging-pro' ); ?></h2>
						<ul>
						<?php
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
						?>
						</ul>
					</div><!-- .widget -->

					<?php
						endif;

						/* translators: %1$s: smiley */
						$archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'vt-blogging-pro' ), convert_smilies( ':)' ) ) . '</p>';
						the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
						echo '<p></p>';
						the_widget( 'WP_Widget_Tag_Cloud' );
					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>