<?php
/**
 * The template for displaying the footer.
 *
 * @package VT Blogging Pro
 */

?>

	</div><!-- #content .site-content -->
	
	<footer id="colophon" class="site-footer">

		<?php
			/* A sidebar in the footer? Yep. You can can customize
			 * your footer with up to four columns of widgets.
			 */
			get_sidebar( 'footer' );
		?>

		<div id="site-bottom" class="clear">
			<div class="container">
		  
				<?php if ( get_theme_mod( 'footer-copyright' ) ) : ?>
					<div class="site-info"><?php echo get_theme_mod( 'footer-copyright' ); ?></div>
				<?php
					else : 
					do_action( 'vt_blogging_pro_footer' );
					endif; 

					if ( has_nav_menu( 'footer' ) ) {
						wp_nav_menu( array( 'theme_location' => 'footer', 'menu_id' => 'footer-menu', 'menu_class' => 'footer-nav' ) );
					} else {
						
					if ( get_theme_mod('footer-menu-on', true) ) : 
					if ( current_user_can( 'edit_theme_options' ) ) { ?>
					  <ul class="footer-nav">
						<li><?php printf( __( '<a href="%s">Add menu for theme location: Footer Menu</a>', 'vt-blogging-pro' ), esc_url( admin_url( 'nav-menus.php' ) ) ); ?></li>
					  </ul><!-- .sf-menu -->
				<?php
					} 
					endif;
					} ?>				

			</div>
		</div><!-- #site-bottom -->
							
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php if (get_theme_mod('sticky-header', 0) == 1) : ?>
<script type="text/javascript">

	(function($){ //Sticky Header 

	    $(document).ready(function(){

	        "use strict"; 
	        
			$(window).scroll(function() {
			    if ($(this).scrollTop() > 0) {
			        $('.site-header,.header-search').addClass('stickytop');
			    }
			    else {
			        $('.site-header,.header-search').removeClass('stickytop');
			    }
			});
			
	    });

	})(jQuery);

</script>
<?php
	endif;
	wp_footer();
?>
</body>
</html>