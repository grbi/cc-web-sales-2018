<?php
if ( have_posts() ) :	
							
	/* Start the Loop */
	while ( have_posts() ) : the_post();
	
	$pin_image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
	
	$class = ( $wp_query->current_post + 1 === $wp_query->post_count ) ? 'clear last' : 'clear';
?>

	<div id="post-<?php the_ID(); ?>" <?php post_class( $class ); ?>>	

		<div class="entry-overview wow fadeInUp" data-wow-duration=".8s" data-wow-delay=".2s">
			
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			
			<?php
				get_template_part('template-parts/entry', 'meta');
				get_template_part('template-parts/post', 'format');
				?>
		
			<div class="entry-summary">
				<?php the_excerpt(); ?>
				<span class="read-more"><a href="<?php the_permalink(); ?>"><?php esc_html_e('Read More', 'vt-blogging-pro'); ?> &raquo;</a></span>
			</div><!-- .entry-summary -->

		</div><!-- .entry-overview -->

	</div><!-- #post-<?php the_ID(); ?> -->

<?php
	endwhile;
	else:
		get_template_part('template-parts/content', 'none' );
	endif; 
	?>