<?php
	/* List first fullpost */
    if ( have_posts() ) : $vt_blogging_pro_count_post = 0;
	$class = ( $wp_query->current_post + 1 === $wp_query->post_count ) ? 'clear last' : 'clear';
        while ( have_posts() ) : the_post(); $vt_blogging_pro_count_post++;
            $sticky_class = ( is_sticky() ) ? 'is_sticky' : null;
            if ( $vt_blogging_pro_count_post == 1 ) {
	?>
	
	<div id="post-<?php the_ID(); ?>" <?php post_class( 'full-post' ); ?>>
		
		<div class="entry-overview wow fadeInUp" data-wow-duration=".8s" data-wow-delay=".2s">

			<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			
			<?php
				get_template_part('template-parts/entry', 'meta');
				get_template_part('template-parts/post', 'format');
				?>
		
			<?php if( strpos( $post->post_content, '<!--more-->' ) ) : ?>
				<div class="entry-summary">
					<?php the_excerpt(); ?>
					<span class="read-more link-more"><a href="<?php the_permalink(); ?>"><?php esc_html_e('Read More', 'vt-blogging-pro'); ?> &raquo;</a></span>
				</div><!-- .entry-summary -->
			<?php else : ?>			
				<div class="entry-content">
					<?php
						the_content();
						wp_link_pages( array(
							'before' => '<div class="page-links">' . __( 'Pages:', 'vt-blogging-pro' ),
							'after'  => '</div>',
						) );
					?>
				</div><!-- .entry-content -->
			<?php endif; ?>
				
		</div><!-- .entry-overview -->
	</div><!-- #post-<?php the_ID(); ?> -->
	
	<?php } else { ?>
			
		<div id="post-<?php the_ID(); ?>" <?php post_class( $class ); ?>>

			<div class="entry-overview wow fadeInUp" data-wow-duration=".8s" data-wow-delay=".2s">
					
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						
				<?php get_template_part('template-parts/entry', 'meta'); ?>

				<?php if ( has_post_thumbnail() ) { ?>
					<a class="thumbnail-link" href="<?php the_permalink(); ?>">
						<div class="thumbnail-wrap">
							<?php 
								the_post_thumbnail('vt_blogging_pro_thumb');
							?>
						</div><!-- .thumbnail-wrap -->
					</a>
				<?php } ?>	
						
				<div class="entry-summary">
					<?php the_excerpt(); ?>
					<span class="read-more"><a href="<?php the_permalink(); ?>"><?php esc_html_e('Read More', 'vt-blogging-pro'); ?> &raquo;</a></span>
				</div><!-- .entry-summary -->
							
			</div><!-- .entry-summary -->

		</div><!-- #post-<?php the_ID(); ?> -->
	<?php                
          }
          if ( $vt_blogging_pro_count_post == $wp_query->post_count) { ?>            

<?php
		}
        endwhile;
    else:
        get_template_part('template-parts/content', 'none');
    endif;
    ?>