===  VT Blogging Pro WordPress Theme ===

Author: VolThemes
Author URI: http://volthemes.com/
Requires at least: WordPress 4.1
Tested up to: WordPress 4.9.2
Version: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: two-columns, custom-background, custom-colors, custom-header, custom-menu, editor-style, featured-images, microformats, post-formats, sticky-post, theme-options, translation-ready, threaded-comments

== Installation via WordPress ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the "vt-blogging-pro.zip" file. Click Install Now.
3. Click Activate to use your new theme right away.

== Installation via FTP ==

1. Upload the vt-blogging-pro theme folder to your wp-content/themes/ directory.
2. Login wp-admin and go to "Appearance > Themes".
3. Click Activate to use your new theme right away.
