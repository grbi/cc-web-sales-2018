<?php
/* WooCommerce custom hooks and functions used by the theme */

// Display 12 products per page
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = get_theme_mod('shop-limit-post');
  return $cols;
}

/**
 * Add Cart icon and count to header if WC is active
 */
function vt_blogging_pro_wc_cart_count() {
 
    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 
        $count = WC()->cart->cart_contents_count;
        ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart', 'vt-blogging-pro' ); ?>"><?php
        if ( $count > 0 ) {
            ?>
            <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
            <?php
        }
                ?></a><?php
    }
 
}
add_action( 'vt_blogging_pro_header_top', 'vt_blogging_pro_wc_cart_count' );

/**
 * Ensure cart contents update when products are added to the cart via AJAX
 */
function vt_blogging_pro_header_add_to_cart_fragment( $fragments ) {
 
    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart', 'vt-blogging-pro' ); ?>"><?php
    if ( $count > 0 ) {
        ?>
        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
        <?php            
    }
        ?></a><?php
 
    $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'vt_blogging_pro_header_add_to_cart_fragment' );

// Custom oredering of shop items
add_filter('woocommerce_get_catalog_ordering_args', 'custom_woocommerce_get_catalog_ordering_args');
if ( !function_exists( 'custom_woocommerce_get_catalog_ordering_args' ) ) :
	function custom_woocommerce_get_catalog_ordering_args( $args ) {
		if (isset($_SESSION['orderby'])) {
			switch ($_SESSION['orderby']) :
				case 'date_asc' :
					$args['orderby'] = 'date';
					$args['order'] = 'asc';
					$args['meta_key'] = '';
				break;
				case 'price_desc' :
					$args['orderby'] = 'meta_value_num';
					$args['order'] = 'desc';
					$args['meta_key'] = '_price';
				break;
				case 'title_desc' :
					$args['orderby'] = 'title';
					$args['order'] = 'desc';
					$args['meta_key'] = '';
				break;
			endswitch;
		}
		return $args;
	}
endif;

add_filter('woocommerce_catalog_orderby', 'custom_woocommerce_catalog_orderby');
if ( !function_exists( 'custom_woocommerce_catalog_orderby' ) ) :
	function custom_woocommerce_catalog_orderby( $sortby ) {
		$sortby['title_desc'] = __( 'Reverse Alphabetically', 'vt-blogging-pro' );
		$sortby['price_desc'] = __( 'Price (highest to lowest)', 'vt-blogging-pro' );
		$sortby['date_asc'] = __( 'Oldest to newest', 'vt-blogging-pro' );
		return $sortby;
	}
endif;

// Set WooCommerce image dimensions upon theme activation
global $pagenow;
if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' )
	add_action('init', 'custom_woocommerce_image_dimensions', 1);

if ( !function_exists( 'custom_woocommerce_image_dimensions' ) ) :
	function custom_woocommerce_image_dimensions() {
		// Image sizes
		update_option( 'woocommerce_thumbnail_image_width', '90' ); // Image gallery thumbs
		update_option( 'woocommerce_thumbnail_image_height', '999' );
		update_option( 'woocommerce_single_image_width', '370' ); // Featured product image
		update_option( 'woocommerce_single_image_height', '999' );
		update_option( 'woocommerce_catalog_image_width', '240' ); // Product category thumbs
		update_option( 'woocommerce_catalog_image_height', '999' );

		// Hard Crop [0 = false, 1 = true]
		update_option( 'woocommerce_thumbnail_image_crop', 0 );
		update_option( 'woocommerce_single_image_crop', 0 );
		update_option( 'woocommerce_catalog_image_crop', 0 );
	}
endif;