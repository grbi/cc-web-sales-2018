<?php
/**
 * The template for displaying archive pages.
 *
 * @package VT Blogging Pro
 */

get_header(); ?>

	<div id="primary" class="content-area clear">

		<main id="main" class="site-main clear">

			<div class="section-header clear">
				<h1>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php _e( 'Home', 'vt-blogging-pro' ); ?></a> &raquo; <?php single_cat_title(''); ?>
				</h1>
			</div><!-- .section-header -->
				
			<div id="recent-content" class="content-loop">

				<?php if ( get_theme_mod( 'archive-page-layout' ) == 'layout-1' ) : 

						get_template_part( 'layouts/blog', 'list-layout-1' );

					elseif ( get_theme_mod( 'archive-page-layout' ) == 'layout-2' ):

						get_template_part( 'layouts/blog', 'list-layout-2' ); 
						
					elseif ( get_theme_mod( 'archive-page-layout' ) == 'standard-list' ):

						get_template_part( 'layouts/blog', 'standard-list' ); 
						
					elseif ( get_theme_mod( 'archive-page-layout' ) == '1stfull' ): 

						get_template_part( 'layouts/blog', 'list-1st-fullpost' ); 
						
					else : 

						get_template_part( 'layouts/blog', 'standard' );

					endif;
						
				?>
			</div><!-- #recent-content -->

		</main><!-- .site-main -->

		<?php get_template_part( 'template-parts/pagination', '' ); ?>

	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>