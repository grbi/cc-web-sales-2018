<?php
/**
 * Register widget area.
 *
 * @since VT Blogging Pro 1.0
 */
function vt_blogging_pro_widgets_init() {

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'vt-blogging-pro' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'vt-blogging-pro' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title"><span>',
		'after_title'   => '</span></h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 1', 'vt-blogging-pro' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'vt-blogging-pro' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 2', 'vt-blogging-pro' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'vt-blogging-pro' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 3', 'vt-blogging-pro' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'vt-blogging-pro' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 4', 'vt-blogging-pro' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'vt-blogging-pro' ),
		'before_widget' => '<div id="%1$s" class="widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	) );

	/***** Register Widgets *****/
	register_widget('vt_blogging_pro_recent_comments_widget');
	register_widget('vt_blogging_pro_recent_posts_widget');
	register_widget('vt_blogging_pro_most_viewed_widget');
	register_widget('vt_blogging_pro_newsletter_widget');
	register_widget('vt_blogging_pro_instagram_widget');
	register_widget('vt_blogging_pro_popular_widget');
	register_widget('vt_blogging_pro_social_widget');
	register_widget('vt_blogging_pro_random_widget');
	register_widget('vt_blogging_pro_about_widget');
	register_widget('vt_blogging_pro_ad_widget');

}
add_action( 'widgets_init', 'vt_blogging_pro_widgets_init' );

/***** Include Widgets *****/
get_template_part('inc/widgets/widget-recent-comments');
get_template_part('inc/widgets/widget-recent-posts');
get_template_part('inc/widgets/widget-most-viewed');
get_template_part('inc/widgets/widget-newsletter');
get_template_part('inc/widgets/widget-instagram');
get_template_part('inc/widgets/widget-popular');
get_template_part('inc/widgets/widget-social');
get_template_part('inc/widgets/widget-random');
get_template_part('inc/widgets/widget-about');
get_template_part('inc/widgets/widget-ad');

?>