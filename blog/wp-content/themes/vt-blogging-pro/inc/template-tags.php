<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package VT Blogging Pro
 */

/**
 * Get Post Views.
 */
if ( ! function_exists( 'vt_blogging_pro_get_post_views' ) ) :

function vt_blogging_pro_get_post_views($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return '<span class="view-count">0</span> View';
    }
    return '<span class="view-count"><i class="fa fa-eye"></i>' . number_format($count) . '</span> ';
}

endif;

/**
 * Set Post Views.
 */
if ( ! function_exists( 'vt_blogging_pro_set_post_views' ) ) :

function vt_blogging_pro_set_post_views($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

endif;

/**
 * Search Filter 
 */
if ( ! function_exists( 'vt_blogging_pro_search_filter' ) ) :

function vt_blogging_pro_search_filter($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}
	return $query;
}

add_filter('pre_get_posts','vt_blogging_pro_search_filter');

endif;

/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
if ( ! function_exists( 'vt_blogging_pro_custom_excerpt_length' ) ) :

function vt_blogging_pro_custom_excerpt_length( $length ) {
    return get_theme_mod('entry-excerpt-length', '38');
}
add_filter( 'excerpt_length', 'vt_blogging_pro_custom_excerpt_length', 999 );

endif;

/**
 * Customize excerpt more.
 */
if ( ! function_exists( 'vt_blogging_pro_excerpt_more' ) ) :

function vt_blogging_pro_excerpt_more( $more ) {
   return '... ';
}
add_filter( 'excerpt_more', 'vt_blogging_pro_excerpt_more' );

endif;

/**
 * Custom excerpt max charlength
 */
function vt_blogging_pro_the_excerpt_max_charlength($charlength)
{
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '&nbsp;...';
	} else {
		echo $excerpt;
	}
}

/**
 * Display the first (single) category of post.
 */
if ( ! function_exists( 'vt_blogging_pro_first_category' ) ) :
function vt_blogging_pro_first_category() {
    $category = get_the_category();
    if ($category) {
      echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s", 'vt-blogging-pro' ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
    }    
}
endif;

/**
 * Adjust is_home query if vt-blog-cats is set
 */
function vt_blogging_pro_set_blog_queries( $query ) {
    $blog_query_set = '';
    if ( get_theme_mod( 'vt-blog-cats' ) ) {
        $blog_query_set = get_theme_mod( 'vt-blog-cats' );
    }

    if ( $blog_query_set ) {
        // do not alter the query on wp-admin pages and only alter it if it's the main query
        if ( !is_admin() && $query->is_main_query() ){
            if ( is_home() ){
                $query->set( 'cat', $blog_query_set );
            }
        }
    }
}
add_action( 'pre_get_posts', 'vt_blogging_pro_set_blog_queries' );

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
if ( ! function_exists( 'vt_blogging_pro_categorized_blog' ) ) :

function vt_blogging_pro_categorized_blog() {
    if ( false === ( $all_the_cool_cats = get_transient( 'vt_blogging_pro_categories' ) ) ) {
        // Create an array of all the categories that are attached to posts.
        $all_the_cool_cats = get_categories( array(
            'fields'     => 'ids',
            'hide_empty' => 1,
            // We only need to know if there is more than one category.
            'number'     => 2,
        ) );

        // Count the number of categories that are attached to the posts.
        $all_the_cool_cats = count( $all_the_cool_cats );

        set_transient( 'vt_blogging_pro_categories', $all_the_cool_cats );
    }

    if ( $all_the_cool_cats > 1 ) {
        // This blog has more than 1 category so vt_blogging_pro_categorized_blog should return true.
        return true;
    } else {
        // This blog has only 1 category so vt_blogging_pro_categorized_blog should return false.
        return false;
    }
}

endif;

// Url Encode
function vt_blogging_pro_url_encode($title){
    $title = html_entity_decode($title);
    $title = urlencode($title);
    return $title;
}

/**
 * Footer info, copyright information
 */
if ( ! function_exists( 'vt_blogging_pro_footer' ) ) :
function vt_blogging_pro_footer() {
   $site_link = '<a href="' . esc_url( home_url( '/' ) ) . '" title="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" ><span>' . get_bloginfo( 'name', 'display' ) . '</span></a>';

   $tg_link =  '<a href="'.esc_url("http://volthemes.com/theme/vt-blogging-pro/").'" target="_blank" title="'.esc_attr__( 'VolThemes', 'vt-blogging-pro' ).'"><span>'.__( 'VolThemes', 'vt-blogging-pro') .'</span></a>';

   $default_footer_value = sprintf( __( 'Copyright &copy; %1$s %2$s.', 'vt-blogging-pro' ), date( 'Y' ), $site_link ).' '.sprintf( __( 'Theme by %2$s', 'vt-blogging-pro' ), 'vt-blogging-pro', $tg_link );

   $vt_blogging_pro_footer = '<div class="site-info">'.$default_footer_value.'</div>';
   echo $vt_blogging_pro_footer;
}
endif;
add_action( 'vt_blogging_pro_footer', 'vt_blogging_pro_footer', 10 );

// Scroll to top
function vt_blogging_pro_scroll_to_top() {
	if (get_theme_mod('backtotop-button', 1) == 1){
?>
	<div id="back-top">
		<a href="#top" title="<?php echo esc_attr('Back to top', 'vt-blogging-pro'); ?>"><span class="genericon genericon-collapse"></span></a>
	</div>
<?php
}}
add_action('wp_footer', 'vt_blogging_pro_scroll_to_top');

/**
 * Flush out the transients used in vt_blogging_pro_categorized_blog.
 */
if ( ! function_exists( 'vt_blogging_pro_category_transient_flusher' ) ) :

function vt_blogging_pro_category_transient_flusher() {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }
    // Like, beat it. Dig?
    delete_transient( 'vt_blogging_pro_categories' );
}
add_action( 'edit_category', 'vt_blogging_pro_category_transient_flusher' );
add_action( 'save_post',     'vt_blogging_pro_category_transient_flusher' );

endif;