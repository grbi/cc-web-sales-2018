<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package VT Blogging Pro
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function vt_blogging_pro_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'vt_blogging_pro_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function vt_blogging_pro_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', bloginfo( 'pingback_url' ), '">';
	}
}
add_action( 'wp_head', 'vt_blogging_pro_pingback_header' );

// Author Social Links
add_filter( 'user_contactmethods', 'vt_blogging_pro_author_social_links' );
function vt_blogging_pro_author_social_links()
{
    $contactmethods              = array();
	$contactmethods['facebook']  = 'Facebook Username';
	$contactmethods['twitter']   = 'Twitter Username';
	$contactmethods['instagram'] = 'Instagram Username';
	$contactmethods['pinterest'] = 'Pinterest Username';
	$contactmethods['google']    = 'Google Plus Username';
	$contactmethods['linkedin']  = 'Linkedin Username';
	$contactmethods['tumblr']    = 'Tumblr Username';
	
	return $contactmethods;
}

// Add SoundCloud oEmbed
function add_oembed_soundcloud(){
wp_oembed_add_provider( 'http://soundcloud.com/*', 'http://soundcloud.com/oembed' );
}
add_action('init','add_oembed_soundcloud');

// Display custom categories
function vt_blogging_pro_display_custom_categories( $post_id, $limit = 5 ) {
    $cats = array(); $i = 0; $post_id = (int)$post_id;
    foreach( wp_get_post_categories($post_id) as $c )
    {
        $i++;
        if ( $i <= $limit )
        {
            $cat = get_category($c);
            array_push($cats, '<a href="'.(get_category_link($cat->term_id)).'">'.esc_html($cat->name).'</a>');
        }
        
        if ( $i == $limit + 1 ) {
            array_push($cats, '...');
        }
    }
    
    if ( sizeOf($cats) > 0 ) {
    	$post_categories = implode(', ',$cats);
    } else {
    	$post_categories = "Not Assigned";
    }
    
    return $post_categories;
}

/**
 * Register a custom Post Categories ID column
 */
function vt_blogging_pro_edit_cat_columns( $vt_blogging_pro_cat_columns ) {
    $vt_blogging_pro_cat_in = array( 'cat_id' => 'Category ID <span class="cat_id_note">For the Default Slider</span>' );
    $vt_blogging_pro_cat_columns = vt_blogging_pro_cat_columns_array_push_after( $vt_blogging_pro_cat_columns, $vt_blogging_pro_cat_in, 0 );
    return $vt_blogging_pro_cat_columns;
}
add_filter( 'manage_edit-category_columns', 'vt_blogging_pro_edit_cat_columns' );

// Print the ID column
function vt_blogging_pro_cat_custom_columns( $value, $name, $cat_id ) {
    if( 'cat_id' == $name )
        echo $cat_id;
}
add_filter( 'manage_category_custom_column', 'vt_blogging_pro_cat_custom_columns', 10, 3 );

// Insert an element at the beggining of the array
function vt_blogging_pro_cat_columns_array_push_after( $src, $vt_blogging_pro_cat_in, $pos ) {
    if ( is_int( $pos ) ) {
        $R = array_merge( array_slice( $src, 0, $pos + 1 ), $vt_blogging_pro_cat_in, array_slice( $src, $pos + 1 ) );
    } else {
        foreach ( $src as $k => $v ) {
            $R[$k] = $v;
            if ( $k == $pos )
                $R = array_merge( $R, $vt_blogging_pro_cat_in );
        }
    }
    return $R;
}