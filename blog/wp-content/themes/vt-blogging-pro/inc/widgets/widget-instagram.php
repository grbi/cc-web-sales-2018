<?php
/**
 * Instagram widget.
 *
 * @package    VT Blogging Pro
 * @author     VolThemes
 * @copyright  Copyright (c) 2018, VolThemes
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 * @since      1.0.0
 */
 
class vt_blogging_pro_instagram_widget extends WP_Widget {

	/**
	 * Sets up the widgets.
	 *
	 * @since 1.0.0
	 */
    function __construct() {
		
		// Set up the widget options.
		$widget_options = array(
			'classname'   => 'instagram-widget',
			'description' => __( 'Display Instagram Widget with images from your instagram feed.', 'vt-blogging-pro' )
		);
		
		parent::__construct(
			'vt-instagram-widget',  					      // $this->id_base
			__( '[VT] Instagram Widget', 'vt-blogging-pro'),  // $this->name
			$widget_options                                   // $this->widget_options
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* User-selected settings. */
		$title = apply_filters( 'widget_title', $instance['title'] );
		$username = $instance['username'];
		$number = $instance['number'];
		$customsize = $instance['customsize'];

		echo $before_widget; 
		?>

			<div class="instagram-wrap">
				<?php if($title != '') echo '<h4 class="widget-title"><span>'.$title.'</span></h4>'; ?>
				<?php if($username != '') {
					$images_array = $this->scrape_instagram($username, $number);

					if ( is_wp_error($images_array) ) {
					   echo $images_array->get_error_message();
					} else {
						?><ul class="instagram-pics"><?php
						foreach ($images_array as $image) {
							echo '<li><a href="'.$image['link'].'" target="_blank"><img src="'. esc_url( $image['thumbnail'] ) .'"  alt="'. esc_attr( $image['description'] ) .'" title="'. esc_attr( $image['description'] ).'" width="'.$customsize.'" height="'.$customsize.'"/></a></li>';
						}
						?></ul><?php
					}
				}?>
			</div>

		<?php
		echo $after_widget;
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['username'] = trim(strip_tags($new_instance['username']));
		$instance['number'] = !absint($new_instance['number']) ? 6 : $new_instance['number'];
		$instance['customsize'] = $new_instance['customsize'];

		return $instance;
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	function form( $instance ) {

		$instance = wp_parse_args( (array) $instance, 
			array( 
				'title' => __('Instagram','vt-blogging-pro'), 
				'username' => '', 
				'number' => 6, 
				'customsize' => '90'
				) 
			);
		$title = esc_attr($instance['title']);
		$username = esc_attr($instance['username']);
		$number = absint($instance['number']);
		$customsize = esc_attr($instance['customsize']);
		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:','vt-blogging-pro'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $instance['title']; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'username' ); ?>"><?php _e('Username:','vt-blogging-pro'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'username' ); ?>" name="<?php echo $this->get_field_name( 'username' ); ?>" type="text" value="<?php echo $instance['username']; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Number of photos:','vt-blogging-pro'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $instance['number']; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'customsize' ); ?>"><?php _e('Custom Size (Max size 90):','vt-blogging-pro'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'customsize' ); ?>" name="<?php echo $this->get_field_name( 'customsize' ); ?>" type="text" value="<?php echo $instance['customsize']; ?>" />
		</p>

		<?php
	}


	function scrape_instagram( $username, $slice = 9 ) {
		$username = strtolower( $username );
		if ( false === ( $instagram = get_transient( 'instagram-media-new-'.sanitize_title_with_dashes( $username ) ) ) ) {
			$remote = wp_remote_get( 'http://instagram.com/'.trim( $username ) );
			if ( is_wp_error( $remote ) )
				return new WP_Error( 'site_down', __( 'Unable to communicate with Instagram.', 'vt-blogging-pro' ) );
			if ( 200 != wp_remote_retrieve_response_code( $remote ) )
				return new WP_Error( 'invalid_response', __( 'Instagram did not return a 200.', 'vt-blogging-pro' ) );
			$shards = explode( 'window._sharedData = ', $remote['body'] );
			$insta_json = explode( ';</script>', $shards[1] );
			$insta_array = json_decode( $insta_json[0], TRUE );
			if ( !$insta_array )
				return new WP_Error( 'bad_json', __( 'Instagram has returned invalid data.', 'vt-blogging-pro' ) );
			// old style
			if ( isset( $insta_array['entry_data']['UserProfile'][0]['userMedia'] ) ) {
				$images = $insta_array['entry_data']['UserProfile'][0]['userMedia'];
				$type = 'old';
			// new style
			} else if ( isset( $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'] ) ) {
				$images = $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
				$type = 'new';
			} else {
				return new WP_Error( 'bad_josn_2', __( 'Instagram has returned invalid data.', 'vt-blogging-pro' ) );
			}
			if ( !is_array( $images ) )
				return new WP_Error( 'bad_array', __( 'Instagram has returned invalid data.', 'vt-blogging-pro' ) );
			$instagram = array();
			switch ( $type ) {
				case 'old':
					foreach ( $images as $image ) {
						if ( $image['user']['username'] == $username ) {
							$image['link']						  = preg_replace( "/^http:/i", "", $image['link'] );
							$image['images']['thumbnail']		   = preg_replace( "/^http:/i", "", $image['images']['thumbnail'] );
							$image['images']['standard_resolution'] = preg_replace( "/^http:/i", "", $image['images']['standard_resolution'] );
							$image['images']['low_resolution']	  = preg_replace( "/^http:/i", "", $image['images']['low_resolution'] );
							$instagram[] = array(
								'description'   => $image['caption']['text'],
								'link'		  	=> $image['link'],
								'time'		  	=> $image['created_time'],
								'comments'	  	=> $image['comments']['count'],
								'likes'		 	=> $image['likes']['count'],
								'thumbnail'	 	=> $image['images']['thumbnail'],
								'large'		 	=> $image['images']['standard_resolution'],
								'small'		 	=> $image['images']['low_resolution'],
								'type'		  	=> $image['type']
							);
						}
					}
				break;
				default:
					foreach ( $images as $image ) {
						$image['display_src'] = preg_replace( "/^http:/i", "", $image['display_src'] );
						if ( $image['is_video']  == true ) {
							$type = 'video';
						} else {
							$type = 'image';
						}
						$instagram[] = array(
							'description'   => __( 'Instagram Image', 'vt-blogging-pro' ),
							'link'		  	=> '//instagram.com/p/' . $image['code'],
							'time'		  	=> $image['date'],
							'comments'	  	=> $image['comments']['count'],
							'likes'		 	=> $image['likes']['count'],
							'thumbnail'	 	=> $image['display_src'],
							'type'		  	=> $type
						);
					}
				break;
			}
			// do not set an empty transient - should help catch private or empty accounts
			if ( ! empty( $instagram ) ) {
				$instagram = base64_encode( serialize( $instagram ) );
				set_transient( 'instagram-media-new-'.sanitize_title_with_dashes( $username ), $instagram, apply_filters( 'null_instagram_cache_time', HOUR_IN_SECONDS*2 ) );
			}
		}
		if ( ! empty( $instagram ) ) {
			$instagram = unserialize( base64_decode( $instagram ) );
			return array_slice( $instagram, 0, $slice );
		} else {
			return new WP_Error( 'no_images', __( 'Instagram did not return any images.', 'vt-blogging-pro' ) );
		}
	}
	
} // class vt_blogging_pro_instagram_widget
?>