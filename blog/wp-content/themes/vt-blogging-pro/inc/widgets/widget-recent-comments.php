<?php 
/**
 * Recent Comments with Thumbnail widget.
 *
 * @package    VT Blogging Pro
 * @author     VolThemes
 * @copyright  Copyright (c) 2018, VolThemes
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 * @since      1.0.0
 */

class vt_blogging_pro_recent_comments_widget extends WP_Widget {
	
	/**
	 * Sets up the widgets.
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		
		// Set up the widget options.
		$widget_options = array(
			'classname'   => 'recent-comments-widget',
			'description' => __( 'Displays latest comments with author avatar', 'vt-blogging-pro' )
		);
		
		parent::__construct(
			'vt-recent-comments', 						     // $this->id_base
			__( '[VT] Recent Comments', 'vt-blogging-pro'),  // $this->name
			$widget_options                                  // $this->widget_options
		);
	}
	
	// Widget output
	function widget( $args, $instance ) {
		
		if(empty($instance['title'])) :
			$title = __( 'Recent Comments', 'vt-blogging-pro' );
		else :
			$title = apply_filters( 'widget_title', $instance['title'] );	
		endif;
		
		if(empty($instance['comments_to_show'])) :
			$comments_to_show = 4;
		else :
			$comments_to_show = $instance['comments_to_show'];
		endif;
			
			echo $args['before_widget'];
			
			if ( ! empty( $title ) ) :
			echo $args['before_title'] . $title . $args['after_title'];
			endif;
			
				$comment_loop_args = array(
				  'number' => $comments_to_show,
				  'status' => 'approve',
				);
				
				$comments_query = new WP_Comment_Query;
				$comments = $comments_query->query( $comment_loop_args );
				?>
				<ul>
					<?php
					if ( $comments ) :
						foreach ( $comments as $comment ) {
					?>
					<li class="vt-blogging-pro-widget-latest">				
						<div class="vt-blogging-pro-widget-latest-left vt-blogging-pro-widget-latest-comment-avatar">
							<a href="<?php echo get_permalink($comment->comment_post_ID); ?>#comment-<?php echo $comment->comment_ID; ?>"><?php echo get_avatar($comment->comment_author_email , 60); ?></a>
						</div><!-- .vt-blogging-pro-latest-comment-avatar -->
						<div class="vt-blogging-pro-widget-latest-right">
							<p class="vt-blogging-pro-widget-latest-comment-author"><?php echo $comment->comment_author . __(' commented on:', 'vt-blogging-pro'); ?></p>
							<a href="<?php echo get_permalink($comment->comment_post_ID); ?>#comment-<?php echo $comment->comment_ID; ?>"><?php echo get_the_title($comment->comment_post_ID); ?></a>
						</div><!-- .vt-blogging-pro-latest-comment-content -->
					</li>
					
					<?php
						}
					else : ?>
						<li><?php _e( 'No comments found', 'vt-blogging-pro'); ?></li>
					<?php 	
					endif;
					?>
				</ul>
				<?php
	
			echo $args['after_widget'];
	}
	
	// Widget form
	function form($field) {
		
		if ( isset( $field[ 'title' ] ) ) :
			$title = $field[ 'title' ];
		else :
			$title = __('Recent Comments', 'vt-blogging-pro');
		endif;
		if(!isset($field['comments_to_show'])) :
			$posts_to_show = '5';
		else :
			$posts_to_show = $field['comments_to_show'];
		endif;
	?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'vt-blogging-pro'); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>  
		    <label for="<?php echo $this->get_field_id( 'comments_to_show' ); ?>"><?php _e('Comments to display:', 'vt-blogging-pro'); ?></label>  
		    <input class="widefat" id="<?php echo $this->get_field_id( 'comments_to_show' ); ?>" name="<?php echo $this->get_field_name( 'comments_to_show' ); ?>" type="text" value="<?php echo esc_attr($posts_to_show); ?>" />  
		</p>  
	<?php 
	}
	
	// Update widget  
	function update( $new_setting, $old_setting ) {
		$setting = array();
		$setting['title'] = ( ! empty( $new_setting['title'] ) ) ? strip_tags( $new_setting['title'] ) : '';
		$setting['comments_to_show'] = ( ! empty( $new_setting['comments_to_show'] ) ) ? strip_tags( $new_setting['comments_to_show'] ) : '';
		return $setting;
	}
	
} // class vt_blogging_pro_recent_comments_widget
?>