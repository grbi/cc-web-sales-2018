<?php
/**
 * Most Views Posts with Thumbnail widget.
 *
 * @package    VT Blogging Pro
 * @author     VolThemes
 * @copyright  Copyright (c) 2018, VolThemes
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 * @since      1.0.0
 */

class vt_blogging_pro_most_viewed_widget extends WP_Widget {

	/**
	 * Sets up the widgets.
	 *
	 * @since 1.0.0
	 */
	function __construct() {

		// Set up the widget options.
		$widget_options = array(
			'classname'   => 'most-viewed-widget widget_posts_thumbnail',
			'description' => __( 'Display the most viewed posts with thumbnails.', 'vt-blogging-pro' )
		);

		parent::__construct(
			'vt-widget-most-viewed',                            // $this->id_base
			__( '[VT] Most Viewed Posts', 'vt-blogging-pro' ),  // $this->name
			$widget_options                                     // $this->widget_options
		);

	}

	/**
	 * Outputs the widget based on the arguments input through the widget controls.
	 *
	 * @since 1.0.0
	 */
	function widget( $args, $instance ) {
		extract( $args );

		// Output the theme's $before_widget wrapper.
		echo $before_widget;

		// If the title not empty, display it.
		if ( $instance['title'] ) {
			echo $before_title . apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base ) . $after_title;
		}

		// Posts query arguments.
		$args = array(
			'post_type'      => 'post',
			'posts_per_page' => $instance['limit'],
			'orderby'        => 'meta_value_num',
			'meta_key'       => 'post_views_count'
		);

		// The post query
		$views = new WP_Query( $args );

		global $post;
		if ( $views->have_posts() ) {
			echo '<ul>';

				while ( $views->have_posts() ) : $views->the_post();

					echo '<li class="clear">';

						echo '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . '<div class="thumbnail-wrap">';
								
							if ( has_post_thumbnail() ) {
								the_post_thumbnail('vt_blogging_pro_widget');
							}
							else {
								echo '<img src="' . esc_url( get_template_directory_uri() ) . '/assets/img/no-thumbnail.png" alt="' . esc_attr__('No Picture', 'vt-blogging-pro') . '" />';
							}	
								
						echo '</div>' . '</a>';
						
						echo '<div class="entry-wrap"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . esc_html( get_the_title() ) . '</a>'; 

						if ( $instance['show_date'] ) :
							echo '<div class="entry-meta"><span class="entry-date">' . get_the_date() . '</span>';
						endif;

						if ( $instance['show_view_counts'] ) :
							echo '' . vt_blogging_pro_get_post_views(get_the_ID()) . '</div>';
						endif;
						
						echo '</div></li>';

				endwhile;

			echo '</ul>';
		}

		// Reset the query.
		wp_reset_postdata();

		// Close the theme's widget wrapper.
		echo $after_widget;

	}

	/**
	 * Updates the widget control options for the particular instance of the widget.
	 *
	 * @since 1.0.0
	 */
	function update( $new_instance, $old_instance ) {

		$instance = $new_instance;
		$instance['title']     = strip_tags( $new_instance['title'] );
		$instance['limit']     = (int) $new_instance['limit'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		$instance['show_view_counts'] = isset( $new_instance['show_view_counts'] ) ? (bool) $new_instance['show_view_counts'] : false;

		return $instance;
	}

	/**
	 * Displays the widget control options in the Widgets admin screen.
	 *
	 * @since 1.0.0
	 */
	function form( $instance ) {

		// Default value.
		$defaults = array(
			'title'     => esc_html__( 'Most Viewed Posts', 'vt-blogging-pro' ),
			'limit'     => 5,
			'show_date' => true,
			'show_view_counts' => true
		);

		$instance = wp_parse_args( (array) $instance, $defaults );
	?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">
				<?php _e( 'Title', 'vt-blogging-pro' ); ?>
			</label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'limit' ); ?>">
				<?php _e( 'Number of posts to show', 'vt-blogging-pro' ); ?>
			</label>
			<input class="small-text" id="<?php echo $this->get_field_id( 'limit' ); ?>" name="<?php echo $this->get_field_name( 'limit' ); ?>" type="number" step="1" min="0" value="<?php echo (int)( $instance['limit'] ); ?>" />
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['show_date'] ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_date' ); ?>">
				<?php _e( 'Display post date?', 'vt-blogging-pro' ); ?>
			</label>
		</p>
		
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $instance['show_view_counts'] ); ?> id="<?php echo $this->get_field_id( 'show_view_counts' ); ?>" name="<?php echo $this->get_field_name( 'show_view_counts' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_view_counts' ); ?>">
				<?php _e( 'Display post view counts?', 'vt-blogging-pro' ); ?>
			</label>
		</p>

	<?php

	}

} // class vt_blogging_pro_views_widget
?>