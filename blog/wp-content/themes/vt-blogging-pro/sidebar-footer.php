<?php
/**
 * The Sidebar containing the Footer Widgets
 *
 * @package VT Blogging Pro
 * @since VT Blogging Pro 1.0
 */
 
//$col_numbers = esc_attr(of_get_option( 'footer_column' ));
$col_numbers = esc_attr(get_theme_mod( 'footer-widget-column' ));
if (   ! is_active_sidebar( 'footer-1' )
	&& ! is_active_sidebar( 'footer-2' )
	&& ! is_active_sidebar( 'footer-3' )
	&& ! is_active_sidebar( 'footer-4' )
)
	
return;
// If we get this far, we have widgets. Let do this.
 
?>

<?php if ( ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) || is_active_sidebar( 'footer-4' )) ) { ?>

	<div class="footer-columns clear">

		<div class="container clear col<?php echo esc_attr($col_numbers); ?>">
			<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
				<div class="footer-column footer-column-1 wow fadeInUp" data-wow-duration=".7s" data-wow-delay=".2s">
					<?php dynamic_sidebar( 'footer-1' ); ?>
				</div>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
				<div class="footer-column footer-column-2 wow fadeInUp" data-wow-duration=".7s" data-wow-delay=".2s">
					<?php dynamic_sidebar( 'footer-2' ); ?>
				</div>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
				<div class="footer-column footer-column-3 wow fadeInUp" data-wow-duration=".7s" data-wow-delay=".2s">
					<?php dynamic_sidebar( 'footer-3' ); ?>
				</div>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
				<div class="footer-column footer-column-4 wow fadeInUp" data-wow-duration=".7s" data-wow-delay=".2s">
					<?php dynamic_sidebar( 'footer-4' ); ?>
				</div>												
			<?php endif; ?>
		</div><!-- .container -->

	</div><!-- .footer-columns -->

<?php } ?>