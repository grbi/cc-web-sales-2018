<?php
/**
 * Defines customizer options
 *
 * @package Customizer Library VT Blogging Pro
 */

function customizer_library_vt_blogging_pro_options() {

	// Theme defaults
	$primary_color = '#eb5424';
	$secondary_color = '#ffffff';
	$tertiary_color = '#65aba7';
	
	$body_font_color = '#636161';
	$heading_font_color = '#3a3a3a';
	
	$submenu_link_color = '#222222';
    $header_bg_color = '#343e47';
    $footer_bg_color = '#343e47';
    $footer_text_color = '#9f9f9f';
	
	// Stores all the controls that will be added
	$options = array();

	// Stores all the sections to be added
	$sections = array();

	// Stores all the panels to be added
	$panels = array();

	// Adds the sections to the $options array
	$options['sections'] = $sections;

	/**-----------------
	 * Logo
	 -----------------*/
    $section = 'title_tagline';
    
    $sections[] = array(
        'id' => $section,
        'title' => __( 'Site Identity', 'vt-blogging-pro' ),
        'priority' => '10',
	);

    $options['logo-height-size'] = array(
        'id' => 'logo-height-size',
        'label'   => __( 'Set a max-height for the logo', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'default' => 67,
        'description' => __( 'Set a Logo Height (without: <strong>px</strong>). Default: 67', 'vt-blogging-pro' ),
		'priority' => '8',
    );
    $options['logo-max-width'] = array(
        'id' => 'logo-max-width',
        'label'   => __( 'Set a Logo max-width for the logo', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'default' => 200,
        'description' => __( 'This only applies if a logo image is uploaded. Default: 200', 'vt-blogging-pro' ),
		'priority' => '8',
    );
	
	/**-----------------
	 * Theme Settings
	 -----------------*/
    $panel = 'vt-panel-layout';

    $panels[] = array(
        'id' => $panel,
        'title' => __( 'Theme Settings', 'vt-blogging-pro' ),
        'priority' => '30'
    );

	// Blog Layout
	$section = 'vt-site-layout-section-site';

	$sections[] = array(
		'id' => $section,
		'title' => __( 'Blog Settings', 'vt-blogging-pro' ),
		'priority' => '20',
		'panel' => $panel
	);
    
	$choices = array(
		'choice-1' => __('Responsive Layout', 'vt-blogging-pro'),
		'choice-2' => __('Fixed Layout', 'vt-blogging-pro'),
    );
	$options['site-layout'] = array(
		'id' => 'site-layout',
		'label'   => __( 'Site Layout', 'vt-blogging-pro' ),
		'section' => $section,
		'type'    => 'radio',
        'choices' => $choices,
		'default' => 'choice-1'
	);
	
    $choices = array(
		'layout-1' => __('List Layout - Left Thumbnail', 'vt-blogging-pro'),
		'layout-2' => __('List Layout - Right Thumbnail', 'vt-blogging-pro'),
		'1stfull' => __('List Layout - First Fullpost', 'vt-blogging-pro'),
		'standard-list' => __('First Standard + List Layout', 'vt-blogging-pro'),
		'standard' => __('Standard Blog Layout', 'vt-blogging-pro'),
			
    );
    $options['blog-page-layout'] = array(
        'id' => 'blog-page-layout',
        'label'   => __( 'Blog Page Layout', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $choices,
        'default' => 'layout-1'
    );
	
	$choices = array(
		'layout-1' => __('List Layout - Left Thumbnail', 'vt-blogging-pro'),
		'layout-2' => __('List Layout - Right Thumbnail', 'vt-blogging-pro'),
		'1stfull' => __('List Layout - First Fullpost', 'vt-blogging-pro'),
		'standard-list' => __('First Standard + List Layout', 'vt-blogging-pro'),
		'standard' => __('Standard Blog Layout', 'vt-blogging-pro'),
    );
    $options['archive-page-layout'] = array(
        'id' => 'archive-page-layout',
        'label'   => __( 'Archive Page Layout', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $choices,
        'default' => 'layout-1'
    );
	$options['entry-excerpt-length'] = array(
		'id' => 'entry-excerpt-length',
		'label'   => __( 'Number of words to show on excerpt', 'vt-blogging-pro' ),
		'section' => $section,
        'type'    => 'number',
        'default' => 38,
        'description' => __( 'Default: 38', 'vt-blogging-pro' )
	);
    $options['vt-blog-cats'] = array(
        'id' => 'vt-blog-cats',
        'label'   => __( 'Exclude Blog Categories', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'text',
        'description' => __( 'Enter the ID\'s of the post categories you\'d like to EXCLUDE from the Blog, enter only the ID\'s with a minus sign (-) before them, separated by a comma (,)<br />Eg: "-13, -17, -19"<br /><br />If you enter the ID\'s without the minus then it\'ll show ONLY posts in those categories.<br /><br />Get the ID at <b>Posts -> Categories</b>.', 'vt-blogging-pro' )
    );
	
	// Header Settings
    $section = 'vt-site-layout-section-header';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Header', 'vt-blogging-pro' ),
        'priority' => '20',
		'panel' => $panel
    );

    $options['sticky-header'] = array(
        'id' => 'sticky-header',
        'label'   => __( 'Enable Sticky Header', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	
	$options['header-search'] = array(
        'id' => 'header-search',
        'label'   => __( 'Enable Header Searchform', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	
	if (class_exists( 'woocommerce' )) {
        $options['woo-cart-button'] = array(
            'id' => 'woo-cart-button',
            'label'   => __( 'Show WooCommerce Cart', 'vt-blogging-pro' ),
            'section' => $section,
            'type'    => 'checkbox',
            'default' => 1,
        );
	}
	
	// Social Share
    $section = 'vt-blog-section-post';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Single Posts', 'vt-blogging-pro' ),
        'priority' => '50',
        'panel' => $panel
    );

    $options['single-meta-on'] = array(
        'id' => 'single-meta-on',
        'label'   => __( 'Show Meta Info', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
    $options['single-tags-on'] = array(
        'id' => 'single-tags-on',
        'label'   => __( 'Show Posts Tags', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
    $options['social-share-on'] = array(
        'id' => 'social-share-on',
        'label'   => __( 'Show Social Share', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	$options['related-posts-on'] = array(
        'id' => 'related-posts-on',
        'label'   => __( 'Show Related Posts', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	
	// Post Share Settings
    $section = 'vt-blog-section-share';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Social Share', 'vt-blogging-pro' ),
        'priority' => '60',
        'panel' => $panel
    );

    $options['social-share-fb'] = array(
        'id' => 'social-share-fb',
        'label'   => __( 'Show Facebook Share', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
    $options['social-share-tw'] = array(
        'id' => 'social-share-tw',
        'label'   => __( 'Show Twitter Share', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
    $options['social-share-pin'] = array(
        'id' => 'social-share-pin',
        'label'   => __( 'Show Pinterest Share', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	$options['social-share-gplus'] = array(
        'id' => 'social-share-gplus',
        'label'   => __( 'Show Google+ Share', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	$options['social-share-in'] = array(
        'id' => 'social-share-in',
        'label'   => __( 'Show Linkedin Share', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	$options['social-share-mail'] = array(
        'id' => 'social-share-mail',
        'label'   => __( 'Show Email Share', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	
	// Footer Settings
    $section = 'vt-site-layout-section-footer';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Footer', 'vt-blogging-pro' ),
        'priority' => '80',
		'panel' => $panel
    );
	
    $choices = array(
        '2' => __( '2 Column', 'vt-blogging-pro'),
        '3' => __( '3 Column', 'vt-blogging-pro'),
        '4' => __( '4 Column', 'vt-blogging-pro')
    );
    $options['footer-widget-column'] = array(
        'id' => 'footer-widget-column',
        'label'   => __( 'Footer Widget Column', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $choices,
        'default' => '4'
    );
	
	$options['footer-copyright'] = array(
        'id' => 'footer-copyright',
        'label'   => __( 'Footer Copyright Text', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'textarea',
        'default' => ''
    );

    $options['footer-menu-on'] = array(
        'id' => 'footer-menu-on',
        'label'   => __( 'Show Footer Menu', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	
	$options['backtotop-button'] = array(
        'id' => 'backtotop-button',
        'label'   => __( 'Show Backtotop Button', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );

    // WooCommerce style Layout
	if (class_exists( 'woocommerce' )) {
        $section = 'vt-site-layout-section-woocommerce';

        $sections[] = array(
            'id' => $section,
            'title' => __( 'WooCommerce', 'vt-blogging-pro' ),
            'priority' => '90',
            'panel' => $panel
        );
		$options['shop-limit-post'] = array(
			'id' => 'shop-limit-post',
			'label'   => __( 'Shop Posts Product per page', 'vt-blogging-pro' ),
			'section' => $section,
			'type'    => 'number',
			'default' => 12,
		);
        $options['remove-wc-page-titles'] = array(
            'id' => 'remove-wc-page-titles',
            'label'   => __( 'Remove Shop Page Titles', 'vt-blogging-pro' ),
            'section' => $section,
            'type'    => 'checkbox',
            'default' => 0,
        );

	}
	
	/**-----------------
	 * Featured area
	 -----------------*/
	$section = 'featured-area';

	$sections[] = array(
		'id' => $section,
		'title' => __( 'Featured Slider Settings', 'vt-blogging-pro' ),
		'priority' => '40'
	);
	
	$options['featured-slider-on'] = array(
		'id' => 'featured-slider-on',
		'label'   => __( 'Enable Featured Slider', 'vt-blogging-pro' ),
		'section' => $section,
		'type'    => 'checkbox',
		'default' => 1,
	);
    $options['featured-slider-slides'] = array(
        'id' => 'featured-slider-slides',
        'label'   => __( 'Amount of Slides', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'default' => 5,
    );
    $options['featured-slider-category'] = array(
        'id' => 'featured-slider-category',
        'label'   => __( 'Show Post Category', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	$options['featured-slider-date'] = array(
        'id' => 'featured-slider-date',
        'label'   => __( 'Show Post Date', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	$options['featured-slider-share'] = array(
        'id' => 'featured-slider-share',
        'label'   => __( 'Show Social Share', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	$options['featured-slider-readmore'] = array(
        'id' => 'featured-slider-readmore',
        'label'   => __( 'Show Read More Button', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'checkbox',
        'default' => 1,
    );
	$options['slider_cat_color'] = array(
		'id' => 'slider_cat_color',
		'label'   => __( 'Category Title Color', 'vt-blogging-pro' ),
		'section' => $section,
		'type'    => 'color',
		'default' => $secondary_color,
	);
	$options['slider_title_color'] = array(
		'id' => 'slider_title_color',
		'label'   => __( 'Title Color', 'vt-blogging-pro' ),
		'section' => $section,
		'type'    => 'color',
		'default' => $secondary_color,
	);
	$options['slider_title_hover_color'] = array(
		'id' => 'slider_title_hover_color',
		'label'   => __( 'Title Hover Color', 'vt-blogging-pro' ),
		'section' => $section,
		'type'    => 'color',
		'default' => $secondary_color,
	);
	$options['slider_read_more_color'] = array(
		'id' => 'slider_read_more_color',
		'label'   => __( 'Read More Text Color', 'vt-blogging-pro' ),
		'section' => $section,
		'type'    => 'color',
		'default' => $secondary_color,
	);
	$options['slider_arrow_color'] = array(
		'id' => 'slider_arrow_color',
		'label'   => __( 'Arrow Color', 'vt-blogging-pro' ),
		'section' => $section,
		'type'    => 'color',
		'default' => $secondary_color,
	);

	/**-----------------
	 * Color Settings
	 -----------------*/
    $panel = 'vt-colors-settings';

    $panels[] = array(
        'id' => $panel,
        'title' => __( 'Color Settings', 'vt-blogging-pro' ),
        'priority' => '50'
    );
    
    // Colors
    $section = 'colors';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Default Colors', 'vt-blogging-pro' ),
        'priority' => '10',
        'panel' => $panel
    );
    $options['link-color'] = array(
        'id' => 'link-color',
        'label'   => __( 'Link Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => '#444444',
    );
	$options['link-color-hover'] = array(
        'id' => 'link-color-hover',
        'label'   => __( 'Link Hover Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => '#222222',
    );

	// Header
    $section = 'vt-panel-colors-section-header';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Header', 'vt-blogging-pro' ),
        'priority' => '10',
        'panel' => $panel
    );
    $options['header-bg-color'] = array(
        'id' => 'header-bg-color',
        'label'   => __( 'Background Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $header_bg_color,
    );
    $options['header-bg-opacity'] = array(
        'id' => 'header-bg-opacity',
        'label'   => __( 'Header Opacity', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'range',
        'input_attrs' => array(
            'min'   => 0,
            'max'   => 99,
            'step'  => 1,
        ),
        'default' => 99
    );

	// Header Search
    $options['header-search-bg-color'] = array(
        'id' => 'header-search-bg-color',
        'label'   => __( 'Header Search Background Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
		'default' => '#3c4852',
    );
	
	// Navigation
    $section = 'vt-panel-colors-section-nav';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Navigation', 'vt-blogging-pro' ),
        'priority' => '10',
        'panel' => $panel
    );
    
    $options['header-menu-bg-color'] = array(
        'id' => 'header-menu-bg-color',
        'label'   => __( 'Menu Background Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $tertiary_color,
    );
    $options['header-menu-color'] = array(
        'id' => 'header-menu-color',
        'label'   => __( 'Menu Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $secondary_color,
    );
	$options['header-menu-hover-color'] = array(
        'id' => 'header-menu-hover-color',
        'label'   => __( 'Menu Hover Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $secondary_color,
    );
	$options['header-menu-border-color'] = array(
        'id' => 'header-menu-border-color',
        'label'   => __( 'Menu Border Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $primary_color,
    );
    $options['header-submenu-bg-color'] = array(
        'id' => 'header-submenu-bg-color',
        'label'   => __( 'Sub Menu Background Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $secondary_color,
    );
	$options['header-submenu-color'] = array(
        'id' => 'header-submenu-color',
        'label'   => __( 'Submenu Link Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $submenu_link_color,
    );
	$options['header-submenu-hover-color'] = array(
        'id' => 'header-submenu-hover-color',
        'label'   => __( 'Submenu Hover Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $submenu_link_color,
    );
	
	// Widgets
    $section = 'vt-panel-colors-section-widgets';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Widgets', 'vt-blogging-pro' ),
        'priority' => '10',
        'panel' => $panel
    );
	$options['widget-sidebar-title'] = array(
        'id' => 'widget-sidebar-title',
        'label'   => __( 'Sidebar Widget Title Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
		'default' => '#333333'
    );
	$options['widget-sidebar-link'] = array(
        'id' => 'widget-sidebar-link',
        'label'   => __( 'Sidebar Widget Link Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
		'default' => '#333333'
    );
	$options['widget-sidebar-hover-link'] = array(
        'id' => 'widget-sidebar-hover-link',
        'label'   => __( 'Sidebar Widget Link Hover Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
		'default' => $primary_color,
    );
	$options['widget-footer-title'] = array(
        'id' => 'widget-footer-title',
        'label'   => __( 'Footer Widget Title Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
		'default' => $secondary_color,
    );
	$options['widget-footer-link'] = array(
        'id' => 'widget-footer-link',
        'label'   => __( 'Footer Widget Link Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
		'default' => '#9f9f9f'
    );
	$options['widget-footer-hover-link'] = array(
        'id' => 'widget-footer-hover-link',
        'label'   => __( 'Footer Widget Link Hover Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $secondary_color,
    );
	$options['widget-border-color'] = array(
        'id' => 'widget-border-color',
        'label'   => __( 'Widget Title Border Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $primary_color,
    );
	
	// Pagination
    $section = 'vt-panel-colors-section-pagination';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Pagination', 'vt-blogging-pro' ),
        'priority' => '10',
        'panel' => $panel
    );
    $options['navigation-pagination'] = array(
        'id' => 'navigation-pagination',
        'label'   => __( 'Pagination Background Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
		'default' => '#e8edf3'
    );
	$options['navi-text-color'] = array(
        'id' => 'navi-text-color',
        'label'   => __( 'Pagination Text & Number Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
		'default' => '#222222'
    );
	$options['navi-current-bg'] = array(
        'id' => 'navi-current-bg',
        'label'   => __( '"Current & Hover" Background Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
		'default' => $primary_color,
    );
	$options['navi-current-text'] = array(
        'id' => 'navi-current-text',
        'label'   => __( '"Current & Hover" Text Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
		'default' => $secondary_color
    );	
	
	// Footer
    $section = 'vt-panel-colors-section-footer';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Footer', 'vt-blogging-pro' ),
        'priority' => '10',
        'panel' => $panel
    );
    $options['footer-bg-color'] = array(
        'id' => 'footer-bg-color',
        'label'   => __( 'Background Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $footer_bg_color,
    );
    $options['footer-text-color'] = array(
        'id' => 'footer-text-color',
        'label'   => __( 'Footer Text Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $footer_text_color,
    );
	$options['footer-link-color'] = array(
        'id' => 'footer-link-color',
        'label'   => __( 'Footer Link Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $footer_text_color,
    );
	$options['footer-link-hover-color'] = array(
        'id' => 'footer-link-hover-color',
        'label'   => __( 'Footer Link Hover Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $secondary_color,
    );
	
	// Site Bottom
    $options['footer-sitebottom-bg-color'] = array(
        'id' => 'footer-sitebottom-bg-color',
        'label'   => __( 'Site Bottom Bg Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => '#262f3c'
    );
    $options['footer-sitebottom-font-color'] = array(
        'id' => 'footer-sitebottom-font-color',
        'label'   => __( 'Site Bottom Font Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $secondary_color,
    );
	$options['footer-sitebottom-link-color'] = array(
        'id' => 'footer-sitebottom-link-color',
        'label'   => __( 'Site Bottom Link Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => '#888888'
    );
	$options['footer-sitebottom-hover-color'] = array(
        'id' => 'footer-sitebottom-hover-color',
        'label'   => __( 'Site Bottom Link Hover Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
		'default' => $submenu_link_color,
    );
    
	/**-----------------
	 * Font Options
	 -----------------*/
    $panel = 'vt-panel-font-settings';

    $panels[] = array(
        'id' => $panel,
        'title' => __( 'Font Settings', 'vt-blogging-pro' ),
        'priority' => '60'
    );

	// Site Title/Tagline
    $section = 'typography-section';
    $font_choices = customizer_library_get_font_choices();

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Title/Tagline Fonts', 'vt-blogging-pro' ),
        'priority' => '20',
        'panel' => $panel
    );

    $options['site-title-font'] = array(
        'id' => 'site-title-font',
        'label'   => __( 'Site Title Font', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $font_choices,
        'default' => 'Open Sans',
        'description' => __( 'Choose a font name. Default: Open Sans', 'vt-blogging-pro' )
    );
    $options['site-title-font-size'] = array(
        'id' => 'site-title-font-size',
        'label'   => __( 'Site Title Size', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'default' => 28,
        'description' => __( 'Set font-size (without: <strong>px</strong>). Default: 28', 'vt-blogging-pro' )
    );
    $options['site-tagline-font'] = array(
        'id' => 'site-tagline-font',
        'label'   => __( 'Site Tagline Font', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $font_choices,
        'default' => 'Lato'
    );
    $options['tagline-font-size'] = array(
        'id' => 'tagline-font-size',
        'label'   => __( 'Site Tagline Size', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'default' => 12,
    );
    $options['title-bottom-margin'] = array(
        'id' => 'title-bottom-margin',
        'label'   => __( 'Site Title Bottom Margin', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'description' => __( 'This will set the space between the site title and the site tagline', 'vt-blogging-pro' ),
        'default' => 0,
    );

	// Site Fonts
    $section = 'typography-section-default';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Site Fonts', 'vt-blogging-pro' ),
        'priority' => '20',
        'panel' => $panel
    );

    $options['body-font-name'] = array(
        'id' => 'body-font-name',
        'label'   => __( 'Body Font', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $font_choices,
        'default' => 'Open Sans',
        'description' => __( 'Choose a font name. Default: Open Sans', 'vt-blogging-pro' )
    );
	$options['body-font-size'] = array(
        'id' => 'body-font-size',
        'label'   => __( 'Font Size', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'default' => 14,
        'description' => __( 'Set font-size (without: <strong>px</strong>). Default: 14', 'vt-blogging-pro' )
    );
    $options['body-font-color'] = array(
        'id' => 'body-font-color',
        'label'   => __( 'Body Font Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => '#414141'
    );
    $options['heading-font'] = array(
        'id' => 'heading-font',
        'label'   => __( 'Heading Font', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $font_choices,
        'default' => 'Open Sans',
        'description' => __( 'Choose a font name. Default: Open Sans', 'vt-blogging-pro' )
    );
    $options['heading-font-color'] = array(
        'id' => 'heading-font-color',
        'label'   => __( 'Heading Font Color', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'color',
        'default' => $heading_font_color,
    );

	if (class_exists( 'woocommerce' )) {
        $options['wc-product-title-size'] = array(
            'id' => 'wc-product-title-size',
            'label'   => __( 'WooCommerce Product Title Size', 'vt-blogging-pro' ),
            'section' => $section,
            'type'    => 'number',
			'default' => 26,
			'description' => __( 'Set font-size (without: <strong>px</strong>). Default: 26', 'vt-blogging-pro' )
        );
        $options['wc-product-title-color'] = array(
            'id' => 'wc-product-title-color',
            'label'   => __( 'WooCommerce Product Title Color', 'vt-blogging-pro' ),
            'section' => $section,
            'type'    => 'color',
            'default' => $heading_font_color,
        );
    }

	// Menus
    $section = 'typography-section-menus';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Menus', 'vt-blogging-pro' ),
        'priority' => '20',
        'panel' => $panel
    );

    $options['menu-title-font-name'] = array(
        'id' => 'menu-title-font-name',
        'label'   => __( 'Google Font Name', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $font_choices,
        'default' => 'Open Sans',
        'description' => __( 'Choose a font name. Default: Open Sans', 'vt-blogging-pro' )
    );
	
    $choices = array(
        'ultra-light' => __( 'Ultra Light', 'vt-blogging-pro' ),
        'lighter' => __( 'Lighter', 'vt-blogging-pro' ),
        'book' => __( 'Book', 'vt-blogging-pro' ),
        'regular' => __( 'Regular', 'vt-blogging-pro' ),
        'medium' => __( 'Medium', 'vt-blogging-pro' ),
        'semi-bold' => __( 'Semi-Bold', 'vt-blogging-pro' ),
        'bold' => __( 'Bold', 'vt-blogging-pro' ),
        'extra-bold' => __( 'Extra Bold', 'vt-blogging-pro' ),
        'ultra-bold' => __( 'Ultra Bold', 'vt-blogging-pro' )
    );
	$options['menu-title-font-weight'] = array(
        'id' => 'menu-title-font-weight',
        'label'   => __( 'Font Weight', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $choices,
        'default' => 'regular',
        'description' => __( 'Set font-weight. Default: 400 (Regular). Important: Not all fonts support every font-weight.', 'vt-blogging-pro' )
    );
	$options['menu-title-font-size'] = array(
        'id' => 'menu-title-font-size',
        'label'   => __( 'Font Size', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'default' => 14,
        'description' => __( 'Set font-size (without: <strong>px</strong>). Default: 14', 'vt-blogging-pro' )
    );
	
    $choices = array(
        'normal' => __( 'Normal', 'vt-blogging-pro' ),
        'italic' => __( 'Italic', 'vt-blogging-pro' )
    );
	$options['menu-title-font-style'] = array(
        'id' => 'menu-title-font-style',
        'label'   => __( 'Font Style', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $choices,
        'default' => 'normal',
        'description' => __( 'Set font-style. Default: Normal', 'vt-blogging-pro' )
    );
	
	$choices = array(
        'capitalize' => __( 'Capitalize', 'vt-blogging-pro' ),
        'lowercase' => __( 'Lowercase', 'vt-blogging-pro' ),
        'uppercase' => __( 'Uppercase', 'vt-blogging-pro' )
    );
	$options['menu-title-text-transform'] = array(
        'id' => 'menu-title-text-transform',
        'label'   => __( 'Text Transform', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $choices,
        'default' => 'uppercase',
        'description' => __( 'Set text-transform. Default: Uppercase', 'vt-blogging-pro' )
    );
	
	$options['menu-title-letter-spacing'] = array(
        'id' => 'menu-title-letter-spacing',
        'label'   => __( 'Letter Spacing', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'default' => 0,
		'description' => __( 'Set letter-spacing (without: <strong>px</strong>). Default: 0', 'vt-blogging-pro' )
    );
	
	// Posts/Pages Title
    $section = 'typography-section-page';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Posts/Pages Title', 'vt-blogging-pro' ),
        'priority' => '20',
        'panel' => $panel
    );

    $options['post-title-font-name'] = array(
        'id' => 'post-title-font-name',
        'label'   => __( 'Google Font Name', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $font_choices,
        'default' => 'Open Sans',
        'description' => __( 'Choose a font name. Default: Open Sans', 'vt-blogging-pro' )
    );
	
    $choices = array(
        'ultra-light' => __( 'Ultra Light', 'vt-blogging-pro' ),
        'lighter' => __( 'Lighter', 'vt-blogging-pro' ),
        'book' => __( 'Book', 'vt-blogging-pro' ),
        'regular' => __( 'Regular', 'vt-blogging-pro' ),
        'medium' => __( 'Medium', 'vt-blogging-pro' ),
        'semi-bold' => __( 'Semi-Bold', 'vt-blogging-pro' ),
        'bold' => __( 'Bold', 'vt-blogging-pro' ),
        'extra-bold' => __( 'Extra Bold', 'vt-blogging-pro' ),
        'ultra-bold' => __( 'Ultra Bold', 'vt-blogging-pro' )
    );
	$options['post-title-font-weight'] = array(
        'id' => 'post-title-font-weight',
        'label'   => __( 'Font Weight', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $choices,
        'default' => 'font-bold',
        'description' => __( 'Set font-weight. Default: 600. Important: Not all fonts support every font-weight.', 'vt-blogging-pro' )
    );
	
	$options['post-title-font-size'] = array(
        'id' => 'post-title-font-size',
        'label'   => __( 'Font Size', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'default' => 26,
		'description' => __( 'Set font-size (without: <strong>px</strong>). Default: 26', 'vt-blogging-pro' )
    );
	
    $choices = array(
        'normal' => __( 'Normal', 'vt-blogging-pro' ),
        'italic' => __( 'Italic', 'vt-blogging-pro' )
    );
	$options['post-title-font-style'] = array(
        'id' => 'post-title-font-style',
        'label'   => __( 'Font Style', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $choices,
        'default' => 'normal',
        'description' => __( 'Set font-style. Default: Normal', 'vt-blogging-pro' )
    );
	
	$choices = array(
        'capitalize' => __( 'Capitalize', 'vt-blogging-pro' ),
        'lowercase' => __( 'Lowercase', 'vt-blogging-pro' ),
        'uppercase' => __( 'Uppercase', 'vt-blogging-pro' )
    );
	$options['post-title-text-transform'] = array(
        'id' => 'post-title-text-transform',
        'label'   => __( 'Text Transform', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $choices,
        'default' => 'capitalize',
        'description' => __( 'Set text-transform. Default: Capitalize', 'vt-blogging-pro' )
    );
	
	$options['post-title-letter-spacing'] = array(
        'id' => 'post-title-letter-spacing',
        'label'   => __( 'Letter Spacing', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'default' => 0,
		'description' => __( 'Set letter-spacing (without: <strong>px</strong>). Default: 0', 'vt-blogging-pro' )
    );

	// Widget Title
    $section = 'typography-section-widget';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'Widget Title', 'vt-blogging-pro' ),
        'priority' => '20',
        'panel' => $panel
    );

    $options['widget-title-font-name'] = array(
        'id' => 'widget-title-font-name',
        'label'   => __( 'Google Font Name', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $font_choices,
        'default' => 'Open Sans',
        'description' => __( 'Choose a font name. Default: Open Sans', 'vt-blogging-pro' )
    );
	
    $choices = array(
        'ultra-light' => __( 'Ultra Light', 'vt-blogging-pro' ),
        'lighter' => __( 'Lighter', 'vt-blogging-pro' ),
        'book' => __( 'Book', 'vt-blogging-pro' ),
        'regular' => __( 'Regular', 'vt-blogging-pro' ),
        'medium' => __( 'Medium', 'vt-blogging-pro' ),
        'semi-bold' => __( 'Semi-Bold', 'vt-blogging-pro' ),
        'bold' => __( 'Bold', 'vt-blogging-pro' ),
        'extra-bold' => __( 'Extra Bold', 'vt-blogging-pro' ),
        'ultra-bold' => __( 'Ultra Bold', 'vt-blogging-pro' )
    );
	$options['widget-title-font-weight'] = array(
        'id' => 'widget-title-font-weight',
        'label'   => __( 'Font Weight', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $choices,
        'default' => 'bold',
        'description' => __( 'Set font-weight. Default: 600. Important: Not all fonts support every font-weight.', 'vt-blogging-pro' )
    );
	
	$options['widget-title-font-size'] = array(
        'id' => 'widget-title-font-size',
        'label'   => __( 'Font Size', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'default' => 18,
		'description' => __( 'Set font-size (without: <strong>px</strong>). Default: 18', 'vt-blogging-pro' )
    );
	
    $choices = array(
        'normal' => __( 'Normal', 'vt-blogging-pro' ),
        'italic' => __( 'Italic', 'vt-blogging-pro' )
    );
	$options['widget-title-font-style'] = array(
        'id' => 'widget-title-font-style',
        'label'   => __( 'Font Style', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $choices,
        'default' => 'normal',
        'description' => __( 'Set font-style. Default: Normal', 'vt-blogging-pro' )
    );
	
	$choices = array(
        'capitalize' => __( 'Capitalize', 'vt-blogging-pro' ),
        'lowercase' => __( 'Lowercase', 'vt-blogging-pro' ),
        'uppercase' => __( 'Uppercase', 'vt-blogging-pro' )
    );
	$options['widget-title-text-transform'] = array(
        'id' => 'widget-title-text-transform',
        'label'   => __( 'Text Transform', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'select',
        'choices' => $choices,
        'default' => 'capitalize',
        'description' => __( 'Set text-transform. Default: Capitalize', 'vt-blogging-pro' )
    );
	
	$options['widget-title-letter-spacing'] = array(
        'id' => 'widget-title-letter-spacing',
        'label'   => __( 'Letter Spacing', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'number',
        'default' => 0,
		'description' => __( 'Set letter-spacing (without: <strong>px</strong>). Default: 0', 'vt-blogging-pro' )
    );

	/**-----------------
	 * Theme Text
	 -----------------*/
    $panel = 'vt-panel-text';

    $panels[] = array(
        'id' => $panel,
        'title' => __( 'Override Theme Text', 'vt-blogging-pro' ),
        'priority' => '70'
    );

	// 404
    $section = 'vt-website-section-error-text';
    
    $sections[] = array(
        'id' => $section,
        'title' => __( '404 Error', 'vt-blogging-pro' ),
        'priority' => '30',
        'panel' => $panel
    );

    $options['error-page-heading'] = array(
        'id' => 'error-page-heading',
        'label'   => __( '404 Error Page Heading', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'text',
        'default' => __( 'Oops! <span>404</span> Error - Page Not Found', 'vt-blogging-pro')
    );
    $options['error-page-msg'] = array(
        'id' => 'verror-page-msg',
        'label'   => __( 'Error 404 Message', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'textarea',
        'default' => __( 'Sorry, but it looks like nothing was found at this location. You may try one of the links below or do a search.', 'vt-blogging-pro')
    );
	
	// No Result
    $section = 'vt-website-section-no-result';

    $sections[] = array(
        'id' => $section,
        'title' => __( 'No Result', 'vt-blogging-pro' ),
        'priority' => '40',
        'panel' => $panel
    );
    $options['website-nosearch-head'] = array(
        'id' => 'website-nosearch-head',
        'label'   => __( 'No Search Results Heading', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'text',
        'default' => __( 'Nothing Found', 'vt-blogging-pro')
    );
    $options['website-nosearch-msg'] = array(
        'id' => 'website-nosearch-msg',
        'label'   => __( 'No Search Results Message', 'vt-blogging-pro' ),
        'section' => $section,
        'type'    => 'textarea',
        'default' => __( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'vt-blogging-pro')
    );


	// Adds the sections to the $options array
	$options['sections'] = $sections;

	// Adds the panels to the $options array
	$options['panels'] = $panels;

	$customizer_library = Customizer_Library::Instance();
	$customizer_library->add_options( $options );

	// To delete custom mods use: customizer_library_remove_theme_mods();

}
add_action( 'init', 'customizer_library_vt_blogging_pro_options' );


function vt_blogging_pro_register_theme_customizer( $wp_customize ){

    // Featured area - Category	
	$wp_customize->add_setting( 'slider-cats', array('default' => '','sanitize_callback' => 'vt_blogging_pro_sanitize_default') );
	$wp_customize->add_control(
		new WP_Customize_Category_Control(
			$wp_customize,
			'slider-cats',
			array(
				'label'		 => __('Select Featured Category', 'vt-blogging-pro'),
				'settings' 	 => 'slider-cats',
				'section'	 => 'featured-area',
				'priority'	 => 28
			)
		)
	);	
	
    // Custom Scripts	
	$wp_customize->add_section( 'vt_blogging_pro_scripts_section' , array(
	    'title'       => __( 'Custom Scripts', 'vt-blogging-pro' ),
		'description' => __( '- Use Header Script (For example: meta tag verification, google fonts url,etc) <br>They will be added before < /head> tag. <br><br> - Use Footer Script (For example: google analytic, histats,etc)<br> They will be added before < /body> tag', 'vt-blogging-pro' ),
	) );

	$wp_customize->add_setting( 'vt_blogging_pro_header_scripts', array('default' => '', 'sanitize_callback' => 'vt_blogging_pro_sanitize_text'));
	$wp_customize->add_control(
		new Customize_CustomCss_Control(
	        $wp_customize,
	        'vt_blogging_pro_header_scripts',
	        array(
				'label' => __( 'Header Scripts', 'vt-blogging-pro' ),
				'description' => __('If you need to add custom scripts to your header (meta tag verification, google fonts url,etc), you should enter them in the box. They will be added before </head> tag', 'vt-blogging-pro'),
	            'section' => 'vt_blogging_pro_scripts_section',
	            'settings' => 'vt_blogging_pro_header_scripts'
	        )
	    )
	);
	
	$wp_customize->add_setting( 'vt_blogging_pro_footer_scripts', array('default' => '', 'sanitize_callback' => 'vt_blogging_pro_sanitize_text'));
	$wp_customize->add_control(
		new Customize_CustomCss_Control(
	        $wp_customize,
	        'vt_blogging_pro_footer_scripts',
	        array(
				'label' => __( 'Footer Scripts', 'vt-blogging-pro' ),
	            'section' => 'vt_blogging_pro_scripts_section',
	            'settings' => 'vt_blogging_pro_footer_scripts'
	        )
	    )
	);
	
}
add_action( 'customize_register', 'vt_blogging_pro_register_theme_customizer' );

/**
 * Sanitize functions.
 */
function vt_blogging_pro_sanitize_default( $value ) {
	return $value;
}

/*
 * Sanitize functions.
 */
function vt_blogging_pro_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

/**
 * Header Code
 *
 * @since vt-blogging-pro 1.0
 */
if ( ! function_exists( 'vt_blogging_pro_apply_header' ) ) :
  	
  	function vt_blogging_pro_apply_header() {	
		if ( get_theme_mod('vt_blogging_pro_header_scripts') ) { 
		?>

		<script id="vt-blogging-pro-custom-scripts">
			<?php if ( get_theme_mod('vt_blogging_pro_header_scripts') ) : ?>
				<?php echo get_theme_mod('vt_blogging_pro_header_scripts'); ?>
			<?php endif; ?>
		</script>
		<?php
    }
}
endif;
add_action('wp_head', 'vt_blogging_pro_apply_header');

/**
 * Footer Code
 *
 * @since vt-blogging-pro 1.0
 */
if ( ! function_exists( 'vt_blogging_pro_apply_footer' ) ) :
  	
  	function vt_blogging_pro_apply_footer() {	
		if ( get_theme_mod('vt_blogging_pro_footer_scripts') ) { 
		?>

		<script id="vt-blogging-pro-custom-scripts">
			<?php if ( get_theme_mod('vt_blogging_pro_footer_scripts') ) : ?>
				<?php echo get_theme_mod('vt_blogging_pro_footer_scripts'); ?>
			<?php endif; ?>
		</script>
		<?php
    }
}
endif;
add_action('wp_footer', 'vt_blogging_pro_apply_footer');