<?php
/**
 * Functions used to implement options
 *
 * @package Customizer Library VT Blogging Pro
 */

/**
 * Enqueue Google Fonts Example
 */
function customizer_vt_blogging_pro_fonts() {

	// Font options
	$fonts = array(
		get_theme_mod( 'site-title-font', customizer_library_get_default( 'site-title-font' ) ),
		get_theme_mod( 'body-font-name', customizer_library_get_default( 'body-font-name' ) ),
		get_theme_mod( 'heading-font-name', customizer_library_get_default( 'heading-font-name' ) ),
		get_theme_mod( 'menu-title-font-name', customizer_library_get_default( 'menu-title-font-name' ) ),
		get_theme_mod( 'post-title-font-name', customizer_library_get_default( 'post-title-font-name' ) ),
		get_theme_mod( 'widget-title-font-name', customizer_library_get_default( 'widget-title-font-name' ) ),
		get_theme_mod( 'wc-product-title-size', customizer_library_get_default( 'wc-product-title-size' ) )

	);

	$font_uri = customizer_library_get_google_font_uri( $fonts );

	// Load Google Fonts
	wp_enqueue_style( 'customizer_vt_blogging_pro_fonts', $font_uri, array(), null, 'screen' );

}
add_action( 'wp_enqueue_scripts', 'customizer_vt_blogging_pro_fonts' );