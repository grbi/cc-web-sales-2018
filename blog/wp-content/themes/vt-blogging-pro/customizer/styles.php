<?php
/**
 * Implements styles set in the theme customizer
 *
 * @package Customizer Library VT Blogging Pro
 */

if ( ! function_exists( 'customizer_library_vt_blogging_pro_build_styles' ) && class_exists( 'Customizer_Library_Styles' ) ) :
/**
 * Process user options to generate CSS needed to implement the choices.
 *
 * @since  1.0.0.
 *
 * @return void
 */
function customizer_library_vt_blogging_pro_build_styles() {

	/* -------------------------------------------------- */
	/* Logo
	/* -------------------------------------------------- */
	// Site Logo Top Padding
	$setting = 'logo-height-size';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$logo_height_size = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'@media only screen and (min-width: 480px) {.custom-logo'
			),
			'declarations' => array(
				'max-height' => $logo_height_size . 'px'
			)
		) ); 
	}
	
	// Site Logo Max Width
	$setting = 'logo-max-width';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$logo_max_width = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'@media only screen and (min-width: 480px) {.custom-logo '
			),
			'declarations' => array(
				'max-width' => $logo_max_width . 'px' . '}'
			)
		) );
	}
	

	
	/* -------------------------------------------------- */
	/* Color Settings
	/* -------------------------------------------------- */

	// Featured Area
	$setting = 'slider_cat_color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.slide-item-text .post-cats > a'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	$setting = 'slider_title_color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.slide-item .post-title a'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	$setting = 'slider_title_hover_color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.slide-item .post-title a:hover'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	$setting = 'slider_read_more_color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.slide-item .feat-more'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	$setting = 'slider_arrow_color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.owl-prev, .owl-next'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// Link Color
	$setting = 'link-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'a,
				.content-loop .read-more a:link, .content-loop .read-more a:visited,
				.entry-footer a,
				.tagcloud a:hover:before,
				.post-edit-link::before, .edit-link a,
				.entry-title a, .entry-title a:visited'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// Link Hover Color
	$setting = 'link-color-hover';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'a:hover,
				.content-loop .read-more a:hover,
				.entry-footer a:hover,
				.tagcloud a:hover:before,
				.post-edit-link::before, .edit-link a:hover,
				.entry-title a:hover'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// Header Background Color
	$setting = 'header-bg-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-header'
			),
			'declarations' => array(
				'background-color' => $color
			)
		) );
	}
	
	// Transparent Header Background Color
	$setting = 'header-bg-color';
	$setting_opacity = 'header-bg-opacity';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );
	$mod_opacity = get_theme_mod( $setting_opacity, customizer_library_get_default( $setting_opacity ) );

	if ( $mod !== customizer_library_get_default( $setting ) || $mod_opacity !== customizer_library_get_default( $setting_opacity ) ) {

		$color = sanitize_hex_color( $mod );
		$rgba_color = customizer_library_hex_to_rgb( $color );
		$opacity = esc_attr( $mod_opacity );
		
		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-header'
			),
			'declarations' => array(
				'background-color' => 'rgba(' . $rgba_color['r'] . ', ' . $rgba_color['g'] . ', ' . $rgba_color['b'] . ', 0.' . $opacity . ');'
			)
		) );
	}
	
	// Header Search Background Color
	$setting = 'header-search-bg-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.header-search'
			),
			'declarations' => array(
				'background-color' => $color
			)
		) );
	}
	
	// Menu Navigation 
	$setting = 'header-menu-bg-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu li.sfHover a, .sf-menu li.current-menu-item a, .sf-menu li.current-menu-item a:hover'
			),
			'declarations' => array(
				'background-color' => $color
			)
		) );
	}
	
	// Menu Background Color 
	$setting = 'header-menu-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu li a,
				 .sf-menu li li a'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// Menu Hover Color 
	$setting = 'header-menu-hover-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu li a:hover'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// Menu Border Color 
	$setting = 'header-menu-border-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu li a::before'
			),
			'declarations' => array(
				'border-bottom' => '3px solid ' . $color
			)
		) );
	}
	
	// Sub Menu Navigation 
	$setting = 'header-submenu-bg-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu ul'
			),
			'declarations' => array(
				'background-color' => $color
			)
		) );
	}
	
	// Submenu Link Color 
	$setting = 'header-submenu-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu li li a'
			),
			'declarations' => array(
				'color' => $color . '!important'
			)
		) );
	}
	
	// Submenu Hover Color 
	$setting = 'header-submenu-hover-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu li .sub-menu li a:hover'
			),
			'declarations' => array(
				'color' => $color . '!important'
			)
		) );
	}
	
	/* -------------------------------------------------- */
	/* Widgets
	/* -------------------------------------------------- */
	$setting = 'widget-sidebar-title';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sidebar .widget-title span'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	$setting = 'widget-sidebar-link';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sidebar .widget ul > li a,
				 .sidebar .widget a'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	$setting = 'widget-sidebar-hover-link';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sidebar .widget ul > li a:hover'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	$setting = 'widget-footer-title';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-footer .widget-title'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	$setting = 'widget-footer-link';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-footer .widget ul > li a'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}	
	
	$setting = 'widget-footer-hover-link';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-footer .widget ul > li a:hover'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	$setting = 'widget-border-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sidebar .widget-title span, .footer-columns .widget-title span'
			),
			'declarations' => array(
				'border-bottom' => '3px solid ' . $color
			)
		) );
	}
	
	// Pagination
	$setting = 'navigation-pagination';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.pagination .page-numbers'
			),
			'declarations' => array(
				'background-color' => $color
			)
		) );
	}
	
	// Pagination Text & Number Color
	$setting = 'navi-text-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.pagination .page-numbers'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// Pagination "Current & Hover" Bg Color
	$setting = 'navi-current-bg';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.pagination .page-numbers:hover,
				 .pagination .page-numbers.current'
			),
			'declarations' => array(
				'background-color' => $color
			)
		) );
	}
	// Woo Pagination Bg Color
	$setting = 'navi-current-bg';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.woocommerce nav.woocommerce-pagination ul li a:focus,
				 .woocommerce nav.woocommerce-pagination ul li a:hover,
				 .woocommerce nav.woocommerce-pagination ul li span.current'
			),
			'declarations' => array(
				'background-color' => $color .'!important'
			)
		) );
	}
	
	// Pagination "Current & Hover" Text Color
	$setting = 'navi-current-text';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.pagination .page-numbers:hover,
				 .pagination .page-numbers.current'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	// Woo Pagination "Current & Hover" Text Color
	$setting = 'navi-current-text';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.woocommerce nav.woocommerce-pagination ul li a:focus,
				 .woocommerce nav.woocommerce-pagination ul li a:hover,
				 .woocommerce nav.woocommerce-pagination ul li span.current'
			),
			'declarations' => array(
				'color' => $color . '!important'
			)
		) );
	}
	
	// Footer Background Color
	$setting = 'footer-bg-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.footer-columns'
			),
			'declarations' => array(
				'background-color' => $color
			)
		) );
	}
	
	// Footer Text Color
	$setting = 'footer-text-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-footer .textwidget,
				.site-footer .widget p,
				.site-footer .entry-meta'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// Footer Link Color
	$setting = 'footer-link-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-footer .widget a'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// Footer Link Hover Color
	$setting = 'footer-link-hover-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-footer .widget ul > li a:hover'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// Footer Bottom Bar Background Color
	$setting = 'footer-sitebottom-bg-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'#site-bottom'
			),
			'declarations' => array(
				'background-color' => $color
			)
		) );
	}
	
	// Footer Site Bottom Font Color
	$setting = 'footer-sitebottom-font-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'#site-bottom .site-info,
				 #site-bottom .footer-nav li a'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// Footer Site Bottom Link Color
	$setting = 'footer-sitebottom-link-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'#site-bottom .site-info a'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// Footer Site Bottom Link Hover Color
	$setting = 'footer-sitebottom-hover-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'#site-bottom .site-info a:hover'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	/* -------------------------------------------------- */
	/* Fonts
	/* -------------------------------------------------- */
	// Site Title Font
	$setting = 'site-title-font';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );
	$stack = customizer_library_get_font_stack( $mod );

	if ( $mod != customizer_library_get_default( $setting ) ) {

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-title a'
			),
			'declarations' => array(
				'font-family' => $stack
			)
		) );

	}
	// Site Title Font Size
	$setting = 'site-title-font-size';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$title_font_size = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-title'
			),
			'declarations' => array(
				'font-size' => $title_font_size . 'px'
			)
		) );
	}
	// Site Tagline Font
	$setting = 'tagline-font';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );
	$stack = customizer_library_get_font_stack( $mod );

	if ( $mod != customizer_library_get_default( $setting ) ) {

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-description'
			),
			'declarations' => array(
				'font-family' => $stack
			)
		) );

	}
	// Site Title Font Size
	$setting = 'tagline-font-size';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$title_font_size = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-description'
			),
			'declarations' => array(
				'font-size' => $title_font_size . 'px'
			)
		) );
	}
	// Site Title Bottom Margin
	$setting = 'title-bottom-margin';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$title_bottom_margin = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.site-title'
			),
			'declarations' => array(
				'margin-bottom' => $title_bottom_margin . 'px'
			)
		) );
	}
	
	// Body Font Name
	$setting = 'body-font-name';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );
	$stack = customizer_library_get_font_stack( $mod );

	if ( $mod != customizer_library_get_default( $setting ) ) {

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'body, .content-loop .entry-summary'
			),
			'declarations' => array(
				'font-family' => $stack
			)
		) );

	}
	
	// Body Font Size
	$setting = 'body-font-size';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$title_font_size = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'body, .content-loop .entry-summary'
			),
			'declarations' => array(
				'font-size' => $title_font_size . 'px'
			)
		) );
	}
	
	// Body Font Color
	$setting = 'body-font-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'body, .content-loop .entry-summary'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// Heading Font
	$setting = 'heading-font';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );
	$stack = customizer_library_get_font_stack( $mod );

	if ( $mod != customizer_library_get_default( $setting ) ) {

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'h1, h2, h3, h4, h5, h6, h1 a, h2 a, h3 a, h4 a, h5 a, h6 a, .widget-area .widget-title'
			),
			'declarations' => array(
				'font-family' => $stack
			)
		) );

	}
	
	// Heading Font Color
	$setting = 'heading-font-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'h1, h2, h3, h4, h5, h6, h1 a, h2 a, h3 a, h4 a, h5 a, h6 a, .widget-area .widget-title'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}
	
	// WooCommerce Product Title Size
	$setting = 'wc-product-title-size';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$title_font_size = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.woocommerce-loop-product__title, .woocommerce div.product .product_title'
			),
			'declarations' => array(
				'font-size' => $title_font_size . 'px'
			)
		) );
	}
	
	// WooCommerce Product Title Color
	$setting = 'wc-product-title-color';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$color = sanitize_hex_color( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.woocommerce-loop-product__title, .woocommerce div.product .product_title'
			),
			'declarations' => array(
				'color' => $color
			)
		) );
	}

	/**-----------------
	 * Menus
	 -----------------*/
	$setting = 'menu-title-font-name';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );
	$stack = customizer_library_get_font_stack( $mod );
	
	if ( $mod != customizer_library_get_default( $setting ) ) {

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu li a'
			),
			'declarations' => array(
				'font-family' => $stack
			)
		) );
	}
	
	// Menu Font Weight
	$setting = 'menu-title-font-weight';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$font_weight = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu li a'
			),
			'declarations' => array(
				'font-weight' => $font_weight 
			)
		) );
	}
	
	// Menu Font Size
	$setting = 'menu-title-font-size';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod != customizer_library_get_default( $setting ) ) {

		$title_font_size = esc_attr( $mod );
		
		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu li a, .sf-menu li li a'
			),
			'declarations' => array(
				'font-size' => $title_font_size . 'px'
			)
		) );
	}
	
	// Menu Font Style
	$setting = 'menu-title-font-style';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$font_style = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu li a'
			),
			'declarations' => array(
				'font-style' => $font_style 
			)
		) );
	}
	
	// Menu Text Transform
	$setting = 'menu-title-text-transform';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$text_transform = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu li a'
			),
			'declarations' => array(
				'text-transform' => $text_transform 
			)
		) );
	}
	
	// Menu Letter Spacing
	$setting = 'menu-title-letter-spacing';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod != customizer_library_get_default( $setting ) ) {

		$letter_spacing = esc_attr( $mod );
		
		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sf-menu li a'
			),
			'declarations' => array(
				'letter-spacing' => $letter_spacing . 'px'
			)
		) );
	}
	
	/**-----------------
	 * Posts/Pages Title 
	 -----------------*/
	$setting = 'post-title-font-name';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );
	$stack = customizer_library_get_font_stack( $mod );
	
	if ( $mod != customizer_library_get_default( $setting ) ) {

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.entry-title, .entry-title a, .page-title, .slide-item .post-title a'
			),
			'declarations' => array(
				'font-family' => $stack
			)
		) );
	}
	
	// Posts/Pages Font Weight
	$setting = 'post-title-font-weight';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$font_weight = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.entry-title, .entry-title a, .page-title, .slide-item .post-title a'
			),
			'declarations' => array(
				'font-weight' => $font_weight 
			)
		) );
	}
	
	// Posts/Pages Font Size
	$setting = 'post-title-font-size';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod != customizer_library_get_default( $setting ) ) {

		$title_font_size = esc_attr( $mod );
		
		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.entry-title, .entry-title a, .page-title, .slide-item .post-title a'
			),
			'declarations' => array(
				'font-size' => $title_font_size . 'px'
			)
		) );
	}
	
	// Posts/Pages Font Style
	$setting = 'post-title-font-style';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$font_style = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.entry-title, .entry-title a, .page-title, .slide-item .post-title a'
			),
			'declarations' => array(
				'font-style' => $font_style 
			)
		) );
	}
	
	// Posts/Pages Text Transform
	$setting = 'post-title-text-transform';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$text_transform = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.entry-title, .entry-title a, .page-title, .slide-item .post-title a'
			),
			'declarations' => array(
				'text-transform' => $text_transform 
			)
		) );
	}
	
	// Posts/Pages Letter Spacing
	$setting = 'post-title-letter-spacing';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod != customizer_library_get_default( $setting ) ) {

		$letter_spacing = esc_attr( $mod );
		
		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.entry-title, .entry-title a, .page-title, .slide-item .post-title a'
			),
			'declarations' => array(
				'letter-spacing' => $letter_spacing . 'px'
			)
		) );
	}
	
	/**-----------------
	 * Widgets
	 -----------------*/
	$setting = 'widget-title-font-name';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );
	$stack = customizer_library_get_font_stack( $mod );
	
	if ( $mod != customizer_library_get_default( $setting ) ) {

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sidebar .widget-title span,
				 .footer-columns .widget-title span'
			),
			'declarations' => array(
				'font-family' => $stack
			)
		) );
	}
	
	// Widget Font Weight
	$setting = 'widget-title-font-weight';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$font_weight = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sidebar .widget-title span,
				 .footer-columns .widget-title span'
			),
			'declarations' => array(
				'font-weight' => $font_weight 
			)
		) );
	}
	
	// Widget Font Size
	$setting = 'widget-title-font-size';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod != customizer_library_get_default( $setting ) ) {

		$title_font_size = esc_attr( $mod );
		
		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sidebar .widget-title span,
				 .footer-columns .widget-title span'
			),
			'declarations' => array(
				'font-size' => $title_font_size . 'px'
			)
		) );
	}
	
	// Widget Style
	$setting = 'widget-title-font-style';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$font_style = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sidebar .widget-title span,
				 .footer-columns .widget-title span'
			),
			'declarations' => array(
				'font-style' => $font_style 
			)
		) );
	}
	
	// Widget Text Transform
	$setting = 'widget-title-text-transform';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod !== customizer_library_get_default( $setting ) ) {

		$text_transform = esc_attr( $mod );

		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sidebar .widget-title span,
				 .footer-columns .widget-title span'
			),
			'declarations' => array(
				'text-transform' => $text_transform 
			)
		) );
	}
	
	// Widget Letter Spacing
	$setting = 'widget-title-letter-spacing';
	$mod = get_theme_mod( $setting, customizer_library_get_default( $setting ) );

	if ( $mod != customizer_library_get_default( $setting ) ) {

		$letter_spacing = esc_attr( $mod );
		
		Customizer_Library_Styles()->add( array(
			'selectors' => array(
				'.sidebar .widget-title span,
				 .footer-columns .widget-title span'
			),
			'declarations' => array(
				'letter-spacing' => $letter_spacing . 'px'
			)
		) );
	}
	
}
endif;

add_action( 'customizer_library_styles', 'customizer_library_vt_blogging_pro_build_styles' );

if ( ! function_exists( 'customizer_library_vt_blogging_pro_styles' ) ) :
/**
 * Generates the style tag and CSS needed for the theme options.
 *
 * By using the "Customizer_Library_Styles" filter, different components can print CSS in the header.
 * It is organized this way to ensure there is only one "style" tag.
 *
 * @since  1.0.0.
 *
 * @return void
 */
function customizer_library_vt_blogging_pro_styles() {

	do_action( 'customizer_library_styles' );

	// Echo the rules
	$css = Customizer_Library_Styles()->build();

	if ( ! empty( $css ) ) {
		echo "\n<!-- Begin Custom CSS -->\n<style type=\"text/css\" id=\"vt-blogging-pro-custom-css\">\n";
		echo $css;
		echo "\n</style>\n<!-- End Custom CSS -->\n";
	}
}
endif;

add_action( 'wp_head', 'customizer_library_vt_blogging_pro_styles', 11 );