<?php
	if (get_theme_mod('featured-slider-on', 1) == 1) :
	$featured_cat = get_theme_mod( 'slider-cats' );
	$get_featured_posts = get_theme_mod('');
	$number = get_theme_mod( 'featured-slider-slides' );
		
	if($get_featured_posts) {
		$featured_posts = explode(',', $get_featured_posts);
		$args = array( 'showposts' => $number, 'post_type' => array('post', 'page'), 'post__in' => $featured_posts, 'orderby' => 'post__in' );
	} else {
		$args = array( 'cat' => $featured_cat, 'showposts' => $number );
	}				
?>			

<?php
	$feat_query = new WP_Query( $args );
	if ($feat_query->have_posts()) :
?>
				
	<div class="featured-area">
		<div class="slider">
		<?php while ($feat_query->have_posts()) :
			$feat_query->the_post();
			$image_featured = vt_blogging_pro_resize_image( get_post_thumbnail_id($post->ID) , wp_get_attachment_thumb_url(), 700, 400, true, true );
			$image_featured = $image_featured['url'];
			$pin_image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
		?>
			<div class="slide-item post" style="background-image: url(<?php echo esc_url($image_featured); ?>);">
                <div class="slide-item-text">
        			<div class="post-text-inner">
						<?php if (get_theme_mod('featured-slider-category', 1) == 1) : ?>
							<p class="post-cats"><?php the_category(', '); ?></p>
						<?php endif; ?>
            			<h4 class="post-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
                        <div class="post-meta">
						<?php
							if ( get_theme_mod('featured-slider-date', true) ) : 
								get_template_part('template-parts/date', 'format');
							endif;

							if ( get_theme_mod('featured-slider-share', true) ) : 
								get_template_part('template-parts/entry', 'share');
							endif;
						?>
						</div>
                        <div class="entry-summary">
                            <p><?php vt_blogging_pro_the_excerpt_max_charlength(150); ?></p>
                        </div>
						<?php if (get_theme_mod('featured-slider-readmore', 1) == 1) : ?>
						<a class="feat-more" href="<?php the_permalink() ?>"><?php _e( 'Continue Reading', 'vt-blogging-pro' ); ?></a>
						<?php endif; ?>
                    </div>
        		</div>
			</div>		
		<?php endwhile; ?>
        </div>
    </div>
<?php
	endif;
	endif;
?>