<time>
	<?php 
		$archive_year  = get_the_time( 'Y' ); 
		$archive_month = get_the_time( 'm' ); 
		$archive_day   = get_the_time( 'd' ); 
	?>
	<a href="<?php echo get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d') ); ?> ">
		<i class="fa fa-clock-o"></i>
		<?php the_time(get_option('date_format')); ?>
	</a>
</time>