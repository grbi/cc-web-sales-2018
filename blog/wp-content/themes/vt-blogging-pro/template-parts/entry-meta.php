<?php if ( !get_theme_mod( 'single-meta-on', 1) == 0) : ?>
	<div class="entry-meta clear">
		<span class="entry-author"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 48 ); ?></a> <?php esc_html_e('Posted by', 'vt-blogging-pro'); ?> <?php the_author_posts_link(); ?></span>
		<span class="entry-date"><time><a href="<?php echo get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d') ); ?> "><?php the_time(get_option('date_format')); ?></a></time></span>
		<span class="entry-category"><?php esc_html_e('in', 'vt-blogging-pro'); ?> <?php vt_blogging_pro_first_category(); ?></span> 
		<span class="post-view"><?php echo vt_blogging_pro_get_post_views(get_the_ID()); ?></span>
	</div><!-- .entry-meta -->
<?php endif; ?>