<?php $pin_image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>
<div class="social-share share-buttons">
	<span class="shareto"><?php esc_html_e( 'Share :', 'vt-blogging-pro' ); ?></span>
	<?php if (get_theme_mod('social-share-fb', 1) == 1) : ?>
		<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-facebook"></i></a>
    <?php endif; 
		if (get_theme_mod('social-share-tw', 1) == 1) : ?>
		<a target="_blank" href="https://twitter.com/home?status=Check%20out%20this%20article:%20<?php echo vt_blogging_pro_url_encode( get_the_title() ); ?>%20-%20<?php echo urlencode(the_permalink()); ?>"><i class="fa fa-twitter"></i></a>                    			
	<?php endif;
		if (get_theme_mod('social-share-pin', 1) == 1) : ?>
		<a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo esc_url($pin_image); ?>&description=<?php the_title(); ?>"><i class="fa fa-pinterest"></i></a>
	<?php endif;
		if (get_theme_mod('social-share-gplus', 1) == 1) : ?>
		<a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><i class="fa fa-google-plus"></i></a>
	<?php endif;
		if (get_theme_mod('social-share-in', 1) == 1) : ?>
		<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo urlencode( get_permalink( get_the_ID() ) ); ?>&amp;title=<?php echo urlencode( esc_attr( get_the_title( get_the_ID() ) ) ); ?>" ><i class="fa fa-linkedin"></i></a>
	<?php endif;
		if (get_theme_mod('social-share-mail', 1) == 1) : ?>
		<a target="_blank" href="mailto:?subject=<?php echo ( ( '[' . get_bloginfo( 'name' ) . '] ' . get_the_title( get_the_ID() ) ) ); ?>&amp;body=<?php echo esc_url( ( get_permalink( get_the_ID() ) ) ); ?>"><i class="fa fa-envelope-open"></i></a>	
	<?php endif; ?>
</div>