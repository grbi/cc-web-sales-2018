<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @package VT Blogging Pro
 */
?>

<section class="no-results not-found">

	<header class="page-header">
		<h1 class="page-title"><?php echo wp_kses_post( get_theme_mod( 'website-nosearch-head', __( 'Nothing Found', 'vt-blogging-pro' ) ) ) ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'vt-blogging-pro' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php echo wp_kses_post( get_theme_mod( 'website-nosearch-msg', __( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'vt-blogging-pro' ) ) ) ?></p>
			<?php
				get_search_form();

		else : ?>

			<p><?php echo wp_kses_post( get_theme_mod( 'website-nosearch-msg', __( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'vt-blogging-pro' ) ) ) ?></p>
			<?php
				get_search_form();

		endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->