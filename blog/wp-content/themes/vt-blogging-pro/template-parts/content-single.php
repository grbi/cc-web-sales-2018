<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package VT Blogging Pro
 */	
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">	
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : 

		get_template_part('template-parts/entry', 'meta');
		?>

		<?php
		endif; ?>

	</header><!-- .entry-header -->

	<div class="entry-content">
	
		<?php 
			get_template_part( 'template-parts/post', 'format' ); 

			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'vt-blogging-pro' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vt-blogging-pro' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	
	<?php
		get_template_part('template-parts/entry', 'tags');
		
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				esc_html__( 'Edit %s', 'vt-blogging-pro' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="edit-link">',
			'</span>'
		);
	?>

</article><!-- #post-## -->

  <div class="entry-footer">
	<div class="wrap-meta">
		<?php 
			get_template_part('template-parts/entry', 'comment');
		  if (get_theme_mod('social-share-on', 1) == 1) :
			get_template_part('template-parts/entry', 'share');
		  endif;	
		?>
	</div><!-- .wrap-meta -->
  </div><!-- .entry-footer -->

<?php
	 
	if (get_theme_mod('related-posts-on', 0) == 1) : 
	 
	// Get the taxonomy terms of the current page for the specified taxonomy.
	$terms = wp_get_post_terms( get_the_ID(), 'category', array( 'fields' => 'ids' ) );

	// Bail if the term empty.
	if ( empty( $terms ) ) {
		return;
	}

	// Posts query arguments.
	$query = array(
		'post__not_in' => array( get_the_ID() ),
		'tax_query'    => array(
			array(
				'taxonomy' => 'category',
				'field'    => 'id',
				'terms'    => $terms,
				'operator' => 'IN'
			)
		),
		'posts_per_page' => 4,
		'post_type'      => 'post',
	);

	// Allow dev to filter the query.
	$args = apply_filters( 'vt_blogging_pro_related_posts_args', $query );

	// The post query
	$related = new WP_Query( $args );

	if ( $related->have_posts() ) : $i = 1; ?>

		<div class="entry-related clear">
			<h3><?php esc_html_e('You May', 'vt-blogging-pro'); ?><span> <?php esc_html_e('Also Like', 'vt-blogging-pro'); ?></span></h3>
			<div class="related-loop clear">
				<?php while ( $related->have_posts() ) : $related->the_post(); ?>
					<?php
					$class = ( 0 == $i % 2 ) ? 'hentry last' : 'hentry';
					?>
					<div class="<?php echo esc_attr( $class ); ?>">
						<?php if ( has_post_thumbnail() ) : ?>
							<a class="thumbnail-link" href="<?php the_permalink(); ?>">
								<div class="thumbnail-wrap">
									<?php the_post_thumbnail('vt_blogging_pro_related_post'); ?>
								</div><!-- .thumbnail-wrap -->
							</a>
						<?php else : ?>
							<a class="thumbnail-link" href="<?php the_permalink(); ?>" rel="bookmark">
								<div class="thumbnail-wrap">
									<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/no-thumbnail-2.png" alt="No Picture" />
								</div><!-- .thumbnail-wrap -->
							</a>
						<?php endif; ?>						
						<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					</div><!-- .grid -->
				<?php $i++; endwhile; ?>
			</div><!-- .related-posts -->
		</div><!-- .entry-related -->

	<?php endif;

	// Restore original Post Data.
	wp_reset_postdata();

	endif;
	
	get_template_part( 'template-parts/single', 'post-author' );
?>