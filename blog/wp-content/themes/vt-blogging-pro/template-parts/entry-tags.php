<?php if ( !get_theme_mod( 'single-tags-on', 1) == 0) : if (has_tag()) { ?>
	<span class="entry-tags">
		<span class="tag-title"><?php echo __('Tags:', 'vt-blogging-pro'); ?></span>
		<span class="tagcloud">
		  <?php
			$post_tags = get_the_tags();
			if ($post_tags) {
			foreach($post_tags as $tag) {
				echo '<a class="tag-cloud-link" href="'; echo esc_url( home_url() );
				echo '/tag/' . $tag->slug . '" class="' . $tag->slug . '">' . $tag->name . '</a>';
			}
			}
		  ?>
		</span>
	</span><!-- .entry-tags -->
<?php } endif; ?>