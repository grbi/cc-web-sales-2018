<?php
/**
 * The template for displaying archive pages.
 *
 * @package VT Blogging Pro
 */

get_header(); ?>

	<div id="primary" class="content-area clear">

		<main id="main" class="site-main clear">

			<div class="section-header clear">
				<?php if ( is_category() ) : ?>
					<h3><?php esc_html_e('Browsing Category', 'vt-blogging-pro'); ?>: <?php printf( __('%s', 'vt-blogging-pro'), single_cat_title('', false) ); ?></h3>
				<?php elseif ( is_tag() ) : ?>
					<h3><?php esc_html_e('Browsing Tag', 'vt-blogging-pro'); ?>: <?php printf(__('%s', 'vt-blogging-pro'), single_tag_title('', false)); ?></h3>
				<?php elseif ( is_author() ) : ?>
					<h3><?php esc_html_e('All Posts By', 'vt-blogging-pro'); ?>: <?php the_post(); echo get_the_author(); ?></h3>
				<?php else : ?>
					<?php if ( is_day() ) : ?>
					<h3><?php esc_html_e('Daily Archives', 'vt-blogging-pro'); ?>: <?php echo get_the_date(); ?></h3>            
					<?php elseif ( is_month() ) : ?>
					<h3><?php esc_html_e('Monthly Archives', 'vt-blogging-pro'); ?>: <?php echo get_the_date( _x( 'F Y', 'monthly archives date format', 'vt-blogging-pro' ) ); ?></h3>            
					<?php elseif ( is_year() ) : ?>
					<h3><?php esc_html_e('Yearly Archives', 'vt-blogging-pro'); ?>: <?php echo get_the_date( _x( 'Y', 'yearly archives date format', 'vt-blogging-pro' ) ); ?></h3>			
					<?php else : ?>
						<h3><?php esc_html_e('Archives', 'vt-blogging-pro'); ?>: </h3>
					<?php endif; ?>
				<?php endif; ?>
			</div><!-- .section-header -->
		
			<div id="recent-content" class="content-loop">

				<?php if ( get_theme_mod( 'archive-page-layout' ) == 'layout-1' ) : 

						get_template_part( 'layouts/blog', 'list-layout-1' );

					elseif ( get_theme_mod( 'archive-page-layout' ) == 'layout-2' ):

						get_template_part( 'layouts/blog', 'list-layout-2' );
						
					elseif ( get_theme_mod( 'archive-page-layout' ) == 'standard-list' ):

						get_template_part( 'layouts/blog', 'standard-list' ); 
						
					elseif ( get_theme_mod( 'archive-page-layout' ) == '1stfull' ): 

						get_template_part( 'layouts/blog', 'list-1st-fullpost' ); 
						
					else : 

						get_template_part( 'layouts/blog', 'standard' );

					endif;
					
				?>

			</div><!-- #recent-content -->

		</main><!-- .site-main -->

		<?php get_template_part( 'template-parts/pagination', '' ); ?>

	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>