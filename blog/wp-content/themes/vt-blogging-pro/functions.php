<?php
/**
 * VT Blogging Pro functions and definitions.
 *
 * @package VT Blogging Pro
 */
$theme = wp_get_theme();
define('VT_BLOGGING_PRO_VERSION', $theme -> get('Version'));
define('VT_BLOGGING_PRO_AUTHOR_URI', $theme -> get('AuthorURI'));
define('VT_BLOGGING_PRO_LIBS_URI', get_template_directory_uri() . '/libs/');

/**
 * Set the content width based on the theme's design and stylesheet.
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function vt_blogging_pro_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'vt_blogging_pro_content_width', 1000 ); /* pixels */
}
add_action( 'after_setup_theme', 'vt_blogging_pro_content_width', 0 );


if ( ! function_exists( 'vt_blogging_pro_setup' ) ) :

function vt_blogging_pro_setup() {

	// Translations can be filed in the /languages/ directory.
	load_theme_textdomain( 'vt-blogging-pro', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Let WordPress manage the document title.
	add_theme_support( 'title-tag' );

	// Enable support for Post Thumbnails on posts and pages
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'post-thumbnails', 300, 300, true); // default Post Thumbnail dimensions (cropped)
	add_image_size( 'vt_blogging_pro_thumb', 200, 150, true);
	add_image_size( 'vt_blogging_pro_full_thumb', 610 );
	add_image_size( 'vt_blogging_pro_related_post', 80, 76, true);
	add_image_size( 'vt_blogging_pro_widget', 80 ); // 80 pixels wide (and unlimited height)
	
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'vt-blogging-pro' ),
		'footer' => esc_html__( 'Footer Menu', 'vt-blogging-pro' ),		
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'image', 'gallery', 'video', 'audio'));
	
	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'vt_blogging_pro_custom_background_args', array(
		'default-color' => 'f0f0f0',
		'default-image' => '',
	) ) );

	// Custom logo
	add_theme_support( 'custom-logo', array(
	   'height'      => 67,
	   'width'       => 200,
	   'flex-height' => true,
	   'flex-width' => true,
	) );
	
	/* Add callback for custom TinyMCE editor stylesheets. (editor-style.css) */
	add_editor_style('editor-style.css'); 
	
	// WooCommerce 3.0 support
	if (class_exists( 'woocommerce' )) {
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}
	
}
endif;
add_action( 'after_setup_theme', 'vt_blogging_pro_setup' );

/**
 * Enqueues scripts and styles.
 */
add_action('wp_enqueue_scripts', 'vt_blogging_pro_scripts');
function vt_blogging_pro_scripts() {

    // CSS
	wp_enqueue_style( 'theme-style', get_stylesheet_uri(), '', VT_BLOGGING_PRO_VERSION );
	wp_enqueue_style( 'animate-css', VT_BLOGGING_PRO_LIBS_URI . 'animate/animate.min.css' );
	wp_enqueue_style( 'font-awesome', VT_BLOGGING_PRO_LIBS_URI . 'font-awesome/css/font-awesome.min.css', array(), '4.7.0' );
    wp_enqueue_style( 'superfish-style', VT_BLOGGING_PRO_LIBS_URI . 'superfish/superfish.css' );
    wp_enqueue_style( 'genericons-style', VT_BLOGGING_PRO_LIBS_URI . 'genericons/genericons.css' );
	wp_enqueue_style( 'owl-carousel', VT_BLOGGING_PRO_LIBS_URI . 'owl/owl.carousel.css', array(), '1.0.3' );
	
	if (class_exists( 'woocommerce' )) {
		wp_enqueue_style( 'woocommerce-css', get_template_directory_uri() . '/woocommerce/css/woocommerce.css', array(), '1.0' );
	}
	
    if ( get_theme_mod( 'site-layout', 'choice-1' ) == 'choice-1' ) {
		wp_enqueue_style( 'slicknav-css', VT_BLOGGING_PRO_LIBS_URI . 'slicknav/slicknav.css', array(), VT_BLOGGING_PRO_VERSION );
    	wp_enqueue_style( 'responsive-style',   get_template_directory_uri() . '/responsive.css', array(), VT_BLOGGING_PRO_VERSION );
	}
	
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }  
	
    // JS
	wp_enqueue_script( 'hc-sticky-js', get_template_directory_uri() . '/assets/js/hc-sticky.js', array(), '', false ); 
	wp_enqueue_script( 'vt-blogging-pro-scripts', get_template_directory_uri() . '/assets/js/custom.js', array(), '', true );
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery.js', array(), '', true );
	wp_enqueue_script( 'html5', VT_BLOGGING_PRO_LIBS_URI . 'html5shiv/html5.js', array(), '', false );
	wp_enqueue_script( 'wow-min-js', VT_BLOGGING_PRO_LIBS_URI . 'wow/wow.min.js', array(), '', false );
	wp_enqueue_script( 'owl-carousel', VT_BLOGGING_PRO_LIBS_URI . 'owl/owl.carousel.min.js', array(), '1.3.3', true );
	wp_enqueue_script( 'superfish', VT_BLOGGING_PRO_LIBS_URI . 'superfish/superfish.js', array(), '', true ); 
	wp_enqueue_script( 'slicknav', VT_BLOGGING_PRO_LIBS_URI . 'slicknav/jquery.slicknav.min.js', array(), '', true );
	wp_enqueue_script( 'modernizr', VT_BLOGGING_PRO_LIBS_URI . 'modernizr/modernizr.min.js',array(), '', true ); 

}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/vt-blogging-pro-resize-image.php';

/**
 * Load Customizer Library scripts
 */
require get_template_directory() . '/customizer/customizer-library/customizer-library.php';
require get_template_directory() . '/customizer/customizer-options.php';
require get_template_directory() . '/customizer/styles.php';
require get_template_directory() . '/customizer/mods.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load plugins.
 */ 
require get_template_directory() . '/inc/plugins.php';

/**
 * Load Widgets
 */ 
require get_template_directory() . '/inc/widgets.php';

// Add Admin stylesheet to the admin page
function vt_blogging_pro_load_admin_script() {
    wp_enqueue_style( 'vt-blogging-admin-css', get_template_directory_uri() . '/assets/css/style-admin.css' );
}
add_action( 'admin_enqueue_scripts', 'vt_blogging_pro_load_admin_script' );

/**
 * Enqueue custom customizer styling.
 */
function vt_blogging_pro_load_customizer_script() {
    wp_enqueue_style( 'vt-blogging-pro-customizer-css', get_template_directory_uri() . "/customizer/customizer-library/css/customizer.css" );
}
add_action( 'customize_controls_enqueue_scripts', 'vt_blogging_pro_load_customizer_script' );

// Load widgets, options and include files
if (class_exists( 'woocommerce' )) {
	require_once('woocommerce/woocommerce-hooks.php');
}

/* Fix PHP warning */
function _get($str){
    $val = !empty($_GET[$str]) ? $_GET[$str] : null;
    return $val;
}