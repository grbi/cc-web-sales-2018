=== Checklist in Post ===
Contributors: tomiskym
Tags: checklist, in, post, shortcode,for,user,reader, checklist,Frontend,list,to-do,todo
Requires at least: 3.0
Tested up to: 4.9
Stable tag: 1.1.2
License: #GNUGPLv3
License URI: https://www.gnu.org/licenses/gpl.html

Allow creating checklists in posts based on bulleted list.

== Description ==
Allow creating checklists in posts based on bulleted list.
**Supports Cookies**

*Usage:*
* Create / Edit Post and add classic Visual Editor bulleted list to it.
* Select list in Visual Editor and click button `"Add Checklist"`.
* Shortcodes `[checklist_in_post] [/checklist_in_post]` will appear.
* Save.
* If you want to use cookies - check cookie option under plugin settings.

== Installation ==
EN:

* Upload the directory wordpresschecklistinpost in /wp-content/plugins/ or install the plugin over the plugin manager of WordPress.
* Activate the plugin over the plugin manager of WordPress.

*Usage:*
* Create / Edit Post and add classic Visual Editor bulleted list to it.
* Select list in Visual Editor and click button `"Add Checklist"`.
* Shortcodes `[checklist_in_post] [/checklist_in_post]` will appear.
* Save.
* If you want to use cookies - check cookie option under plugin settings.

== Changelog ==
1.1.2
Fixed: Styling for some themes
Fixed: Javascript on change event
1.1.1
Fixed: ToggleCookie
Fixed: Cookie path=/
1.1.0 
New : Changed way how checked elements are build, changed classes
New : Cookie support - settings page into WordPress Admin. 365 days period.
1.0.4
Fixed: Contact Form conflict resolved (Thanks to @tdper)
1.0.3
Fixed: Allow Nested Lists (Thanks @iisisrael)
Fixed: Allow using HTML into Lists
Fixed: Force padding-left: 0px;
1.0.2
Force transparent element list background (for themes using background in li)
1.0
Initial Release
