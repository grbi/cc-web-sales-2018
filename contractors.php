<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
		<link rel="stylesheet" href="css/font-awesome.min.css" />
        <title>ContractComplete - Detailed Product Specifications</title>
		<?php
			include 'https_redirect.php';
			include 'css_common.php';
		?>
    </head>
    <body>
        
		<?php 
			include 'header.html';
			include 'features_contractor.php';
			
			/*
			$rolearg = "";
			if(array_key_exists ( "role" , $_GET)){
				$rolearg = $_GET["role"];
			}
			
			switch ($rolearg) {
				case 'consultants':
					include 'features_consultant.php';
					break;
				case 'contractors':
					include 'features_contractor.php';
					break;
				case 'subcontractors':
					include 'features_subcontractor.php';
					break;
				case 'owners':
					include 'features_owner.php';
					break;
			}*/
		?>
        
		

		
		
	
	
        
		<?php
			include 'signup-prompt.php';
        ?>
	   
		<?php
			include 'footer.php';
		?>
        
		<?php
			include 'common_scripts.html';
		?>
		
		<script type="text/javascript">
			window.loadFile('vendors/owl-carousel/owl.carousel.min.js', function(){
					var owl = $('.owl-carousel2');
					owl.owlCarousel({
						items:1,
						loop:true,
						margin:10,
						autoplay:true,
						autoplayTimeout:5000,
						autoplayHoverPause:true,
						navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
						nav: false
					});
					$('.play').on('click',function(){
						owl.trigger('play.owl.autoplay',[5000])
					});
					$('.stop').on('click',function(){
						owl.trigger('stop.owl.autoplay')
					});
			  });
		</script>
    </body>
</html>