<SCRIPT type="text/javascript"> 
  window.loadFile = window.loadFile || function( url, callback ) {
  var script = document.createElement( "script" )
  script.type = "text/javascript";
  if(script.readyState) {  // only required for IE <9
    script.onreadystatechange = function() {
      if ( script.readyState === "loaded" || script.readyState === "complete" ) {
        script.onreadystatechange = null;
        if(callback)callback();
      }
    };
  } else {  //Others
    script.onload = function() {
      if(callback)callback();
    };
  }

  script.src = url;
  document.getElementsByTagName( "head" )[0].appendChild( script );
}
</SCRIPT>	