<!doctype html>
<html lang="en-us" ng-app="myApp">
<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
        <title>ContractComplete - Tendering & Contract Management Software</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="vendors/linericon/style.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
        <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="vendors/animate-css/animate.css">
        <link rel="stylesheet" href="vendors/flaticon/flaticon.css">
        <!-- main css -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="vendors/jquery-ui/jquery-ui.css">
	<style type="text/css">
		.questionBorder{
			/*border-color:black;
			border-width:4px;
			border-style:solid;*/
			max-width:800px;
			padding:30px;
			margin-left:auto;
			margin-right:auto;
			text-align:left;
			font-size:18px;
			line-height:40px;
			margin-bottom: 50px;
		}
		
		.radioStyle{
			-webkit-appearance: radio;
			transform: scale(2);
		}
		
		.extra_info{
			color: green;
			line-height: 25px;
		}
		.good_news{
			color: blue;
			line-height: 25px;
		}
		
		html, body {
			max-width: 100%;
			overflow-x: hidden;
		}
		
		.question-layout-table {
			margin: auto;
		}
		
		.question-layout-table td{
			text-align: left;
			border: 1px solid;
			padding: 12px;
			vertical-align: top;
		}
		
		.question-layout-table td:nth-child(2){
			text-align:right;
			width: 120px;
		}
		
		.question-layout-table td:nth-child(1){
			font-size: 16px;
		}
		
		.results-plot{
			width: 800px;
			height: 600px;
			margin: auto;
		}
		
		#results-html{
			padding: 16px;
		}
	</style>
		<script src="https://cdn.plot.ly/plotly-1.2.0.min.js"></script>
		<script type="text/javascript">
			window.frmtCurrency = function(value){
				var num = '$' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
				return num;
			};
		
			window.calculation = {
				common_q0: {
					fn: function() { return document.getElementById('common_q0_ans').value; },
					score: function() { return 0; }, // doesn't directly impact store
					info: function() { return ""; }
				},
				common_q00: {
					fn: function() { return document.getElementById('common_q00_ans').value; },
					score: function() { return 0; }, // doesn't directly impact store
					info: function() { return ""; }
				},
				common_q000: {
					fn: function() { return document.getElementById('common_q000_ans').value; },
					score: function() { return 0; }, // doesn't directly impact store
					info: function() { return ""; }
				},
				contractor_q1: {
					label: 'Invoicing',
					fn: function() { return document.getElementById('contractor_q1_ans').value; },
					score: function() { return window.calculation.contractor_q1.fn() * 30 * window.calculation.common_q0.fn(); },
					info: function() {
						var score = window.calculation.contractor_q1.score();
						return 'You will save about ' + Math.round(score/60.0) + " hours (" + window.frmtCurrency(score * window.minuteRate) + ")" + " each year on invoicing.<br>ContractComplete allows invoices with a cover sheet and a line item<br>breakdown to be generated in seconds with the click of a button."; 
					}
				},
				other_q1: {
					label: 'Bid Review',
					fn: function() { return document.getElementById('other_q1_ans').value; },
					score: function() { return window.calculation.other_q1.fn() * 180; },
					info: function() { 
						var score = window.calculation.other_q1.score();
						return 'You will save about ' + Math.round(score/60.0)  + " hours (" + window.frmtCurrency(score * window.minuteRate) + ")" +  " each year on tendering.<br>ContractComplete automatically generates your bid comparison documents as soon as the deadline has passed.<br>It also helps with estimating by remembering unit costs from previous contracts!"; 
					}
				},
				other_q2: {
					label: 'Payment Certificates',
					fn: function() { return document.getElementById('other_q2_ans').value; },
					score: function() { return window.calculation.other_q2.fn() * 30 * window.calculation.common_q0.fn(); },
					info: function() { 
						var score = window.calculation.other_q2.score();
						return 'You will save about ' + Math.round(score/60.0) + " hours (" + window.frmtCurrency(score * window.minuteRate) + ")" +  " each year on payment certificates.<br>ContractComplete automatically generates payment certificates for every invoice.<br>You just add your customizations and click approve!"; 
					}
				},
				other_q3: {
					label: 'Tendering Addenda',
					fn: function() { return document.getElementById('other_q3_ans').value; },
					score: function() { return window.calculation.other_q3.fn() * 10 * window.calculation.other_q1.fn(); }, // multiply by tender count
					info: function() { 
						var score = window.calculation.other_q3.score();
						return 'You will save about ' + Math.round(score/60.0) + " hours (" + window.frmtCurrency(score * window.minuteRate) + ")" +  " each year on addenda.<br>ContractComplete automatically notifies bidders when an addendum has been issued.<br>It also tracks which contractors have actually read it!<br>No more chasing people down."; 
					}
				},
				common_q1: {
					label: 'Change Orders',
					fn: function() { return document.getElementById('common_q1_ans').value; },
					score: function() { return window.calculation.common_q1.fn() * 30 * window.calculation.common_q0.fn(); },
					info: function() { 
						var score = window.calculation.common_q1.score();
						return 'You will save about ' + Math.round(score/60.0) + " hours (" + window.frmtCurrency(score * window.minuteRate) + ")" +  " each year on change orders.<br>With a few clicks, ContractComplete generates change orders and <br>associated documents in addition to faciliting the eSigning process!<br>It will even request quotes from the contractor if needed.";
					}
				},
				common_q2: {
					label: 'Document Management',
					fn: function() { return document.getElementById('common_q2_ans').value; },
					score: function() { return window.calculation.common_q2.fn() * 10 * window.calculation.common_q000.fn(); },
					info: function() { 
						var score = window.calculation.common_q2.score();
						return 'You will save about ' + Math.round(score/60.0 )+ " hours (" + window.frmtCurrency(score * window.minuteRate) + ")" +  " each year finding documents.<br>ContractComplete mobile stores all documents in one convenient location.<br>This includes all of your drawings and any documents generated by<br>ContractComplete such as invoices and change orders."; 
					}
				}
			}
		
		
			// quiz code
			window.contractorSeries = ['common_q0', 'common_q000', 'common_q00','contractor_q1', 'common_q1', 'common_q2', 'results'];
			window.otherSeries = ['common_q0', 'common_q000', 'common_q00', 'other_q1', 'other_q2', 'other_q3', 'common_q1', 'common_q2', 'results'];
			
			window.onViewResults = function(){
				window.readyForResults = true;
				window.showResults();
				document.getElementById('nextButton').style.display = 'none';
			}
			
			window.showResults = function(){
				if(!window.readyForResults) return;
				document.getElementById('nextButton').style.display = 'none';
				
				window.minuteRate = window.calculation.common_q00.fn()/60.0;
				var html = "<br/><br/>";
				var hasResults = false;
				var totalScore = 0;
				for(var i = 0; i < window.series.length - 1; i++){
					totalScore += window.calculation[window.series[i]].score();
					console.log(window.calculation[window.series[i]].score());
				}
				
				var value = window.minuteRate * totalScore;
				var num = window.frmtCurrency(value);
				
				html+= "<div class='good_news'>Good news!  Looks like ContractComplete can save you about " + Math.round(totalScore /60) + " hours of labor per year!  At the provided hourly rate, that works out to an annual cost savings of approximately <b>" + num + "</b>.  Keep reading to see how!</div><br/>";
				
				for(var i = 0; i < window.series.length - 1; i++){
					if(window.calculation[window.series[i]].score() > 0){
						hasResults = true;
					}
				}
				
				html += "<div class='good_news'>Hover over the chart below to see your personalized cost savings breakdown.  Or, scroll up to change the parameters.</div><br/>";
				
				if(!hasResults)html = 'ContractComplete won\'t save you any time.  Are you sure you answered these questions correctly?';
				
				document.getElementById('results-html').innerHTML = html;
				document.getElementById('show-results').style.display = 'inline-block';
				
				if(hasResults){
					/* Code for generating the results plot */
					var xv = [];
					var yv = [];
					var hoverTxt = [];
					
					var sorted = [];
					for(var i = 0; i < window.series.length - 1; i++) sorted.push(window.series[i]);
					
					sorted.sort(function(a,b){
						return window.calculation[a].score()  - window.calculation[b].score();
					});
					//debugger;
					for(var i = 0; i < sorted.length; i++){
						var score = window.minuteRate *window.calculation[sorted[i]].score();
						yv.push(window.calculation[sorted[i]].label);
						xv.push(score);
						hoverTxt.push(window.calculation[sorted[i]].info());
					}
					
					//debugger;
					var TESTER = document.getElementById('tester');
					Plotly.newPlot( TESTER, [{
						x: xv,
						y: yv,
						text: hoverTxt,
						orientation: 'h',
						hoverlabel: { bgcolor: "#1da29b" },
						type: 'bar',
						title: 'Your Savings',
						marker: {color: '#146B75'}
					}], {
					margin: {
						t: 50,
						l: 200
					} } );
					
					document.getElementById('tester').style.display = 'block';
				}
			};
			
			window.businessTypeSelect = function(type){
				document.getElementById('nextButton').style.display = 'inline-block';
				document.getElementById('question-table').style.display = 'table';
				document.getElementById('calculator-parameters').style.display = 'block';
				if(type == 'contractor'){
					window.series = window.contractorSeries;
				}else{
					window.series = window.otherSeries;
				}
				
				// hide all rows
				for(var i = 0; i < window.contractorSeries.length; i++) document.getElementById(window.contractorSeries[i]).style.display = 'none';
				for(var i = 0; i < window.otherSeries.length; i++) document.getElementById(window.otherSeries[i]).style.display = 'none';
				
				// show the ones we care about
				for(var i = 0; i < window.series.length - 1 /*skip the last one*/; i++){
					document.getElementById(window.series[i]).style.display = 'table-row';
				}
				
				document.getElementById(window.series[window.series.length - 1]).style.display='block';
				
				window.showResults();
			};
		
		</script>
</head>

<body ng-app=""  ng-controller="main-controller">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PFGMS6K"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php 
		include 'header.html';
	?>
	
	<!--================Home Banner Area =================-->
        <section class="banner_area" style="height:300px;min-height:auto">
			<div class="video-wrapper" style="position:absolute;top:0px;left:0px;width:100%;overflow:hidden;height:300px">
				<video autoplay muted loop id="myVideo">
				  <source src="video/blueprint.mov" type="video/mp4">
				</video>
			</div>
            <div class="banner_inner d-flex align-items-center" style="background:none">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container">
					<div class="banner_content text-center" style="margin-top:0px">
						<h2>Is ContractComplete Right for You?</h2>
					</div>
				</div>
            </div>
        </section>
	<!--================End Home Banner Area =================-->

	<div style="padding:16px;text-align:center">
		<div>
			<h1>1. Calculator Type</h1>
			<div class="questionBorder" id="q1">
				Which of the following best describes your company?<br/><br/>
				<form action="">
				  <input type="radio" onclick="businessTypeSelect('contractor')" class="radioStyle" name="gender" value="contractor">&nbsp;&nbsp;&nbsp;&nbsp;A Contractor<br>
				  <input type="radio" onclick="businessTypeSelect('other')" class="radioStyle" name="gender" value="consultant">&nbsp;&nbsp;&nbsp;&nbsp;A Consultant, Engineer, or Architect<br>
				  <input type="radio" onclick="businessTypeSelect('other')" class="radioStyle" name="gender" value="developer">&nbsp;&nbsp;&nbsp;&nbsp;A Land Developer
				</form>
			</div>
			<h1 id="calculator-parameters" style="display:none">2. Calculator Parameters</h1>
			<table class="question-layout-table" id="question-table" style="display:none">
				<tr id="common_q0">
					<td>
						How many contracts is your company actively <b>working on</b> in a given year?
					</td>
					<td>
						<input id="common_q0_ans" type="number" name="quantity" min="1" max="5000" value="100" onchange="showResults()">
					</td>
				</tr>
				<tr id="common_q000">
					<td>
						How many <b>field staff</b> does your company employ?
					</td>
					<td>
						<input id="common_q000_ans" type="number" name="quantity" min="1" max="5000" value="10" onchange="showResults()">
					</td>
				</tr>
				<tr id="common_q00">
					<td>
						What is the approximate (average) <b>hourly rate</b> (in dollars) for your <b>contract admin staff</b>?
					</td>
					<td>
						<span style="font-size:30px">$ </span><input onchange="showResults()" id="common_q00_ans" type="number" name="quantity" min="1" max="1000" value="40">
					</td>
				</tr>
				<tr id="other_q1">
					<td>
						How many contracts does your company tender each year?  If you do not tender any contracts, enter 0.
					</td>
					<td>
						<input onchange="showResults()" id="other_q1_ans" type="number" name="quantity" min="0" max="5000" value="20">
					</td>
				</tr>
				<tr id="other_q1">
					<td>
						How many contracts does your company tender each year?  If you do not tender any contracts, enter 0.
					</td>
					<td>
						<input onchange="showResults()" id="other_q1_ans" type="number" name="quantity" min="0" max="5000" value="20">
					</td>
				</tr>
				<tr id="other_q1">
					<td>
						How many contracts does your company tender each year?  If you do not tender any contracts, enter 0.
					</td>
					<td>
						<input onchange="showResults()" id="other_q1_ans" type="number" name="quantity" min="0" max="5000" value="20">
					</td>
				</tr>
				<tr id="other_q2">
					<td>
						How many payment certificates <b>per contract</b> do you create each year?  If you do not use payment certificates, enter 0.
					</td>
					<td>
						<input onchange="showResults()" id="other_q2_ans" type="number" name="quantity" min="0" max="5000" value="5">
					</td>
				</tr>
				<tr id="other_q3">
					<td>
						How many addendums do you issue<b> during tendering per contract</b> on average?  If you do not use addenda, enter 0.
					</td>
					<td>
						<input onchange="showResults()" id="other_q3_ans" type="number" name="quantity" min="0" max="50" value="2">
					</td>
				</tr>
				<tr id="contractor_q1">
					<td>
						How many invoices does your company create <b>per contract</b> each year on average?
					</td>
					<td>
						<input onchange="showResults()" id="contractor_q1_ans" type="number" name="quantity" min="0" max="5000" value="6">
					</td>
				</tr>
				<tr id="common_q1">
					<td>
						How many change orders (or quotes for change orders) do you create <b>per contract</b> each year?  If you do not use change orders, enter 0.
					</td>
					<td>
						<input onchange="showResults()" id="common_q1_ans" type="number" name="quantity" min="0" max="5000" value="4">
					</td>
				</tr>
				<tr id="common_q2">
					<td>
						How many times in the last year have you needed to search for a drawing (in your inbox or elsewhere), or didn't have a required drawing on a site visit?
					</td>
					<td>
						<input onchange="showResults()" id="common_q2_ans" type="number" name="quantity" min="0" max="5000" value="20">
					</td>
				</tr>
			</table>
			
			<a id="nextButton" href="javascript:window.onViewResults()" class="tickets_btn_nav" style="width: 200px;margin-top:35px;display:none">View Results</a>
			
			<div id="show-results" style="width:100%;display:none">
				<h1 style="margin-top: 50px;">3. Your Savings</h1>
				<div id="results-html"></div>
				<a href="javascript:onGetStartedDemoRequest();" class="tickets_btn_nav" style="width: 200px;margin-top:35px">Request a Demo</a>
			</div>
			
			<div  id="results" style="width:100%">
				<div id="tester" class="results-plot" style="display:none" ></div>
				<div style="width:100%" id="results_info"></div>
			</div>
		</div>
	</div>
        <!--================End Work Area =================-->
        
		<?php
			include 'signup-prompt.php';
        ?>
	   
		<?php
			include 'footer.php';
		?>
        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/stellar.js"></script>
        <script src="vendors/lightbox/simpleLightbox.min.js"></script>
        <script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="vendors/isotope/isotope-min.js"></script>
        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="js/mail-script.js"></script>
        <script src="vendors/counter-up/jquery.waypoints.min.js"></script>
        <script src="vendors/counter-up/jquery.counterup.min.js"></script>
        <script src="vendors/jquery-ui/jquery-ui.js"></script>
        <script src="js/theme.js"></script>
		<?php
			include 'common_scripts.html';
		?>
</body>
</html>
