<style type="text/css">
	.content-wrapper{
		padding:128px;
		padding-top:0px;
	}
	
	@media (max-width:800px){
		.content-wrapper{
			padding:16px;
			padding-top:0px;
		}
	}
		
	.content-wrapper p{
		font-size:24px;
		margin-top: 35px;
	    line-height: 1.4;
	}
	
	#profile-image{
		border-radius: 150px;
		max-height: 300px;
		max-width: 300px;
		margin-left: 30px;
		margin-right: 30px;
		margin-bottom: 20px;
		float:right;
	}
	
	@media (max-width:800px){
		#profile-image{
			float:none!important;
			margin-left: auto;
			margin-right: auto;
			display: table-cell;
		}
	}
	
	.cta{
		background-color:#ffecd4;
		padding:64px;
	}
	
	.main-header{
		font-size:45px;
	}
	
	@media (min-width:800px){
		.main-header{
			font-size:90px;
		}
	}
	
</style>
<script type ="text/javascript">
	window.onSendVideo = function(){
		$("#video-request-dialog" ).dialog('open');
	}
	window.cancelRequestVideo = function(){
		$("#video-request-dialog" ).dialog('close');
	}
	window.acceptVideo = function(){
		var email = $("#video-request-email").val();
		
		window.doVideoRequest({
            displayName: 'Sean Ciampaglia',
            fromEmailAddress: 'sean@contractcomplete.com',
            emailAddress: email,
            pipelineId: '1137200',
            pipelineStageId: '1137201',
			emailTemplateUrl: 'https://www.contractcomplete.com/tpl/bid_mistakes_email1.moustache.html',
			emailSubject: 'The Video you Requested'
		});
	}
</script>


<section class="made_life_area p_120">
<div class="content-wrapper" style="padding-bottom:32px;text-align:center">
	<h3>Is your bid process costing you?</h3>
	<h1 class="main-header">Discover the 5 Big Mistakes Bid Callers are Making</h1>
</div>
<div class="cta" style="width:100%;text-align:center">
	<p style="font-size:32px;color:black">Yes, I want to stop losing money on stupid mistakes!</p>
	<a href="javascript:onSendVideo();" class="tickets_btn_nav" style="width: 200px;margin-top:35px">Send me the Video!</a>
</div>
<div class="content-wrapper" style="padding-bottom:16px">
	<br/>
	<img id="profile-image" src="landingpages/images/sean.jpg"></img>
	<p>Hey, it's Sean Ciampaglia here.</p>
	<p>- Co-founder of ContractComplete -</p>
	<p>
		I’ve been the general manager for a large commercial landscape contractor for over 10 years. Over the years I’ve participated in over 600 bids, and interestingly enough, every client’s process is slightly different.
	</p>
	<p>
		I've worked with countless contract administrators, and many of them have bid calling processes that are costing them <b>big</b>!  The mistakes I discuss in this video result in huge embarassment and financial loss.  The good news is that this pain can be easily avoided, and I made this video to show you how!
	</p>
	<p>
		The video is 100% free.  It's a no brainer.
	</p>
	<p>
		-Sean C.
	</p>
	<p>
	<a href="javascript:onSendVideo();" class="tickets_btn_nav" style="width: 200px;margin-top:35px">Send me the Video!</a>
	</p>
</div>
</section>

<script type="text/javascript">
	document.title = "5 Biggest Mistakes when Calling Bids";
</script>