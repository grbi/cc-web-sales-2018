<html>
	<head>
		<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
		<style type="text/css">
			h1,h2,h3,p{
				font-family: "Roboto", sans-serif
			}
		
			.content-wrapper{
				padding:128px;
				padding-top:0px;
			}
			
			@media (max-width:800px){
				.content-wrapper{
					padding:16px;
					padding-top:0px;
				}
			}
				
			.content-wrapper p{
				font-size:24px;
				margin-top: 35px;
				line-height: 1.4;
			}
					

			
			#profile-image{
				border-radius: 150px;
				max-height: 300px;
				max-width: 300px;
				margin-left: 30px;
				margin-right: 30px;
				margin-bottom: 20px;
				float:right;
			}
			
			@media (max-width:800px){
				#profile-image{
					float:none!important;
					margin-left: auto;
					margin-right: auto;
					display: table-cell;
				}
			}
	
			.main-header{
				font-size:45px;
			}
			
			@media (min-width:800px){
				.main-header{
					font-size:90px;
				}
			}
			
.tickets_btn {
  display: inline-block;
  background: rgba(255, 255, 255, 0.1);
  color: #146b75;
  font-family: "Roboto", sans-serif;
  width: 134px;
  font-size: 13px;
  font-weight: 500;
  border: 1px solid #146B75;
  line-height: 48px;
  border-radius: 3px;
  outline: none !important;
  box-shadow: none !important;
  text-align: center;
  cursor: pointer;
  transition: all 200ms linear 0s; }
  .tickets_btn:hover {
    background: #1da29bd4;
    color: white;
    border-color: white; }
			
		</style>
		<script type ="text/javascript">
			var  getParameterByName = function(name) {
				var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
				return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
			}

			var dealId = getParameterByName('dealid');
			window.onload = function(){
				$.ajax({
					//url: "http://localhost:8080/webservicev3/api/leads/pipelineadvance",
					url: "https://api.contractcomplete.com/api/leads/pipelineadvance",
					method: "POST",
					crossDomain: true,
					data: JSON.stringify({
						dealId: dealId,
						pipelineId: '1137200',
						pipelineStageId: '1137203'
					}),
					contentType: 'application/json;charset=UTF-8',
					success: function(){
						
					},
					error: function(){
						
					}
				});
			}
		</script>
	</head>
	<body>
		<div class="content-wrapper" style="padding-bottom:32px;padding-top:32px;text-align:center">
			<h3>Thank you!  Here is your video.</h3>
			<h1 class="main-header">Discover the 5 Big Mistakes Bid Callers are Making</h1>
		</div>
		<div class="content-wrapper" style="text-align:center">
			<video style="margin-left:auto;margin-right:auto" width="640" height="480" controls>
			  <source src="video/biggest_tendering_mistakes.m4v" type="video/mp4">
				Your browser does not support the video tag.  Please download the video <a target="_blank" href="video/biggest_tendering_mistakes.mp4">here</a>
			</video>
		</div>
		<div class="content-wrapper">
			<br/>
			<img id="profile-image" src="images/sean.jpg"></img>
			<p>Hey, it's Sean Ciampaglia here.</p>
			<p>- Co-founder of ContractComplete -</p>
			<p>
				Thanks so much for checkout out my video.  I hope this was helpful to you.
			</p>
			<p>
				If you're interested in learning more about how ContractComplete can save you money in bid calling, please reach out.  We'd love to hear from you.  We provide no obligation demos to companies looking to improve their bid calling and contract administration.
			</p>
			<p>
				<a href="https://www.contractcomplete.com#demo" class="tickets_btn">Request a Demo</a>
			</p>
			<p>
				Thanks again, and we hope to hear from you soon.
			</p>
			<p>
				-Sean C.
			</p>
		</div>
	
		<script type="text/javascript">
			document.title = "5 Biggest Mistakes when Calling Bids";
		</script>

	
	</body>
</html>