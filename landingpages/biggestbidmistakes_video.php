<style type="text/css">
	.content-wrapper{
		padding:128px;
		padding-top:0px;
	}
	
	@media (max-width:800px){
		.content-wrapper{
			padding:16px;
			padding-top:0px;
		}
	}
		
	.content-wrapper p{
		font-size:24px;
		margin-top: 35px;
	    line-height: 1.4;
	}
	
	#profile-image{
		border-radius: 150px;
		max-height: 300px;
		max-width: 300px;
		margin-left: 30px;
		margin-right: 30px;
		margin-bottom: 20px;
		float:right;
		margin-top:20px;
	}
	
	@media (max-width:800px){
		#profile-image{
			float:none!important;
			margin-left: auto;
			margin-right: auto;
			display: table-cell;
		}
	}
	
	.cta{
		background-color:#ffecd4;
		padding:64px;
		clear: both;
		line-height: 35px;
	}
	
	.main-header{
		font-size:45px;
	}
	
	.content-wrapper.split-layout{
		/*width: 49%!important;*/
	}
	
	.testimonials{
		float:right;
		width:49%;
		padding-right:128px;
	}
	
	.testimonials img{
		max-width: 100px;
		max-height: 100px;
	}
	
	.testimonials td{
		padding: 16px;
		padding-bottom: 48px;
		color: grey;
		vertical-align:top;
	}
	
	.message{
		float:left;
		width:50%;
	}
	
	@media (min-width:800px){
		.main-header{
			font-size:90px;
		}
	}
	
	.video{
		margin-top:20px;
	}
	
	@media (max-width:799px){
		.testimonials{
			float:none;
			width: 100%;
			padding-right:16px;
		}
	
		.message{
			float:none;
			width: 100%;
		}
		
		.testimonials td{
			width: auto;
		}
	
		.video{
			top:-85px;
		}
	}
	
</style>
<script type ="text/javascript">
	window.viewDemo = function(){
		$('#mask').show();
		window.emailUser({
			displayName: 'CC Sales Site',
			emailTemplateUrl: 'https://www.contractcomplete.com/tpl/bid_mistakes_user_clicks_demo.moustache.html',
			emailSubject: 'User Clicked "Request a Demo"',
			emailAddress: 'info@grandriverbi.com',
			bcc: ''
		}, function(){
			$('#mask').hide();
			window.location = 'https://www.contractcomplete.com#demo';
		});
	}
</script>


<section class="made_life_area p_120" style="background-color:white">
<div class="content-wrapper" style="padding-bottom:32px;text-align:center;margin-top:200px">
	<h3>Is your bid process costing you?</h3>
	<h1 class="main-header">Discover the 5 Big Mistakes Bid Callers are Making</h1>
</div>

<div class="content-wrapper" style="text-align:center; clear: both;padding:0px">
	<div class="row made_life_text" style="margin:0px;margin-bottom:60px;">
		<div class="col-lg-6" style="margin:auto">
			<div class="chart_img">
				<div style="position:relative;height:0;padding-bottom:56.22%"><iframe width="671" height="340" style="max-width: 100%;" src="https://www.youtube.com/embed/2MsjtbNMSbo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
			</div>
		</div>
	</div>
</div>
	<div style="clear:both"></div>

<div class="content-wrapper split-layout message" style="padding-bottom:16px;padding-right:64px; ">
	<!--<img id="profile-image" src="landingpages/images/sean.jpg"></img>-->
	<p style="padding-top:0px">Hey, it's Sean Ciampaglia here.</p>
	<p>- Co-founder of ContractComplete -</p>
	<p>
		I’ve been the general manager for a large commercial landscape contractor for over 10 years. Over the years I’ve participated in over 600 bids, and interestingly enough, every client’s process is slightly different.
	</p>
	<p>
		I've worked with countless contract administrators, and many of them have bid calling processes that are costing them <b>big</b>!  The mistakes I discuss in this video result in huge embarassment and financial loss.  The good news is that this pain can be easily avoided, and I made this video to show you how!
	</p>
	<p>
		-Sean C.
	</p>
</div>

<div class="content-wrapper split-layout testimonials" style="padding-bottom:16px; padding-left:0px;">
	<table style="width:100%; font-size: 18px;">
		<tr>
			<td class="testimonial-logo">
				<a target="_blank" href="http://www.strybos.com/"><img src="landingpages/images/sbk.jpg"></img></a>
			</td>
			<td>
				Great experience, easy to keep track of contracts, programmers made changes based on our suggestions.<br/><br/>
				Matthew R. - Associate, senior landscape technologist
			</td>
		</tr>
		<tr>
			<td>
				<a target="_blank" href="http://adessodesigninc.ca/"><img src="landingpages/images/adesso.jpg"></img></a>
			</td>
			<td>
				ContractComplete is easy to use and saves me hours of work.<br/><br/>
				Matthew M. - Landscape Architect
			</td>
		</tr>
		<tr>
			<td>
				<a target="_blank" href="https://www.aboudtng.com/landscapearchitecture"><img src="landingpages/images/aboud.jpg"></img></a>
			</td>
			<td>
				Efficient contract administration software that saves time from bidding to closeout.<br/><br/>
				Mark G. - Landscape Architect/Partner
			</td>
		</tr>
	</table>
</div>

<div class="cta" style="width:100%;text-align:center">
	<p style="font-size:32px;color:black">Did you enjoy the video?  We want to stay in touch!  Ask us about a demo.</p>
	<a href="javascript:viewDemo();" class="tickets_btn_nav" style="width: 200px;margin-top:35px">Request a Demo</a>
</div>
</section>

<script type="text/javascript">
	document.title = "5 Biggest Mistakes when Calling Bids";
</script>