<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
		<link rel="stylesheet" href="css/font-awesome.min.css" />
		<link rel="stylesheet" href="fonts/open_sans/stylesheet.css" />
        <title>ContractComplete - Budgeting</title>
		<?php
			include 'https_redirect.php';
			include 'css_common.php';
		?>
    </head>
    <body>

<?php 
	$safari = true;
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')) {
		$safari=false;
	}
	include 'header.html'; 
?>
        
<div class="product-container shaded-bg" style="">	
	<span class="product-subtitle">Smart Estimation Tools</span><br/>
	<span class="product-title">Create Project Budgets Based<br/> on Historical Pricing</span>
	<br/>
	<img src="img/budget_pdf.png" class="product-pdf-img" style="max-width:90%" /><br/>
	<?php
		include 'schedule_demo.html';
	?>
</div>
<div class="product-container" style="margin-top:100px">	
	<div class="accent-title">Increase Budget Accuracy</div>
	<div class="divider-dot" style="background-color:#0aa89f">&nbsp</div>
	<div class="accent-title">Notice Trends in the Market</div>
	<div class="divider-dot" style="background-color:#0070BB">&nbsp</div>
	<div class="accent-title">Save Time</div><br/>
	<!--
	<img src="img/historic_pricing.png" style="" class="product-pdf-img" />
	<img src="img/chart2.png" style="max-width: 600px;margin-left: -500px;margin-top: 450px;" class="product-pdf-img" />
-->
	<img src="img/budgeting_stacked.png" style="" class="hero-image product-pdf-img borderless" />
	<br class="small-screen-only"/>
	<div class="image-caption-wrapper" style="float:right;float: right;">
		<span class="feature-title">Powerful &amp; Complete</span>
		<div class="accent-title-larger">Automatically Compile<Br/>Historical Pricing</div>
		<p>
			As you award projects to contractors, your<br/>pricing database naturally grows.<br/><br/>All data is your own and not shared with anyone.
		</p>
	</div>
	<div style="clear:both;height:1px"></div>
	<img src="img/estimation_db.png" class="product-pdf-img small-screen-only" />
	<div class="image-caption-wrapper" style="padding-right:64px;display:inline-block">
		<span class="feature-title" style="color:#0aa89f">Flexible &amp; Reliable</span>
		<div class="accent-title-larger">Search Based on<Br/>Date and Location</div>
		<p>
			Filter your search results based on when and where<br/>the project took place in order to create your<br/>most accurate budgets possible.
		</p>
	</div>
	<img src="img/estimation_db.png" class="product-pdf-img large-screen-only" />
	<div style="clear:both;height:80px"></div>

	<?php
		include 'testimonial_adesso2.html'
	?>
	<div style="clear:both;height:1px"></div>
	<img src="img/comparison.png" class="product-pdf-img" />
	<div class="image-caption-wrapper" style="float:right;">
		<span class="feature-title">Automate your Bid Comparison</span>
		<div class="accent-title-larger">Compare your Bids<Br/>with your Budget</div>
		<p>
			Instantly assess how your team's budget did
			<br/>after the bid close. You can easily compare
			<br/>bidder prices to the average or the budget.
		</p>
	</div>

	<div style="clear:both;height:120px"></div>
	<span class="accent-title-larger">See Other Solutions</span>
	<div style="clear:both;height:60px"></div>
	<div style="clear:both"></div>
	<a href="projectmanagement.php"><div class="accent-title">Project Management</div></a>
	<div class="divider-dot" style="background-color:#0aa89f">&nbsp</div>
	<a href="bidding.php"><div class="accent-title">Bidding Management</div></a>
	<div class="divider-dot" style="background-color:#0070BB">&nbsp</div>
	<a href="progressbilling.php"><div class="accent-title">Progress Billing</div><br/>
	
	<div style="clear:both;height:80px"></div>
	<?php
		include 'schedule_demo2.html'
	?>
</div>
<?php 
include 'footer.php'; 
include 'common_scripts.html';
?>
		 
		 <script type="text/javascript">
			var w = window;
			var loadIframe = function(){
				var vidDefer = document.getElementsByTagName('iframe');
				for (var i=0; i<vidDefer.length; i++) {
					if(vidDefer[i].getAttribute('data-src')) {
						vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
					} 
				} 
			}
			
			if (w.addEventListener) { w.addEventListener("load", loadIframe, false); }
			else if (w.attachEvent) { w.attachEvent("onload",loadIframe); }
		 </script>
		 