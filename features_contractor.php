        <!--================Home Banner Area =================-->
        <section class="banner_area">
			<div class="video-wrapper" style="position:absolute;top:0px;left:0px;width:100%;overflow:hidden;height:600px">
				<!--<img src="img/consultant-banner.jpeg" style="min-width:100%;min-height:600px;height:600px"></img>-->
			</div>
            <div class="banner_inner d-flex align-items-center">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container" style="padding-top:150px;height:600px">
					<div class="banner_content text_center consultants-banner" style="width:500px;float:left">
						<h2>Secure Digital Bidding &amp; <b>Accelerated Cash Flow</b></h2><br/>
						<h3>
							Built for Contractors in Infrastructure &amp; Land Development. Submit bids with confidence, then streamline your change management &amp; progress reporting to boost your cash flow.
						</h3>
						<a href="https://connect.contractcomplete.com/get-a-demo" target="_blank" class="tickets_btn_nav" style="margin-top:15px">Get Demo</a>
						<div class="small-screen-only" style="width:100%;clear:both;height:100px">&nbsp;</div>
					</div>
					<div style="max-width:50%;float:right;display:table-cell;vertical-align:middle;height:700px;padding-top:50px">
						<img src="img/Construction man.webp" style="width:100%;margin-top: -60px;" class="top-banner-image"></img>
						</div>
						<div class="rating top-banner-image">
							<div  class="rating-stars" style="color:yellow">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
							<div>
								By Capterra <a target="_blank" href="https://www.capterra.com/p/166394/ContractComplete/"><i class="fa fa-location-arrow" style="font-size: 24px;color:#D95B43"></i></a>
							</div>
						</div>
					</div>
				</div>
            </div>
        </section>
		<div class="alert-banner" style="background-color:#1b6b76">
			Invited to a bid by a consultant? Here's what you need to know:  <a target="_blank" style="color:#C02942" href="https://www.contractcomplete.com/blog/retrieving-tender-documents-and-submitting-a-bid/"><u>Bidder Instructions</u></a>
		</div>
        <!--================End Home Banner Area =================-->
		
        <!--================Feature 1 Area =================-->
         <section class="made_life_area p_120" style="padding-top:70px;padding-bottom: 260px;background-color:white">
         	<div class="container">
         		<div class="row made_life_inner" style="text-align:center">
					<div class="col-lg-6 feature-img">
						<a href="img/features/contractor-2_raw.webp" data-lightbox="contractor-2" data-title="Accelerate your cash flow on awarded projects"><img src="img/features/contractor-2.webp"></img></a>	
					</div>
					<div class="col-lg-6 feature-text" style="margin:auto;padding-right:70px;padding-left:70px" >
						<table><tr><td>
						<img class="features-checkmark" src="img/checkmark.webp" />
						</td><td>
						<div class="large-screen-only features-header">Accelerate your cash flow on awarded projects</div>
						</td></tr>
						<tr><td></td>
							<td>
								<ul>
									<li>Track progress, outstanding A/R, A/P and holdbacks fast.</li>
									<li>Generate proposed changes and change orders instantly.</li>
									<li>Stay current and get paid for proposed changes and extras.</li>
								</ul>
							</td>
						</tr></table>
					</div>
         		</div>
         	</div>
         </section>
         <!--================End Feature 1 Area =================-->
		
		
		
         <section class="made_life_area p_120" style="padding-top:40px;background-color:#f6f6f7">
         	<div class="container">
         		<div class="row made_life_inner" style="text-align:center">
					<div class="col-lg-6 feature-text" style="margin:auto;padding-right:70px;padding-left:70px">
						<table>
							<tr>
								<td colspan="2">
									<div class="feature-img" style="padding-top:16px;padding-bottom:16px;text-align: center;margin-top:-200px">
											<a href="img/features/contractor-1_raw.webp" data-lightbox="contractor-1" data-title="The most accurate project budgets"><img src="img/features/contractor-1.webp"  /></a>
									</div>
								</td>
							</tr>
						<tr><td>
						<img class="features-checkmark" src="img/checkmark.webp" />
						</td><td>
						<div class="large-screen-only features-header">Modernize your estimating team</div>
						</td></tr></table>
						<table style="text-align:top">
							<tr>
								<td style="vertical-align: top;">
								</td>
								<td>
									<ul >
										<li>Invite subcontractors to bid electronically with ease.</li>
										<li>Secure submissions, no couriers required.</li>
										<li>Quote new jobs in a jiffy with access to historic data.</li>
									</ul>
								</td>
							</tr>
						</table>
					</div>
					<div class="col-lg-6 feature-text" style="margin:auto;padding-right:70px;padding-left:70px" >
						<table>
							<tr>
								<td colspan="2">
									<div class="feature-img no-padding-on-small-screen" style="padding-top:16px;padding-bottom:16px;text-align: center;">
											<a href="img/features/contractor-3_raw.webp" data-lightbox="contractor-3" data-title="The most accurate project budgets"><img src="img/features/contractor-3.webp"  /></a>
									</div>
								</td>
							</tr><tr><td style="padding-top: 30px;">
						<img class="features-checkmark" src="img/checkmark.webp" />
						</td><td>
						<div class="large-screen-only features-header">Create your own contracts &amp invite your consultants</div>
						</td></tr></table>
						<table style="text-align:top">
							<tr>
								<td style="vertical-align:top">
								</td>
								<td>
									<ul>
										<li>Keep all your contracts &amp project documents organized on one system.</li>
										<li>Collaborate with your consultants to speed up change orders and payment certificates.</li>
										<li>Web and mobile access from anywhere.</li>
									</ul>
								</td>
							</tr>
						</table>
					</div>
         		</div>
         	</div>
         </section>
		
		
		

		 <?php
			include 'inline_signup.html';
		 ?>