<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
		<link rel="stylesheet" href="css/font-awesome.min.css" />
		<link rel="stylesheet" href="fonts/open_sans/stylesheet.css" />
        <title>ContractComplete - Progress Billing</title>
		<?php
			include 'https_redirect.php';
			include 'css_common.php';
		?>
    </head>
    <body>

<?php 
	$safari = true;
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')) {
		$safari=false;
	}
	include 'header.html'; 
?>
        
<div class="product-container shaded-bg" style="">	
	<span class="product-subtitle">Invoice Tracking for Unit Price Contracts</span><br/>
	<span class="product-title">Progress Billing &amp; Payment<br/>Certification made Easy</span>
	<br/>
	<img src="img/progress_report.png" class="hero-image product-pdf-img" />
	<div style="clear:both;height:0px;"></div>
	<a class="book-demo" style="background-color:#0070BB" target="_blank" href="https://connect.contractcomplete.com/get-a-demo">Schedule a Demo</a>
</div>
<div class="product-container" style="margin-top:100px">	
	<div class="accent-title">Prevent<br/>Overbilling</div>
	<div class="divider-dot" style="background-color:#0aa89f">&nbsp</div>
	<div class="accent-title">Receive Consistent Invoices<br/> from your Contractor</div>
	<div class="divider-dot" style="background-color:#0070BB">&nbsp</div>
	<div class="accent-title">Instant, Error-Free<br/>Payment Certificates</div><br/>
	<!--
	<img src="img/contractor_invoice.png" style="max-width:45%;margin-top:-80px;margin-left:-250px" class="product-pdf-img" />
	<img src="img/contractor_invoice_coverpage.png" style="max-width: 600px;margin-left: -750px;margin-top: 450px;" class="product-pdf-img" />
	-->
	<img src="img/invoice_stack.png"  class="product-pdf-img borderless" />
	<div class="image-caption-wrapper" style="float:right">
		<span class="feature-title">Easy to Read and Understand</span>
		<div class="accent-title-larger">Contractors Create</br>Beautiful Progress<br/>Invoices</div>
		<p>
			No More Excel Errors in spreadsheets. Eliminate<br/>
			Inconsistent Formats. Rest assured it's the most<br/>
			up to date invoice.
		</p>
	</div>
	<div style="clear:both;height:1px"></div>
	<img src="img/pc_stack.png" class="product-pdf-img small-screen-only borderless" />
	<div class="image-caption-wrapper" style="float:left;text-align:right">
		<div class="caption-nested-div">
		<span class="feature-title" style="color:#0aa89f">Save Time</span>
		<div class="accent-title-larger">Instant Payment<Br/>Certificates</div>
		<p>
			Keep cash flowing.<br/>Reduce double data entry.<br/>Eliminate Errors.
		</p>
		</div>
	</div>
	<img src="img/pc_stack.png" class="product-pdf-img large-screen-only borderless" />
	<!--
	<img src="img/consultant_pc.png" style="max-width: 700px;width:35%;display:inline-block;margin-top:-100px" class="product-pdf-img" />
	<img src="img/consultant_pc_pdf.png" style="max-width: 400px;width:35%;display:inline-block;margin-left: -500px;margin-top: 300px;" class="product-pdf-img" />
	-->
	<div style="clear:both;height:80px"></div>

	<?php
		include 'testimonial_sbk.html'
	?>
	<div style="clear:both;height:40px"></div>
	<img src="img/progress_tracking.png" class="product-pdf-img borderless" />
	<!--
	<img src="img/contract_page.png" style="max-width:700px;margin-top:-200px" class="product-pdf-img" />
	<img src="img/mobile_contracts.png" style="border:none;box-shadow:none;max-height:500px; margin-left: -100px; margin-top: -200px;" class="product-pdf-img" />
	-->
	<div class="image-caption-wrapper" style="float: right;">
		<span class="feature-title">Centralize your Project Data</span>
		<div class="accent-title-larger">Track Progress Across<br/>All your Projects</div>
		<p>
			Know where your team is at within a glance, from<br/>anywhere in the world.
		</p>
	</div>

	<div style="clear:both;height:40px"></div>
	<img src="img/actionbox.png" class="product-pdf-img small-screen-only" />
	<div class="image-caption-wrapper" style="float:left;text-align:right">
		<div class="caption-nested-div">
			<span class="feature-title" style="color:#0aa89f">ActionBox Daily Reminders</span>
			<div class="accent-title-larger">Never Miss a<Br/>Contractor Invoice</div>
			<p>
				All submitted invoices go straight to your<br/>
				ActionBox, as well as your email and mobile<br/>
				App. Receive daily reminders so billing delays<br/>
				don't cause scheduling delays.
			</p>
		</div>
	</div>
	<img src="img/actionbox.png" class="product-pdf-img large-screen-only" />
	<div style="clear:both;height:80px"></div>
	
	<span class="accent-title-larger">See Other Solutions</span>
	<div style="clear:both;height:60px"></div>
	<div style="clear:both"></div>
	<a href="projectmanagement.php"><div class="accent-title">Project Management</div></a>
	<div class="divider-dot" style="background-color:#0aa89f">&nbsp</div>
	<a href="budgeting.php"><div class="accent-title">Budget Creation</div></a>
	<div class="divider-dot" style="background-color:#0070BB">&nbsp</div>
	<a href="bidding.php"><div class="accent-title">Bidding Management</div></a><br/>
	
	<div style="clear:both;height:80px"></div>

	<?php
		include 'schedule_demo2.html'
	?>
</div>
<?php 
include 'footer.php'; 
include 'common_scripts.html';
?>
		 
		 <script type="text/javascript">
			var w = window;
			var loadIframe = function(){
				var vidDefer = document.getElementsByTagName('iframe');
				for (var i=0; i<vidDefer.length; i++) {
					if(vidDefer[i].getAttribute('data-src')) {
						vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
					} 
				} 
			}
			
			if (w.addEventListener) { w.addEventListener("load", loadIframe, false); }
			else if (w.attachEvent) { w.attachEvent("onload",loadIframe); }
		 </script>
		 