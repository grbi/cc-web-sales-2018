<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
		<link rel="stylesheet" href="css/font-awesome.min.css" />
        <title>ContractComplete - Support</title>
		<?php
			include 'https_redirect.php';
			include 'css_common.php';
		?>
    </head>
    <body>

<?php 
	$safari = true;
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')) {
		$safari=false;
	}
	include 'header.html'; 
?>
        
        
		<?php
			include 'signup-prompt.php';
        ?>

        <!--================Home Banner Area =================-->
        <section class="banner_area">
			<div class="video-wrapper" style="position:absolute;top:0px;left:0px;width:100%;overflow:hidden;height:600px">
				<!--<img src="img/consultant-banner.jpeg" style="min-width:100%;min-height:600px;height:600px"></img>-->
			</div>
            <div class="banner_inner d-flex align-items-center">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container" style="padding-top:150px;height:600px">
					<div class="banner_content text_center consultants-banner" style="width:500px;float:left">
						<h1>Need additional support?</h1><br/>
						<h2 style="margin-top: -30px;">We've got you covered.</h2><br/>
						<h3>
							Step by step.  From tendering to<br/>payment certificates.
						</h3>
						<div class="small-screen-only" style="width:100%;clear:both;height:100px">&nbsp;</div>
					</div>
					<div   class="banner-image-section" style="max-width:50%;float:right;display:table-cell;vertical-align:middle;height:600px;padding-top:50px">
						<img src="img/arrow circle.<?php echo ($safari ?  'webp' : 'png'); ?>" style="border-radius:10px;box-shadow:none;-webkit-box-shadow:none;margin-top: -70px;" class="home-banner-img top-banner-image" />
						<img src="img/Man pointing.<?php echo ($safari ?  'webp' : 'png'); ?>" style="border-radius:10px;box-shadow:none;-webkit-box-shadow:none;" class="home-banner-img top-banner-image resources-banner-img"></img>
						<div class="dot dot1" style="">
							&nbsp;
						</div>
					</div>
				</div>
            </div>
        </section>
		<div class="alert-banner" style="background-color:#cfdd48">
			To help businesses during these challenging times, we are offering 3 months at 75% off.&nbsp;&nbsp;<a target="_blank" style="color:#C02942" href="https://connect.contractcomplete.com/75-percent-off"><u>Find out more</u></a>
		</div>
        

         <!--================Feature 2 Area =================-->
         <section class="made_life_area p_120" style="padding-top:40px;background-color:white;text-align:center">
			<h1 style="color:#0070bb">ContractComplete Resources</h1>
			<h2 style="color:black;font-size: 20px;"><b style="color:black">There are several ways you can get help with ContractComplete.</b></h2>
			<p><b style="color:black">Can't find what you're looking for?</b>  Contact us by email or phone, or start a live chat with a ContractComplete representative.</p>
			
        	<div class="container resources-container">
        		<div class="row footer_inner">
        			<div class="col-lg-4 resources-col">
						<div class="res-img-wrapper">
							<img src="img/Documentation icon.<?php echo ($safari ?  'webp' : 'png'); ?>" alt="Documentation" />
						</div>
						<p class="resources-title">Documentation</p>
						<p class="resources-description">We've amassed several resources which can help you get started with ContractComplete and answer any questions you might have.</p>
						<b class="resources-link">See full documentation list below.</b>
					</div>
        			<div class="col-lg-4 resources-col">
						<div class="res-img-wrapper">
							<img src="img/Video Icon.<?php echo ($safari ?  'webp' : 'png'); ?>" alt="Video Icon" />
						</div>
						<p class="resources-title">Videos</p>
						<p class="resources-description">We have compiled several tutorial videos which demonstrate various tasks that can be accomplished within ContractComplete.</p>
						<b class="resources-link"><a href="videos.php">View full collection of tutorial videos.</a></b>
					</div>
        			<div class="col-lg-4 resources-col">
						<div class="res-img-wrapper">
							<img src="img/Blog Icon.<?php echo ($safari ?  'webp' : 'png'); ?>" alt="Blog Icon" />
						</div>
						<p class="resources-title">Blog</p>
						<p class="resources-description">Want to keep up to date with what's happening in the world of ContractComplete?  We also post feature announcements there.</p>
						<b class="resources-link"><a href="https://www.contractcomplete.com/blog/">Check out our blog.</a></b>
					</div>
				</div>
			</div>
		</section>


        	<div class="container" style="max-width: 100%;">
        		<div class="row">
        			<div class="col-lg-3" style="background-color:#dff4f3;border: 2px solid white;">
						<div class="work_inner docs-content" style="width:100%" >
							<div class="" style="padding: 32px;padding-left:64px">
								<?php
									class related_document{
										public $name;
										public $src;
									};
								
									$page = 'overview';
									if ( isset($_GET['page']) ) {
										$page = $_GET['page'];
									}
								?>
								<img src="img/docs_inner.<?php echo ($safari ?  'webp' : 'png'); ?>"/>
								<h2 style="color: #0aa89f;font-weight: 300;font-size: 20px;margin-top: 20px;">Documentation</h2>
							
								<ul style="margin-left: -40px;">	
							
								<li>
									<a href="?page=overview#content" class="<?php if($page == 'overview') echo "selected-doc"; else echo ""; ?>">ContractComplete Overview</a>
								</li>
								
								
								<li>
									<a href="?page=my_profile#content" class="<?php if($page == 'my_profile') echo "selected-doc"; else echo ""; ?>">My Profile</a>
								</li>
								
								
								<li>
								<details style="margin-left: -18px;">
								<summary>
									<a href="?page=help#content" class="<?php if($page == 'help') echo "selected-doc"; else echo ""; ?>">Help</a>
								</summary>
								
										<ul>
											<li>
												<a href="?page=documentation#content" class="<?php if($page == 'documentation') echo "selected-doc"; else echo ""; ?>">Documentation</a>
											</li>
											
											<li>
												<a href="?page=live_chat#content" class="<?php if($page == 'live_chat') echo "selected-doc"; else echo ""; ?>">Live Chat</a>
											</li>
											
											<li>
												<a href="?page=send_feedback#content" class="<?php if($page == 'send_feedback') echo "selected-doc"; else echo ""; ?>">Send Feedback</a>
											</li>
										</ul>
										
								</details>
								</li>
								
								
								
								<li>
								<details style="margin-left: -18px;">
								<summary>
								
									<a href="?page=my_company#content" class="<?php if($page == 'my_company') echo "selected-doc"; else echo ""; ?>">My Company</a>
								</summary>
										<ul>
											<li>
												<a href="?page=profile#content" class="<?php if($page == 'profile') echo "selected-doc"; else echo ""; ?>">Profile</a>
											</li>
											
											<li>
												<a href="?page=user_configuration#content" class="<?php if($page == 'user_configuration') echo "selected-doc"; else echo ""; ?>">User Configuration</a>
											</li>
											
											<li>
												<a href="?page=contacts#content" class="<?php if($page == 'contacts') echo "selected-doc"; else echo ""; ?>">Contacts</a>
											</li>
											
											<li>
												<a href="?page=integrations#content" class="<?php if($page == 'integrations') echo "selected-doc"; else echo ""; ?>">Integrations</a>
											</li>
											
											<li>
												<a href="?page=invoices#content" class="<?php if($page == 'invoices') echo "selected-doc"; else echo ""; ?>">Invoices</a>
											</li>
											
											<li>
												<a href="?page=subscriptions#content" class="<?php if($page == 'subscriptions') echo "selected-doc"; else echo ""; ?>">Subscriptions</a>
											</li>
											
											<li>
												<a href="?page=estimation#content" class="<?php if($page == 'estimation') echo "selected-doc"; else echo ""; ?>">Estimation</a>
											</li>
										</ul>
								</details>
								</li>
								
								<li>
									<a href="?page=my_action_box#content" class="<?php if($page == 'my_action_box') echo "selected-doc"; else echo ""; ?>">My Action Box</a>
								</li>
								<br/>
						
								<li>
									<a href="?page=contract_setup#content" class="<?php if($page == 'contract_setup') echo "selected-doc"; else echo ""; ?>"><strong>Contract Setup</strong></a>
								</li>
								
								
								
								<li>
								<details style="margin-left: -18px;">
								<summary>
								
									<a href="?page=my_bids_tenders_quotes#content" class="<?php if($page == 'my_bids_tenders_quotes') echo "selected-doc"; else echo ""; ?>">My Bids, Tenders & Quotes</a>
								</summary>
									<ul>
										<li>
											<a href="?page=quote#content" class="<?php if($page == 'quote') echo "selected-doc"; else echo ""; ?>">Quote</a>
										</li>
										
										<li>
										<details style="margin-left: -18px;">
										<summary>
											<a href="?page=tender#content" class="<?php if($page == 'tender') echo "selected-doc"; else echo ""; ?>">Tender</a>
										</summary>
												<ul>
													<li>
														<a href="?page=tender_draft#content" class="<?php if($page == 'tender_draft') echo "selected-doc"; else echo ""; ?>">Draft</a>
													</li>
													
													<li>
													<details style="margin-left: -18px;">
													<summary>
														<a href="?page=tendering#content" class="<?php if($page == 'tendering') echo "selected-doc"; else echo ""; ?>">Tendering</a>
													</summary>
															<ul>
																<li>
																	<a href="?page=tendering_stakeholder#content" class="<?php if($page == 'tendering_stakeholder') echo "selected-doc"; else echo ""; ?>">Tendering Stakeholder's View</a>
																</li>
																<li>
																	<a href="?page=tendering_bidder#content" class="<?php if($page == 'tendering_bidder') echo "selected-doc"; else echo ""; ?>">Tendering Bidder's View</a>
																</li>
															</ul>
													</details>
													</li>
													
													
													<li>
													<details style="margin-left: -18px;">
													<summary>
														<a href="?page=post_tender#content" class="<?php if($page == 'post_tender') echo "selected-doc"; else echo ""; ?>">Post Tender</a>
													</summary>
															<ul>
																<li><a href="?page=reopen_tender#content" class="<?php if($page == 'reopen_tender') echo "selected-doc"; else echo ""; ?>">Reopen Tender</a></li>
																<li><a href="?page=allow_resubmission#content" class="<?php if($page == 'allow_resubmission') echo "selected-doc"; else echo ""; ?>">Allow Resubmission</a></li>
																<li><a href="?page=manual_bid_entry#content" class="<?php if($page == 'manual_bid_entry') echo "selected-doc"; else echo ""; ?>">Manual Bid Entry</a></li>
																<li><a href="?page=negotiations#content" class="<?php if($page == 'negotiations') echo "selected-doc"; else echo ""; ?>">Negotiations</a></li>
																<li><a href="?page=awarding#content" class="<?php if($page == 'awarding') echo "selected-doc"; else echo ""; ?>">Awarding a Contract</a></li>
															</ul>
													</details>		
													</li>
										
												</ul>
										</details>
										</li>
									</ul>
									
								</details>
								</li>
								
								<li>
								<details  style="margin-left: -18px;">
								<summary>
									<a href="?page=my_contracts#content" class="<?php if($page == 'my_contracts') echo "selected-doc"; else echo ""; ?>">My Contracts</a>
								</summary>
										<ul>
											<li>
												<details style="margin-left: -18px;">
													<summary>
														<a href="?page=awarded_collaborative_mode#content" class="<?php if($page == 'awarded_collaborative_mode') echo "selected-doc"; else echo ""; ?>">Awarded Collaborative mode</a>
													</summary>
															<ul>
																<li>
																	<details style="margin-left: -18px;">
																		<summary>
																			<a href="?page=awarded_draft#content" class="<?php if($page == 'awarded_draft') echo "selected-doc"; else echo ""; ?>">Draft</a>
																		</summary>
																			<ul>
																				<li><a href="?page=awarded_draft_stakeholder_view#content" class="<?php if($page == 'awarded_draft_stakeholder_view') echo "selected-doc"; else echo ""; ?>">Draft Stakeholder's view</a></li>
																				<li><a href="?page=awarded_draft_contractor_view#content" class="<?php if($page == 'awarded_draft_contractor_view') echo "selected-doc"; else echo ""; ?>">Draft Contractor's view</a></li>
																			</ul>
																	</details>
																</li>
																<li>
																	<details style="margin-left: -18px;">
																		<summary>
																			<a href="?page=collaborative_mode_inprogress#content" class="<?php if($page == 'collaborative_mode_inprogress') echo "selected-doc"; else echo ""; ?>">In Progress</a>
																		</summary>
																			<ul>
																				<li><a href="?page=inprogress_stakeholder#content" class="<?php if($page == 'inprogress_stakeholder') echo "selected-doc"; else echo ""; ?>">In Progress Stakeholder's view</a></li>
																				<li><a href="?page=inprogress_contractor#content" class="<?php if($page == 'inprogress_contractor') echo "selected-doc"; else echo ""; ?>">In Progress Contractor's view</a></li>
																			</ul>
																	</details>
																</li>
																<li><a href="?page=collaborative_mode_completed#content" class="<?php if($page == 'collaborative_mode_completed') echo "selected-doc"; else echo ""; ?>">Completed</a></li>
															</ul>
												</details>
											</li>
											
											<li>
												<a href="?page=awarded_administrator_solo#content" class="<?php if($page == 'awarded_administrator_solo') echo "selected-doc"; else echo ""; ?>">Awarded Administrator Solo</a>
											</li>
											
											<li>
												<a href="?page=awarded_contractor_solo#content" class="<?php if($page == 'awarded_contractor_solo') echo "selected-doc"; else echo ""; ?>">Awarded Contractor Solo</a>
											</li>
										</ul>
								</details>
								</li>
								
								<li>
									<a href="?page=clone_a_contract#content" class="<?php if($page == 'clone_a_contract') echo "selected-doc"; else echo ""; ?>">Clone a Contract</a>
								</li>
								</br></br>
								
								<li>
									<h4 style="color:#17a2b8";>Common Pages</h4>
										<ul style="margin-left: -40px;">
											<li>
											<details style="margin-left: -18px;">
											<summary>
												<a href="?page=overview_page#content" class="<?php if($page == 'overview_page') echo "selected-doc"; else echo ""; ?>">Overview Page</a>
											</summary>
		
													<ul>
														<li><a href="?page=bidder_selection#content" class="<?php if($page == 'bidder_selection') echo "selected-doc"; else echo ""; ?>">Bidder/Contractor Selection</a></li> 
													</ul>
											
											</details>
											</li>
											
											<li>
											<details style="margin-left: -18px;">
											<summary>
											
												<a href="?page=schedule_of_prices#content" class="<?php if($page == 'schedule_of_prices') echo "selected-doc"; else echo ""; ?>">Schedule of Prices</a>
											</summary>
													<ul>
														<li>
															<a href="?page=importer#content" class="<?php if($page == 'importer') echo "selected-doc"; else echo ""; ?>">Importing from Excel</a>
														</li>
														
														<li>
															<a href="?page=smart_importer#content" class="<?php if($page == 'smart_importer') echo "selected-doc"; else echo ""; ?>">Smart Importer</a>
														</li>
													</ul>
											</details>
											</li>
											
											<li>
												<a href="?page=documents#content" class="<?php if($page == 'documents') echo "selected-doc"; else echo ""; ?>">Documents</a>
											</li>
											
											<li>
												<a href="?page=provisional_items#content" class="<?php if($page == 'provisional_items') echo "selected-doc"; else echo ""; ?>">Provisional Items</a>
											</li>
											
											<li>
												<a href="?page=qualification_questions#content" class="<?php if($page == 'provisional_items') echo "selected-doc"; else echo ""; ?>">Qualification Questions</a>
											</li>
											
											
											<li>
												<a href="?page=rfi#content" class="<?php if($page == 'provisional_items') echo "selected-doc"; else echo ""; ?>">RFI</a>
											</li>
											
											
											<li>
												<a href="?page=submittal#content" class="<?php if($page == 'provisional_items') echo "selected-doc"; else echo ""; ?>">Submittals</a>
											</li>
																					
											
											<li>
											<details style="margin-left: -18px;">
											<summary>
												<a href="?page=change_orders_page#content" class="<?php if($page == 'change_orders_page') echo "selected-doc"; else echo ""; ?>">Change Orders Page</a>
											</summary>
													<ul>
														<li>
															<a href="?page=proposed_change#content" class="<?php if($page == 'proposed_change') echo "selected-doc"; else echo ""; ?>">Proposing Changes</a>
														</li>
														
														<li>
															<a href="?page=change_orders#content" class="<?php if($page == 'change_orders') echo "selected-doc"; else echo ""; ?>">Change Orders</a>
														</li>
													</ul>
											</details>
											</li>
											
											<li>
												<a href="?page=progress_reports#content" class="<?php if($page == 'progress_reports') echo "selected-doc"; else echo ""; ?>">Progress Reports</a>
											</li>
											
											<li>
											<details style="margin-left: -18px;">
											<summary>
												<a href="?page=invoice_payment_certificate#content" class="<?php if($page == 'invoice_payment_certificate') echo "selected-doc"; else echo ""; ?>">Invoice/Payment Certificate</a>
											</summary>
													<ul>
														<li>
															<a href="?page=invoicing#content" class="<?php if($page == 'invoicing') echo "selected-doc"; else echo ""; ?>">Invoicing</a>
														</li>
														
														<li>
															<a href="?page=payment_certificates#content" class="<?php if($page == 'payment_certificates') echo "selected-doc"; else echo ""; ?>">Payment Certificates</a>
														</li>
													</ul>
											</details>
											</li>
										</ul>
										
								</li>
								
							</ul>
						
						</div>
									
							
							
			

				
							
							<div class="breadcrumbs" style="display:none">
								<div class="inner">
									<span><a href="?page=home#content">Documentation Home&nbsp;&nbsp;</a></span><i class="fa fa-arrow-right breadcrumbs-spacer"></i>
									<?php 
										if($page == 'schedule_of_prices'
											|| $page == 'overview_page'
											|| $page == 'provisional_items'
											|| $page == 'documents'
											|| $page == 'bidder_selection'
											|| $page == 'importer'){
											?>
												<span class="selected">Contract Setup&nbsp;&nbsp;</span>
											<?php
										}
										
										if($page == 'bidding' 
												|| $page == 'addenda'){
											?>
												<span class="selected">Tendering&nbsp;&nbsp;</span>
											<?php
										}
										
										if($page == 'negotiations'
											|| $page == 'allow_resubmission'){
											?>
												<span class="selected">Awarding&nbsp;&nbsp;</span>
											<?php
										}
										
										if($page == 'proposed_change'
											|| $page == 'change_orders'
											|| $page == 'progress_reports'
											|| $page == 'invoicing'
											|| $page == 'payment_certificates'){
											?>
												<span class="selected">Post Tendering&nbsp;&nbsp;</span>
											<?php
										}
									?>
								</div>
							</div>
							</br><br/>
							<div>
								<?php
									$hasRelated = true;
									
									if($page == 'bidding' || $page == 'home'){
										$hasRelated = false;
									}
									
									if ($hasRelated){
										?>
											<div class="related" style="margin-left: 64px;">
												<span class="contents-title">Related Articles</span><br/><br/>
													<?php
												
														if($page == 'contract_setup' 
															|| $page == 'overview' 
															|| $page == 'awarded_collaborative_mode' 
															|| $page == 'awarded_administrator_solo' 
															|| $page == 'awarded_contractor_solo' 
															|| $page == 'tender'
															|| $page == 'quote'){
																?>
																	<ul>
																		<li <?php if($page=="overview") echo "style='display:none'"; ?>><a href="?page=overview#content">ContractComplete Overview</a></li>
																		<li <?php if($page=="awarded_collaborative_mode") echo "style='display:none'"; ?>><a href="?page=awarded_collaborative_mode#content">Awarded Collaborative mode</a></li>
																		<li <?php if($page=="awarded_administrator_solo") echo "style='display:none'"; ?>><a href="?page=awarded_administrator_solo#content">Awarded Administrator Solo</a></li>
																		<li <?php if($page=="awarded_contractor_solo") echo "style='display:none'"; ?>><a href="?page=awarded_contractor_solo#content">Awarded Contractor Solo</a></li>
																		<li <?php if($page=="tender") echo "style='display:none'"; ?>><a href="?page=tender#content">Tender</a></li>
																		<li <?php if($page=="quote") echo "style='display:none'"; ?>><a href="?page=quote#content">Quote</a></li>
																	</ul>
																<?php
														}
													
														if($page == 'tendering' 
															|| $page == 'addenda'){
															?>
															
														
															
																<ul>
																	<li <?php if($page=="tendering") echo "style='display:none'"; ?>><a href="?page=tendering#content">Tendering</a></li>
																	<li <?php if($page=="addenda") echo "style='display:none'"; ?>><a href="?page=addenda#content">Issuing Addenda</a></li>
																</ul>
															<?php
														}
														
														if($page == 'awarding' 
															|| $page == 'negotiations'
															|| $page == 'allow_resubmission'){
															?>
																<ul>
																	<li <?php if($page=="awarding") echo "style='display:none'"; ?>><a href="?page=awarding#content">Awarding a Contract</a></li>
																	<li <?php if($page=="negotiations") echo "style='display:none'"; ?>><a href="?page=negotiations#content">Negotiating</a></li>
																	<li <?php if($page=="allow_resubmission") echo "style='display:none'"; ?>><a href="?page=allow_resubmission#content">Allow Resubmission</a></li>
																</ul>
															<?php
														}
													
														if($page == 'post_tender' 
															|| $page == 'proposed_change'
															|| $page == 'change_orders'
															|| $page == 'progress_reports'
															|| $page == 'invoicing'
															|| $page == 'payment_certificates'){
															?>
																<ul>
																	<li <?php if($page=="post_tender") echo "style='display:none'"; ?>><a href="?page=post_tender#content">Post Tender Home</a></li>
																	<li <?php if($page=="proposed_change") echo "style='display:none'"; ?>><a href="?page=proposed_change#content">Proposed Changes</a></li>
																	<li <?php if($page=="change_orders") echo "style='display:none'"; ?>><a href="?page=change_orders#content">Change Orders</a></li>
																	<li <?php if($page=="progress_reports") echo "style='display:none'"; ?>><a href="?page=progress_reports#content">Progress Reports</a></li>
																	<li <?php if($page=="invoicing") echo "style='display:none'"; ?>><a href="?page=invoicing#content">Invoicing</a></li>
																	<li <?php if($page=="payment_certificates") echo "style='display:none'"; ?>><a href="?page=payment_certificates#content">Payment Certificates</a></li>
																</ul>
															<?php
														}
													
														if($page == 'overview'){
															?>
																<ul>
																	<li <?php if($page=="user_configuration") echo "style='display:none'"; ?>><a href="?page=user_configuration#content">User Configuration</a></li>
																</ul>
															<?php
														}
													?>
											</div>
										<?php
									}
								?>
							</div>
						</div>
					</div>
					
        			<div class="col-lg-9 main-docs-content" style="background-color:#efefef;border: 2px solid white;padding: 20px;padding-top: 60px;">
						<img class="docs-title-arrow" src="img/triangle.<?php echo ($safari ?  'webp' : 'png'); ?>" />
							
			<a name="content"></a>
							<?php
								include 'docs/'.$page.'.html';
							?>
					</div>
				</div>
			</div>
		 <span id="btnScrollTop" style=" position:fixed; display:none; color:#146b75;  z-index:99; font-size:50px; width:200; right:30px;  bottom:150px; cursor: pointer;">
			<i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
		 </span>
		 

		 
		 
		 <?php
			include 'inline_signup.html';
		 ?>

<?php 

include 'common_scripts.html';
include 'footer.php'; 

?>
		 
		 <script type="text/javascript">
			var w = window;
			var loadIframe = function(){
				var vidDefer = document.getElementsByTagName('iframe');
				for (var i=0; i<vidDefer.length; i++) {
					if(vidDefer[i].getAttribute('data-src')) {
						vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
					} 
				} 
			}
			
			if (w.addEventListener) { w.addEventListener("load", loadIframe, false); }
			else if (w.attachEvent) { w.attachEvent("onload",loadIframe); }

			//Open menu dynamically
			$(document).ready(function(){
				var selectedEl = $('.selected-doc');
				selectedEl.css("font-weight", "bold").css('color', '#c75050').css('font-size', '18px');
				selectedEl.parents().map(function(){
					var currentTag = this.tagName;
					if(currentTag.toLowerCase() == 'details'){
						$(this).attr("open", "true");
					}
				});
				
				// scroll to top
				$('#btnScrollTop').click(function(){
					window.scrollTo({ top: 0, behavior: 'smooth' });
				});
				
				/*Admin solo page & Post tender*/
				
				function hideAllButtons(time){
					$('#toggled1, #toggled2, #toggled3').hide(time);
					$('#doToggle1, #doToggle2, #doToggle3').attr('isShow', 'false');
				}
				hideAllButtons(0);
				function resetButtonStyle(){
					$('#doToggle1, #doToggle2, #doToggle3').css({
						"background-color": "#17a2b8"
					});
				};
				$('#doToggle1').click(function(){console.log($(this).attr('isShow'));
					resetButtonStyle();
					if($(this).attr('isShow') == "true"){
						hideAllButtons();
					}else{
						hideAllButtons();
						$('#toggled1').show(500);
						$('#doToggle1').css('background-color', '#146b75');
						$(this).attr('isShow', 'true');
					}
				});
				
				$('#doToggle2').click(function(){
					resetButtonStyle();
					if($(this).attr('isShow') == "true"){
						hideAllButtons();
					}else{
						hideAllButtons();
						$('#toggled2').show(500);
						$('#doToggle2').css('background-color', '#146b75');
						$(this).attr('isShow', 'true');
					}
				});
				$('#doToggle3').click(function(){
					resetButtonStyle();
					if($(this).attr('isShow') == "true"){
						hideAllButtons();
					}else{
						hideAllButtons();
						$('#toggled3').show(500);
						$('#doToggle3').css('background-color', '#146b75');
						$(this).attr('isShow', 'true');
					}
				});
						
				/*Admin Solo page & Post tender*/
				
				/*Negotiation*/
				$('#negotiation1').click(function(){ 
					$('#negotiation1_text').toggle(500);
				});
				
				$('#negotiation2').click(function(){
					$('#negotiation2_text').toggle(500);
				});
				/*Negotiation*/
				
				/*Post Tender stakeholder's view*/
				
				$('#negotiate').click(function(){ 
					$('#negotiate_text').toggle(500);
				});
				
				$('#allowResubmission').click(function(){
					$('#allowResubmission_text').toggle(500);
				});
				
				$('#manualBidEntry').click(function(){ 
					$('#manualBidEntry_text').toggle(500);
				});
				
				
				
			/*RFI page-Donut Chart*/
			$(".quarter_draft").click(function(){
				$("#rfi_draft, #rfi_submitted, #rfi_responded, #rfi_reopened").hide();
				$("#rfi_draft").show();
			});
				
			$(".quarter_submitted").click(function(){
				$("#rfi_draft, #rfi_submitted, #rfi_responded, #rfi_reopened").hide();
				$("#rfi_submitted").show();
			});
				
			$(".quarter_responded").click(function(){
				$("#rfi_draft, #rfi_submitted, #rfi_responded, #rfi_reopened").hide();
				$("#rfi_responded").show();
			});
				
			$(".quarter_reopened").click(function(){
				$("#rfi_draft, #rfi_submitted, #rfi_responded, #rfi_reopened").hide();
				$("#rfi_reopened").show();
			});
			/*RFI page-Donut Chart*/
			
			
			
			/*Submittals page*/
			
			
				$('#submittal').click(function(){ 
					$('#submittal_text').toggle(500);
				});
				
				$('#submittalRequest').click(function(){
					$('#submittalRequest_text').toggle(500);
				});
			
			/*Submittals page*/
			
			
				
				
				
				/*Post Tender stakeholder's view*/	
				
			});
			var toTopButton = document.getElementById("btnScrollTop");
			document.querySelector('body').onscroll = function(){//whenever they scroll
			  if (window.scrollY > 150)//if scroll is 150px from top
				toTopButton.style.display = "block";//if they scroll down, show
			  else
				toTopButton.style.display = "none";//if they scroll up, hide
			};
			
			
		 </script>
		 