<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
        <title>ContractComplete - FAQ</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="vendors/linericon/style.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
        <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="vendors/animate-css/animate.css">
        <!-- main css -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="vendors/jquery-ui/jquery-ui.css">
    </head>
    <body>
        
		<?php 
			include 'header.html';
		?>
		
		
        <!--================Home Banner Area =================-->
        <section class="banner_area large-screen-only">
			<div class="item" style="display:table-cell;text-align:center;vertical-align:middle;height:600px;width:100vw;padding-top: 65px;background-color: #f3f3f3;">
					<div class="carousel-padded-item-alt" style="display:inline-block;max-width:500px;margin-right:60px;text-align:left;padding-top:100px">
						<h1>FREQUENTLY ASKED QUESTIONS</h1>
						<div class="carousel-subheader">We get these questions a lot. Hopefully you find the answers helpful.</div><br/>
						<a href="https://connect.contractcomplete.com/get-a-demo" class="tickets_btn_nav" style="width: 200px">Request a Demo Instead</a>
					</div>				
					<img src="img/youtube-cartoon.png" style="display:inline-block; max-width:500px;max-height:500px;margin-top:-140px"></img>
			</div>
        </section>
        <section class="banner_area small-screen-only" style="width:100%">
			<div class="item" style="display:block;text-align:center;vertical-align:middle;height:600px;width:100%;padding-top:140px;background-color: #f3f3f3;">
					<div class="carousel-padded-item-alt" style="display:inline-block;max-width:100%;text-align:center;padding-top:100px">
						<img src="img/youtube-cartoon.png" style="display:inline-block; max-width:80%;max-height:200px;margin-top:-100px;padding-bottom:10px;"></img>
						<h1>FAQs</h1>
						<div class="carousel-subheader" style="text-align:center">Questions we are often asked</div><br/>
						<a href="https://connect.contractcomplete.com/get-a-demo" target="_blank" class="tickets_btn" style="width: 200px">Request a Demo Instead</a>
					</div>
			</div>
        </section>
        <!--================End Home Banner Area =================-->
        
        <!--================Work Area =================-->
        <section class="work_area p_120">
        	<div class="container">				
				<div class="faq-question" style="padding-top:40px;">How much time investment does it take to get started with ContractComplete?</div>
				<div class="faq-answer">
					<ul>
						<li>We have designed an easy and effective onboarding process that allows new users to start their first bid/tender in <b style="color:#777777">as little at one hour.</b></li>
						<li>Our onboarding strategy involves a walkthrough of the new contract process with one of our experts.</li>
						<li>Our goal is to make sure you are seeing the benefits in as little time as possible!</li>
						Click the image below to enlarge<br/>
						<a href="img/CustomerJourneyMapCompact.png" target="_blank" data-title="Your first contract">
							<img src="img/CustomerJourneyMapCompact.png" style="width:800px;max-width:100%"></img>
						</a>
					</ul>
				</div>
				<div class="faq-question" style="padding-top:40px;">Do bidders have to pay to submit bids or tenders?</div>
				<div class="faq-answer">
					<ul>
						<li>No. There is no charge for the bidders.</li>
						<li>We want to make it as easy as possible for our users to invite bidders so we've chosen to allow them to submit for Free.</li>
					</ul>
				</div>
				
				<div class="faq-question">How much time does it take me to create and send my first bid or tender?</div>
				<div class="faq-answer">
					<ul>
						<li>You can have your first tender out on your first day using the software. </li>
						<li>You can easily import your unit price contracts into our program from an Excel spreadsheet.</li>
						<li>Invite your bidders, set a deadline, add some instructions, upload your documents and hit Send!</li>
					</ul>
				</div>
				
				<div class="faq-question">Can the bidders see my budget? Can they see who else is invited?</div>
				<div class="faq-answer">
					<ul>
						<li>No, of course they cannot see your budget, that's for your eyes only. </li>
						<li>And no, they cannot see who else is invited. </li>
					</ul>
				</div>
				
				<div class="faq-question">Does ContractComplete create Bid Comparison Analysis?</div>
				<div class="faq-answer">
					<ul>
						<li>Typically, office staff will manually assemble a Tender Comparison otherwise known as bid analysis from pdf forms or excel bid forms received from bidding contractors.</li>
						<li>With ContractComplete bids are submitted on a web-based form and line by line comparison is created instantly at bid deadline.</li>
					</ul>
				</div>
				
				<div class="faq-question">Where is my data stored?</div>
				<div class="faq-answer">
					<ul>
						<li>Many of our current users were using their desktop PC, in-house server or, some sort of generic cloud-based document sharing application (eg. Google drive, dropbox).</li>
						<li>ContractComplete comes with unlimited data storage with <a target="_blank" href="https://aws.amazon.com/products/storage/">AWS - Amazon Web Services</a>. </li>
					</ul>
				</div>
				
				<div class="faq-question">How can I access ContractComplete?</div>
				<div class="faq-answer">
					<ul>
						<li>Many offices are restricted to accessing their documents at work from a local server.</li>
						<li>Our SaaS is web-based and users can access their documents from any web browser (Chrome, Mozilla, Safari, edge) anywhere with proper sign in credentials (username + Password)</li>
					</ul>
				</div>
				
				<div class="faq-question">How secure is my data on ContractComplete platform?</div>
				<div class="faq-answer">
					<ul>
						<li>Customer data is stored in a secure database protected by multiple firewalls and security protocols.</li>
						<li>All ContractComplete employees accessing sensitive information are required to sign an NDA.</li>
						<li><a target="_blank" href="https://www.contractcomplete.com/blog/etender-bid-encryption-assurance-for-the-paranoid-contractor/">Static encryption</a> of contractors' bids is an optional feature available free of charge.</li>
					</ul>
				</div>
				
				<div class="faq-question">Do I need a smartphone/ mobile to use ContractComplete?</div>
				<div class="faq-answer">
					<ul>
						<li>No, ContractComplete does have and <a target="_blank" href="https://apps.apple.com/ca/app/contract-complete/id1148784554">Apple iOS</a> and <a target="_blank" href="https://play.google.com/store/apps/details?id=com.grbi.contractcomplete">android</a> application for mobile use. From the app, documents can be viewed and downloaded and real-time progress on each line item can be viewed.</li>
						<li>Most contract modifications are required to be done from desktop computer best experience using chrome, mozilla, or safari.</li>
					</ul>
				</div>
				
				<div class="faq-question">What document formats am I able to share?</div>
				<div class="faq-answer">
					<ul>
						<li>The most commonly accepted file type in private and public construction is PDF (printable document format). However, in certain circumstances larger CAD files maybe shared with contractors for greater ease in estimating quantity take-offs. </li>
						<li>ContractComplete will store most document formats - PDF, all microsoft office formats, CAD, CSV, zip.  However only certain formats can be previewed in the browser.  Others must be downloaded first.</li>
					</ul>
				</div>
				
				<div class="faq-question">How do I get my excel spreadsheet onto ContractComplete?</div>
				<div class="faq-answer">
					<ul>
						<li>Most other platforms require the use of a strict template and copy and pasting your data in  a specific way.</li>
						<li>Using artificial intelligence(ai), ContractComplete's smart-spreadsheet-importer is able to recognize xlsm and xlsx documents of a wide range of column arrangements, sub-groupings and line item numbering.</li>
					</ul>
				</div>
        		
				<div style="clear:both"></div>
			</div>
		</section>
		
		
        <!--================End Work Area =================-->
        
		<?php
			include 'signup-prompt.php';
        ?>
	   
		<?php
			include 'footer.php';
		?>
        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/stellar.js"></script>
        <script src="vendors/lightbox/simpleLightbox.min.js"></script>
        <script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="vendors/isotope/isotope-min.js"></script>
        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="js/mail-script.js"></script>
        <script src="vendors/counter-up/jquery.waypoints.min.js"></script>
        <script src="vendors/counter-up/jquery.counterup.min.js"></script>
        <script src="js/theme.js"></script>
        <script src="vendors/jquery-ui/jquery-ui.js"></script>
		<?php
			include 'common_scripts.html';
		?>
    </body>
</html>