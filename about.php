<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
		<link rel="stylesheet" href="css/font-awesome.min.css" />
        <title>ContractComplete - Testimonials</title>
		<?php
			include 'https_redirect.php';
			include 'css_common.php';
		?>
    </head>
    <body>

<?php include 'header.html'; ?>
        
        
		<?php
			include 'signup-prompt.php';
        ?>

        <!--================Home Banner Area =================-->
        <section class="banner_area">
			<div class="video-wrapper" style="position:absolute;top:0px;left:0px;width:100%;overflow:hidden;height:600px">
				<!--<img src="img/consultant-banner.jpeg" style="min-width:100%;min-height:600px;height:600px"></img>-->
			</div>
            <div class="banner_inner d-flex align-items-center">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container" style="padding-top:150px;height:600px">
					<div class="banner_content text_center consultants-banner" style="width:500px;float:left">
						<h2>Our Story</h2><br/>
						<h3>
							ContractComplete is here to help engineers and architects save time &amp; money.<br/><br/>
							<b>Here's our story.</b>
						</h3>
					</div>
					<div   class="banner-image-section" style="max-width:50%;float:right;display:table-cell;vertical-align:middle;height:600px;padding-top:50px">
						<img src="img/Woman with laptop.png" style="border-radius:10px;margin-top: -60px;margin-left: -200px;box-shadow:none;-webkit-box-shadow:none;" class="home-banner-img top-banner-image"></img>
						<img src="img/open_quote.png" style="border-radius:10px;box-shadow:none;-webkit-box-shadow:none;margin-top: -700px;margin-left: 280px;" class="home-banner-img top-banner-image" />
						<img src="img/close_quote.png" style="border-radius:10px;box-shadow:none;-webkit-box-shadow:none;margin-top: -380px;" class="home-banner-img top-banner-image" />
						<div class="dot dot1" style="">
							&nbsp;
						</div>
					</div>
				</div>
            </div>
        </section>
		<div class="alert-banner" style="background-color:#0070bb">
			To help businesses during these challenging times, we are offering 3 months at 75% off.&nbsp;&nbsp;<a target="_blank" style="color:#C02942" href="https://connect.contractcomplete.com/75-percent-off"><u>Find out more</u></a>
		</div>
        
		<section class="made_life_area p_120" style="background-color:white">
			<div class="container" style="text-align:center">
				<div class="row made_life_inner company-info" style="text-align:center;background-color:#f4f7d5;">
					<h2>Our Mission</h2>
					Since 2015, ContractComplete’s mission has been to eliminate paper and paperwork for contract administrators in the private land development and infrastructure sector. From cost estimation to substantial completion, we improve processes by automating data entry, as well as document creation and distribution.<br/><br/>
					We want to make contract administration easier for our users so they can focus on other aspects of their business that generate revenue. We realize that during the construction season contract administrators get busier and busier as the season progresses. Calling bids, compiling bid reviews, answering bidder questions, keeping track of progress and changes gets exponentially harder as the workload increases.<br/><br/>
					After thousands of late nights and cups of coffee later, we’re proud to have built a product that helps contract administrators stay organized and on top of their paperwork.
				</div>
			</div>
		</section>
		
         <!--================Feature 2 Area =================-->
         <section class="made_life_area p_120" style="background-color:white;    font-weight: 400;color:black">
         	<div class="container" style="text-align:center">
         		<div class="row made_life_inner" style="text-align:center;background-color:#deecf6;margin-top:100px;">
					<div class="col-lg-1">
					
					</div>
					<div class="col-lg-4">
						<img class="headshot" src="img/about/dan_headshot.jpg">
						<div class="cofounder">Cofounder</div>
						<b class="employee-name">Daniel Moore</b>
						<b class="employee-title">President &amp; CTO</b>
						<p class="employee-description">
							Daniel completed his bacheleors of engineering &amp; management (in software engineering) at McMaster in 2011. He brings over 10 years of software development experience to the team, holding positions at BlackBerry, Ford Motor Company, and others.  He co-founded ContractComplete in 2015.  He is responsible for overseeing product development &amp; engineering.<br/><Br/>Outside of work, he enjoys golf, squash, and running.
						</p>
					</div>
					<div class="col-lg-2">
					
					</div>
					<div class="col-lg-4">
						<img class="headshot" src="img/about/sean_headshot.jpg">
						<div class="cofounder">Cofounder</div>
						<b class="employee-name">Sean Ciampaglia</b>
						<b class="employee-title">Cofounder &amp; Director of Marketing</b>
						<p class="employee-description">
							Sean has a Bachelor of Science in Mechanical Engineering from the University of Toronto. He has over 15 years experience as a general contractor actively managing projects in the land development industry. He realized in 2015 that there must be a better way to do contract administration in order to minimize mistakes and save contractors and consultants time on their paperwork. Check out <a target="_blank" href="https://www.contractcomplete.com/blog/what-inspired-me-to-co-found-a-software-company/">this blog post</a> about why he decided to help the industry.<br/><br/>
							He's a father, an entrepreneur and habitual crossfitter. He enjoys roasting coffee beans and uses the pour-over method to brew his daily cup.

						</p>
					</div>
					<div class="col-lg-1">
					
					</div>
         		</div>
         		<div class="row made_life_inner" style="text-align:center;background-color:#f4f7d5;margin-top:150px;">

					<div class="col-lg-4">
						<img class="headshot" src="img/about/mozhgan_headshot.jpg">
						<b class="employee-name">Mozhgan Baharloo</b>
						<b class="employee-title">Quality Lead</b>
						<p class="employee-description">
							Mozhgan joined ContractComplete in 2017 as our quality lead. She ensures users have a great experience while using the ContractComplete software.<br/><br/>
							She graduated from the Quality Assurance Software testing at IIBS (Innovations in Business Solutions) in 2016 and Information Technology Business Analysis at Conestoga College in 2017.<br/><br/>
							She also has a Bachelor of Applied Science Degree in Water Transmission Operation & Maintenance of Irrigation & Drainage Network, 2013, and an Associate Degree in Irrigation Technology, 2010.
						</p>
					</div>
					<div class="col-lg-4">
						<img class="headshot" src="img/about/alex_headshot.jpg">
						<b class="employee-name">Alex Aubry</b>
						<b class="employee-title">Business Development &amp; Customer Success</b>
						<p class="employee-description">
							Alex joined ContractComplete in 2018 to help grow sales and support our users. With a customer centric approach, Alex is constantly collecting user feedback to share with the development team in order to improve user experience.<br/><br/>
							Staying active and busy is the highest priority for Alex. On the squash court, hockey rink or golf course; this is where you will find him when he isn't working, exploring local history, brewing ales, or attempting art.

						</p>
					</div>
					<div class="col-lg-4">
						<img class="headshot" src="img/anon_avatar.png">
						<b class="employee-name">Mahdi Asadifard</b>
						<b class="employee-title">Full Stack Developer</b>
						<p class="employee-description">
							Mahdi joined ContractComplete in 2019 as more and more feedback was received and development requirements increased.  He is responsible for both client and server side development of ContractComplete.
						</p>
					</div>
					
				</div>
         		<div class="row made_life_inner" style="text-align:center;background-color:#deecf6;margin-top:150px;">

					<div class="col-lg-4">
					</div>
					<div class="col-lg-4">
						<img class="headshot" src="img/about/amber_headshot.jpg">
						<b class="employee-name">Amber Dunphy</b>
						<b class="employee-title">Business Development &amp; Sales</b>
						<p class="employee-description">
							Amber has recently joined ContractComplete as a Sales Representative. With over 10 years of sales experience, she has earned a reputation for building solid relationships with her customers based on trust and impeccable customer service.<br/><br/>Outside of work, you'll find Amber at the gym, chasing after her kids and dog, watching a true-crime documentary, or hosting a social gathering.

						</p>
					</div>
					<div class="col-lg-4">
					</div>
					
				</div>
			</div>
         </section>
         <!--================End Feature 2 Area =================-->
		 
		 <?php
			include 'inline_signup.html';
		 ?>

<?php 

include 'common_scripts.html';
include 'footer.php'; 

?>
		 
		 <script type="text/javascript">
			var w = window;
			var loadIframe = function(){
				var vidDefer = document.getElementsByTagName('iframe');
				for (var i=0; i<vidDefer.length; i++) {
					if(vidDefer[i].getAttribute('data-src')) {
						vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
					} 
				} 
			}
			
			if (w.addEventListener) { w.addEventListener("load", loadIframe, false); }
			else if (w.attachEvent) { w.attachEvent("onload",loadIframe); }
		 </script>
		 