<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
        <title>ContractComplete - Detailed Product Specifications</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="vendors/linericon/style.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
        <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="vendors/animate-css/animate.css">
        <!-- main css -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="vendors/jquery-ui/jquery-ui.css">
    </head>
    <body>
        
		<?php 
			include 'header.html';
		?>
        
        <!--================Home Banner Area =================-->
        <section class="banner_area large-screen-only">
			<div class="item" style="display:table-cell;text-align:center;vertical-align:middle;height:600px;width:100vw;padding-top: 65px;background-color: #f3f3f3;">
					<div class="carousel-padded-item-alt" style="display:inline-block;max-width:500px;margin-right:60px;text-align:left;padding-top:100px">
						<h1>DOCUMENTATION</h1>
						<div class="carousel-subheader">Step by step. From tendering to payment certificates.</div><br/>
						<a href="https://connect.contractcomplete.com/get-a-demo" class="tickets_btn" style="width: 200px">Request a Demo Instead</a>
					</div>				
					<img src="img/documentation.png" style="display:inline-block; max-width:500px;max-height:500px;margin-top:-140px"></img>
			</div>
        </section>
        <section class="banner_area small-screen-only" style="width:100%">
			<div class="item" style="display:table-cell;text-align:center;vertical-align:middle;height:600px;width:100%;padding-top:140px;background-color: #f3f3f3;">
					<div class="carousel-padded-item-alt" style="display:inline-block;max-width:100%;text-align:center;padding-top:100px">
						<img src="img/documentation.png" style="display:inline-block; max-width:80%;max-height:500px;margin-top:-140px"></img>
						<h1>DOCUMENTATION</h1>
						<div class="carousel-subheader" style="text-align:center">Step by step. From tendering to payment certificates.</div><br/>
						<a href="https://connect.contractcomplete.com/get-a-demo;" target="_blank" class="tickets_btn" style="width: 200px">Request a Demo Instead</a>
					</div>
			</div>
        </section>
        <!--================End Home Banner Area =================-->
        
        <!--================Work Area =================-->
        <section class="work_area p_120" style="padding:32px">
        	<div class="">
					<?php
						class related_document{
							public $name;
							public $src;
						};
					
						$page = 'home';
						if ( isset($_GET['page']) ) {
							$page = $_GET['page'];
						}
					?>
					
					<div class="work_inner docs-content" >
						<div class="" style="background-color:#edf6ff;padding: 32px;padding-right:64px">
							<h2>Documentation</h2>
							<ul>
								<li>
									<a href="documentation.php" class="<?php if($page == '') echo "selected-doc"; else echo ""; ?>">Home</a>
								</li>
								<li>
									<a href="?page=overview" class="<?php if($page == 'overview') echo "selected-doc"; else echo ""; ?>">ContractComplete Overview</a>
								</li>
								<li>
									<a href="?page=contract_setup" class="<?php if($page == 'contract_setup') echo "selected-doc"; else echo ""; ?>">Contract Setup</a>
									<ul>
										<li><a href="?page=followers" class="<?php if($page == 'followers') echo "selected-doc"; else echo ""; ?>">Followers & Action Items</a></li>
										<li><a href="?page=line_items" class="<?php if($page == 'line_items') echo "selected-doc"; else echo ""; ?>">Line Items</a></li>
										<li><a href="?page=provisional_items" class="<?php if($page == 'provisional_items') echo "selected-doc"; else echo ""; ?>">Provisional Items</a></li>
										<li><a href="?page=documents" class="<?php if($page == 'documents') echo "selected-doc"; else echo ""; ?>">Documents</a></li>
										<li><a href="?page=bidder_selection" class="<?php if($page == 'bidder_selection') echo "selected-doc"; else echo ""; ?>">Bidder/Contractor Selection</a></li>
										<li><a href="?page=importer" class="<?php if($page == 'importer') echo "selected-doc"; else echo ""; ?>">Importing from Excel</a></li>
									</ul>
								</li>
								<li>
									<a href="?page=tendering" class="<?php if($page == 'tendering') echo "selected-doc"; else echo ""; ?>">Tendering</a>
									<ul>
										<li><a href="?page=bidding" class="<?php if($page == 'bidding') echo "selected-doc"; else echo ""; ?>">Bidding on a Contract</a></li>
										<li><a href="?page=addenda" class="<?php if($page == 'addenda') echo "selected-doc"; else echo ""; ?>">Addendums & Changes</a></li>
									</ul>
								</li>
								<li>
									<a href="?page=awarding" class="<?php if($page == 'awarding') echo "selected-doc"; else echo ""; ?>">Awarding a Contract</a>
									<ul>
										<li><a href="?page=negotiations" class="<?php if($page == 'negotiations') echo "selected-doc"; else echo ""; ?>">Negotiations</a></li>
										<li><a href="?page=correcting_mistakes" class="<?php if($page == 'correcting_mistakes') echo "selected-doc"; else echo ""; ?>">Correcting Mistakes</a></li>
									</ul>
								</li>
								<li>
									<a href="?page=post_tender" class="<?php if($page == 'post_tender') echo "selected-doc"; else echo ""; ?>">Post Tender</a>
									<ul>
										<li><a href="?page=proposed_change" class="<?php if($page == 'proposed_change') echo "selected-doc"; else echo ""; ?>">Proposing Changes</a></li>
										<li><a href="?page=change_orders" class="<?php if($page == 'change_orders') echo "selected-doc"; else echo ""; ?>">Change Orders</a></li>
										<li><a href="?page=progress_reports" class="<?php if($page == 'progress_reports') echo "selected-doc"; else echo ""; ?>">Progress Reports</a></li>
										<li><a href="?page=invoicing" class="<?php if($page == 'invoicing') echo "selected-doc"; else echo ""; ?>">Invoicing</a></li>
										<li><a href="?page=payment_certificates" class="<?php if($page == 'payment_certificates') echo "selected-doc"; else echo ""; ?>">Payment Certificates</a></li>
									</ul>
								</li>
							</ul>
						</div>
						
						<div class="col-lg-4" style="padding:32px">
							<div class="breadcrumbs" style="display:none">
								<div class="inner">
									<span><a href="?page=home">Documentation Home&nbsp;&nbsp;</a></span><i class="fa fa-arrow-right breadcrumbs-spacer"></i>
									<?php 
										if($page == 'line_items'
											|| $page == 'followers'
											|| $page == 'provisional_items'
											|| $page == 'documents'
											|| $page == 'bidder_selection'
											|| $page == 'importer'){
											?>
												<span class="selected">Contract Setup&nbsp;&nbsp;</span>
											<?php
										}
										
										if($page == 'bidding' 
												|| $page == 'addenda'){
											?>
												<span class="selected">Tendering&nbsp;&nbsp;</span>
											<?php
										}
										
										if($page == 'negotiations'
											|| $page == 'correcting_mistakes'){
											?>
												<span class="selected">Awarding&nbsp;&nbsp;</span>
											<?php
										}
										
										if($page == 'proposed_change'
											|| $page == 'change_orders'
											|| $page == 'progress_reports'
											|| $page == 'invoicing'
											|| $page == 'payment_certificates'){
											?>
												<span class="selected">Post Tendering&nbsp;&nbsp;</span>
											<?php
										}
									?>
								</div>
							</div>
							</br><br/>
							<div class="docs-content">
								<?php
									$hasRelated = true;
									
									if($page == 'overview' || $page == 'bidding' || $page == 'home'){
										$hasRelated = false;
									}
									
									if ($hasRelated){
										?>
											<div class="related">
												<span class="contents-title">Related Articles</span><br/><br/>
													<?php
												
														if($page == 'contract_setup' 
															|| $page == 'followers' 
															|| $page == 'line_items' 
															|| $page == 'provisional_items' 
															|| $page == 'documents' 
															|| $page == 'bidder_selection'
															|| $page == 'importer'){
																?>
																	<ul>
																		<li <?php if($page=="followers") echo "style='display:none'"; ?>><a href="?page=followers">Followers & Action Items</a></li>
																		<li <?php if($page=="line_items") echo "style='display:none'"; ?>><a href="?page=line_items">Line Items</a></li>
																		<li <?php if($page=="provisional_items") echo "style='display:none'"; ?>><a href="?page=provisional_items">Provisional Items</a></li>
																		<li <?php if($page=="documents") echo "style='display:none'"; ?>><a href="?page=documents">Documents</a></li>
																		<li <?php if($page=="bidder_selection") echo "style='display:none'"; ?>><a href="?page=bidder_selection">Bidder/Contractor Selection</a></li>
																		<li <?php if($page=="importer") echo "style='display:none'"; ?>><a href="?page=importer">Importer Tool</a></li>
																	</ul>
																<?php
														}
													
														if($page == 'tendering' 
															|| $page == 'addenda'){
															?>
																<ul>
																	<li <?php if($page=="tendering") echo "style='display:none'"; ?>><a href="?page=tendering">Tendering</a></li>
																	<li <?php if($page=="addenda") echo "style='display:none'"; ?>><a href="?page=addenda">Issuing Addenda</a></li>
																</ul>
															<?php
														}
														
														if($page == 'awarding' 
															|| $page == 'negotiations'
															|| $page == 'correcting_mistakes'){
															?>
																<ul>
																	<li <?php if($page=="awarding") echo "style='display:none'"; ?>><a href="?page=awarding">Awarding a Contract</a></li>
																	<li <?php if($page=="negotiations") echo "style='display:none'"; ?>><a href="?page=negotiations">Negotiating</a></li>
																	<li <?php if($page=="correcting_mistakes") echo "style='display:none'"; ?>><a href="?page=correcting_mistakes">Correcting Mistakes</a></li>
																</ul>
															<?php
														}
													
														if($page == 'post_tender' 
															|| $page == 'proposed_change'
															|| $page == 'change_orders'
															|| $page == 'progress_reports'
															|| $page == 'invoicing'
															|| $page == 'payment_certificates'){
															?>
																<ul>
																	<li <?php if($page=="post_tender") echo "style='display:none'"; ?>><a href="?page=post_tender">Post Tender Home</a></li>
																	<li <?php if($page=="proposed_change") echo "style='display:none'"; ?>><a href="?page=proposed_change">Proposed Changes</a></li>
																	<li <?php if($page=="change_orders") echo "style='display:none'"; ?>><a href="?page=change_orders">Change Orders</a></li>
																	<li <?php if($page=="progress_reports") echo "style='display:none'"; ?>><a href="?page=progress_reports">Progress Reports</a></li>
																	<li <?php if($page=="invoicing") echo "style='display:none'"; ?>><a href="?page=invoicing">Invoicing</a></li>
																	<li <?php if($page=="payment_certificates") echo "style='display:none'"; ?>><a href="?page=payment_certificates">Payment Certificates</a></li>
																</ul>
															<?php
														}
													?>
											</div>
										<?php
									}
								?>
							</div>
						</div>
					</div>
					<div class="work_inner row docs-body" style="margin-bottom:0px;padding-left: 64px;">
						<div class="">
								<?php
									include 'docs/'.$page.'.html';
								?>
						</div>
					</div>
        		</div>
        	</div>
			<div style="clear:both"></div>
        </section>
        <!--================End Work Area =================-->
        
		<?php
			include 'signup-prompt.php';
        ?>
	   
		<?php
			include 'footer.php';
		?>
        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/stellar.js"></script>
        <script src="vendors/lightbox/simpleLightbox.min.js"></script>
        <script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="vendors/isotope/isotope-min.js"></script>
        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="js/mail-script.js"></script>
        <script src="vendors/counter-up/jquery.waypoints.min.js"></script>
        <script src="vendors/counter-up/jquery.counterup.min.js"></script>
        <script src="vendors/jquery-ui/jquery-ui.js"></script>
        <script src="js/theme.js"></script>
		<?php
			include 'common_scripts.html';
		?>
    </body>
</html>