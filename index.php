<!DOCTYPE html>
<html lang="en">
    <head>
        <title>ContractComplete - Bid Management and Contract Administration Software</title>
		<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="Fast, Streamlined Bidding for Unit Price Contracts. Administer your unit price contracts like a pro with streamlined bidding, addenda, change orders, RFIs, submittals, invoicing, and more!  See how we take the stress out of your day." >
        <link rel="icon" href="img/favicon.png" type="image/png">
		<link rel="stylesheet" href="css/font-awesome.min.css" />
		<link rel="stylesheet" href="vendors/iframe-lightbox-master/css/iframe-lightbox.min.css" />
		<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
		<?php
			include 'https_redirect.php';
			include 'css_common.php';
		?>
    </head>
    <body style="overflow-x:hidden">
	
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KQGP92Q"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

<?php
	/* Set up A/B Testing */
	$test_ver = null;
	if(!isset($_GET["ver"])){
		$test_ver = random_int (3,4);
	}else{
		$test_ver = $_GET["ver"];
	}
	
?>

<?php 
	$safari = true;
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')) {
		$safari=false;
	}
	include 'header.html'; 
?>
        
		
		
		<?php
			include 'signup-prompt.php';
        ?>

        <!--================Home Banner Area =================-->
        <section class="banner_area responsive-hero-banner" >
			<div class="video-wrapper hero-banner-inner" style="position:relative;top:0px;left:0px;width:100%;overflow:hidden;">
				<!--<img src="img/dozer.jpg" style="min-width:100%;min-height:600px;height:600px"></img>-->
				<img class="large-screen-only" alt="Construction site" src="img/construction-min.<?php echo ($safari ?  'webp' : 'jpeg'); ?>" style="min-width:100%;min-height:700px;height:700px" />
				<img class="small-screen-only hero-bg-img" alt="Construction site" src="img/construction-min_mobile.<?php echo ($safari ?  'webp' : 'jpeg'); ?>" style="min-width:100%;min-height:700px;height:700px" />
			</div>
            <div class="banner_inner d-flex align-items-center" style="margin-top:-560px;background:none">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container" style="padding-top:150px;height:560px">
					<div class="banner_content text_center consultants-banner" style="float:left">
						<div class="small-screen-only" style="width:100%;clear:both;height:25px">&nbsp;</div>
						<?php
							if($test_ver == 1){
								?> <h2><b>Got a Contract<br/>Management<br/>Headache?</b><br/> 
									<h3>
										Tired of managing your contracts using spreadsheets?  Feeling overworked and stressed out?  Let ContractComplete help.
									</h3>
								<?php
							}else if($test_ver == 2){
								?> <h2><b>Bidding and<br/>Contract Admin.<br/>Simplified.</b><br/> 
									<h3>
										Tired of managing your contracts using spreadsheets?  Feeling overworked and stressed out?  Let ContractComplete help.  This is what we do best.
									</h3>
								<?php
							}else{
								?> 
									<h2 class="small-screen-only" style="margin-top:-25px;font-size:36px"><b>Streamline<br/>the Unit<br/>Price Contract.</b></h2>
									<h2 class="large-screen-only" style="font-size: 48px;"><b>Streamline<br/>the Unit<br/>Price Contract.</b></h2><br/> 
									<h3>
										Take your unit price contracts to the next level. From bidding to change management and payment certification.
									</h3>
								<?php
							}
						?>
						<a href="javascript:window.scrollToDemoRequest()" class="tickets_btn_nav" style="margin-top:15px;">Get a Demo</a>
						<a href="https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy5jYXB0aXZhdGUuZm0vYnVpbGRvdXJmdXR1cmUv/episode/NDE0MmI2MjctNzI2OS00YWM4LWE0MWUtZDQ1YzczZWFjMmVj?sa=X&ved=0CAUQkfYCahcKEwjgpcPClbDvAhUAAAAAHQAAAAAQAg" target="_blank" class="tickets_btn_nav" style="margin-top:15px;background-color:#0aa89f">Hear Podcast</a>
						<div class="small-screen-only" style="width:100%;clear:both;height:100px">&nbsp;</div>
					</div>
					<div class="banner-image-section" style="width:500px;max-width:50%;float:right;display:table-cell;vertical-align:middle;height:700px;padding-top:70px">
						<!--<div style="position:relative;height:0;padding-bottom:56.22%">
							<iframe src="" data-src="https://www.youtube.com/embed/EK294ApQRJs?ecver=2&rel=0" style="position:absolute;width:100%;height:100%;left:0; border: 2px solid white;" 
								width="640" height="360" frameborder="0" allow="encrypted-media" allowfullscreen>
							</iframe>
						</div>
						-->
						<img class="multi-platform-img" alt="Laptop, desktop, and mobile phone with ContractComplete" src="img/MultiPlatform-min.<?php echo ($safari ?  'webp' : 'png'); ?>" />
						<a href="javascript:void(0);" class="iframe-lightbox-link" data-src="https://www.youtube.com/embed/fpWCxIOSuuk?autoplay=0&enablejsapi=1"
data-padding-bottom="56.25%" style="max-width: 200px;margin-top: -400px;margin-left: 100px;display:block">
							<img alt="Play button" src="img/play-button-overlay-png.png" onclick="window.logVideoClick()" class="play-btn-overlay" style="max-width:200px" />
						</a>
						<div class="rating top-banner-image">
							<div class="rating-stars" style="color:#e7ae4c">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
							<div>
								On Capterra <a target="_blank" href="https://www.capterra.com/p/166394/ContractComplete/"><i class="fa fa-location-arrow" style="font-size: 24px;color:#D95B43"></i></a>
							</div>
						</div>
					</div>
				</div>
            </div>
        </section>
		<div class="alert-banner">
			Onboarding is free and easy! Setup your first project today within 60 minutes.&nbsp;&nbsp;
			
		</div>
        <!--================End Home Banner Area =================-->
<?php
	if($test_ver == 4){
?>
<section class="made_life_area p_120" style="clear: both;padding-bottom:50px;text-align:center;background:white">
			<div class="accent-title" style="font-size:30px">
				Paperwork Sucks.  Start Automating it Today
			</div>
			<div style="clear:both;height:25px"></div>
			<img alt="Demo video thumbnail" src="img/thumbnail.<?php echo ($safari ?  'webp' : 'png'); ?>" style="width: 800px;max-width:90%;margin:auto" />
			<a href="javascript:void(0);" class="iframe-lightbox-link play-overlay-2" data-src="https://www.youtube.com/embed/fpWCxIOSuuk?autoplay=0&amp;enablejsapi=1" 
				style="max-width: 200px;margin-left: calc(50vw - 100px);display:block">
				<img alt="play button" src="img/play-button-overlay-png.png" onclick="window.logVideoClick()" style="max-width:200px">
			</a>
			<div class="large-screen-only" style="height:150px;clear:both"></div>
</section>
<?php
	}
?>
		
		<section class="made_life_area p_120" style="clear: both;padding-bottom:50px;text-align:center">
		
				<h2 class="stress-header" style="margin: 20px;">Introducing Contract Administration Mastery</h2>

				<!-- Flickity HTML init -->
			<div class="gallery js-flickity small-screen-only"
			  data-flickity-options='{ "wrapAround": true }'>
			  <div class="gallery-cell" style="width:100%">
					<table class="stress-table table-responsive" style="margin-top:40px;padding-left:20px;padding-right:20px">
						<tr class="img-row">
							<td style="text-align:center"><img alt="Budgeting graphic" src="img/asset1.png" /></td>
						</tr>
						<tr class="img-row">
							<td style="height: 100px;">&nbsp;</td>
						</tr>
						<tr class="title-row">
							<td style="padding-left:30px;padding-right:30px;text-align:center"><b>QUICKLY CREATE<br/>PROJECT BUDGETS</b></td>
						</tr>
						<tr class="descriptions-row">
							<td>Use historical pricing to quickly create project budgets.  Alternatively you can easily import your schedule of prices and provisional items from Excel.</td>
						</tr>
						<tr>
							<td>
								<a class="product-link" href="budgeting.php">See Features</a>
							</td>
						</tr>
					</table>
			  </div>
			  <div class="gallery-cell" style="width:100%">
					<table class="stress-table table-responsive" style="margin-top:40px;padding-left:20px;padding-right:20px">
						<tr class="img-row">
							<td style="text-align:center"><img  style= width: 200px;height: auto;alt="Bidding graphic" src="img/asset3.png" /></td>
						</tr>
						<tr class="img-row">
							<td style="height: 50px;">&nbsp;</td>
						</tr>
						<tr class="title-row">
							<td style="padding-left:30px;padding-right:30px;text-align:center"><b>STREAMLINE YOUR<br/>BIDDING PROCESS</b></td>
						</tr>
						<tr class="descriptions-row">
							<td>Send out your bid in minutes & measure bidder engagement.  Enjoy instant bid reviews.</td>
						</tr>
						<tr>
							<td>
								<a class="product-link" href="bidding.php">See Features</a>
							</td>
						</tr>
					</table>
			  </div>
			  <div class="gallery-cell" style="width:100%">
					<table class="stress-table table-responsive" style="margin-top:40px;padding-left:20px;padding-right:20px">
						<tr class="img-row">
							<td style="text-align:center"><img  alt="Contract admin graphic" src="img/asset4.png" /></td>
						</tr>
						<tr class="img-row">
							<td style="height: 50px;">&nbsp;</td>
						</tr>
						<tr class="title-row">
							<td style="padding-left:30px;padding-right:30px;text-align:center"><b>INSTANT RFIs, SUBMIT-<br/>TALS, & CHANGE ORDERS</b></td>
						</tr>
						<tr class="descriptions-row">
							<td>Collaborate with your contractor for all changes to the scope and contract.  Streamline RFIs and submittals within the platform.</td>
						</tr>
						<tr>
							<td>
								<a class="product-link" href="projectmanagement.php">See Features</a>
							</td>
						</tr>
					</table>
			  </div>
			  <div class="gallery-cell" style="width:100%">
					<table class="stress-table table-responsive" style="margin-top:40px;padding-left:20px;padding-right:20px">
						<tr class="img-row">
							<td style="text-align:center"><img  alt="Progress billing graphic" src="img/asset5.png" /></td>
						</tr>
						<tr class="img-row">
							<td style="height: 50px;">&nbsp;</td>
						</tr>
						<tr class="title-row">
							<td style="padding-left:30px;padding-right:30px;text-align:center"><b>PROGRESS BILLING<br/>MADE EASY</b></td>
						</tr>
						<tr class="descriptions-row">
							<td>As progress is reported, contractors send beautifully formatted invoices and payment certification is instant an error free.</td>
						</tr>
						<tr>
							<td>
								<a class="product-link" href="progressbilling.php">See Features/a>
							</td>
						</tr>
					</table>
			  </div>
			</div>
			<div style="text-align: center;margin: auto" class="large-screen-only">
				<table class="stress-table table-responsive" style="margin-top:0px;padding-left:100px;padding-right:100px">
					<tr class="img-row">
						<td><img  alt="Budgeting graphic" src="img/asset1.png" /></td>
						<td><img  alt="Bidding graphic" src="img/asset3.png" /></td>
						<td><img  alt="Project management graphic" src="img/asset4.png" /></td>
						<td><img  alt="Billing graphic" src="img/asset5.png" /></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr class="title-row">
						<td style="padding-left:30px;padding-right:30px"><b>QUICKLY CREATE<br/>PROJECT BUDGETS</b></td>
						<td><b>STREAMLINE YOUR<br/>BIDDING PROCESS</b></td>
						<td><b>INSTANT RFIs, SUBMIT-<br/>TALS, & CHANGE ORDERS</b></td>
						<td style="padding-left:30px;padding-right:30px"><b>PROGRESS BILLING<br/>MADE EASY</b></td>
					</tr>
					<tr class="descriptions-row" style="font-size: 19px">
						<td>Use historical pricing to quickly create project budgets.  Alternatively you can easily import your schedule of prices and provisional items from Excel.</td>
						<td>Send out your bid in minutes & measure bidder engagement.  Enjoy instant bid reviews.</td>
						<td>Collaborate with your contractor for all changes to the scope and contract.  Streamline RFIs and submittals within the platform.</td>
						<td>As progress is reported, contractors send beautifully formatted invoices and payment certification is instant an error free.</td>
					</tr>
					<tr>
						<td>
							<a class="product-link" href="budgeting.php">See Features</a>
						</td>
						<td>
							<a class="product-link" href="bidding.php">See Features</a>
						</td>
						<td>
							<a class="product-link" href="projectmanagement.php">See Features</a>
						</td>
						<td>
							<a class="product-link" href="progressbilling.php">See Features</a>
						</td>
					</tr>
				</table>
			</div>
		</section>
		
		<section class="made_life_area p_120" style="background-color:white;clear: both;padding-bottom:0px">
			<div style="text-align: center;margin: auto;border-bottom: none">
				<h2 style="font-size: 30px;margin: 20px;">Hundreds of Businesses are Saving with ContractComplete</h2>
				<div class="companies-container">
					<a class="user-company-link" target="_blank" href="https://www.aboudtng.com/"><img alt="Aboud & Associates" class="user-company-logo" src="img/users/aboud.<?php echo ($safari ?  'webp' : 'png'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="http://adessodesigninc.ca/"><img alt="Adesso" class="user-company-logo" src="img/users/adesso.<?php echo ($safari ?  'webp' : 'png'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://www.brantford.ca/en/index.aspx"><img alt="City of Brantford" class="user-company-logo" src="img/users/brantford.<?php echo ($safari ?  'webp' : 'gif'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://www.hamilton.ca/"><img alt="City of Hamilton" class="user-company-logo" src="img/users/city_of_hamilton-min.<?php echo ($safari ?  'webp' : 'jpg'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://www.stratford.ca/en/index.aspx"><img  alt="City of Stratford" class="user-company-logo" src="img/users/city_of_stratford.<?php echo ($safari ?  'webp' : 'svg'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://falcon.ca/"><img  alt="Falcon Engineering" class="user-company-logo" src="img/users/falcon.<?php echo ($safari ?  'webp' : 'png'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://fotenn.com/"><img  alt="Fotenn" class="user-company-logo" src="img/users/fotenn.<?php echo ($safari ?  'webp' : 'png'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://geoscapecontracting.com/"><img  alt="Geoscape Contracting" class="user-company-logo" src="img/users/geoscape.<?php echo ($safari ?  'webp' : 'jpg'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://hkla.ca/"><img  alt="HKLA" class="user-company-logo" src="img/users/hkla.<?php echo ($safari ?  'webp' : 'png'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://www.nakdesignstrategies.com/"><img  alt="NAK Design Strategies" class="user-company-logo" src="img/users/nak.<?php echo ($safari ?  'webp' : 'jpg'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://www.niagararegion.ca/"><img  alt="Niagara Region" class="user-company-logo" src="img/users/niagara_region.<?php echo ($safari ?  'webp' : 'jpg'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://www.strybos.com/"><img  alt="SBK Landscape Architects" class="user-company-logo" src="img/users/sbk.<?php echo ($safari ?  'webp' : 'jpg'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://www.sgm-inc.com/"><img  alt="SGM Inc." class="user-company-logo" src="img/users/sgm.<?php echo ($safari ?  'webp' : 'png'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://www.wsp.com/en-CA"><img  alt="WSP" class="user-company-logo" src="img/users/wsp.<?php echo ($safari ?  'webp' : 'png'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://www.shiftland.com/"><img  alt="SHIFT Landscape Architecture" class="user-company-logo" src="img/users/shift.<?php echo ($safari ?  'webp' : 'png'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="https://www.branthaven.com/"><img  alt="Branthaven Homes" class="user-company-logo" src="img/users/branthaven-homes.<?php echo ($safari ?  'webp' : 'jpg'); ?>" /></a>
					<a class="user-company-link" target="_blank" href="http://www.trinison.com/"><img  alt="Trinison" class="user-company-logo" src="img/users/trinison.<?php echo ($safari ?  'webp' : 'png'); ?>" /></a>
				</div>
			</div>
		</section>
	
		<div class="alert-banner" style="margin-top:64px;margin-bottom:64px">
			Jason from Colorado saved his civil engineering firm $5000 on his first 3 bids.  Get setup in under 60 minutes.<br/><a href="javascript:window.scrollToDemoRequest()"  class="tickets_btn_nav" style="margin-top:15px;">Get a Demo</a>
			
		</div>
		
        <section class="testimonials_area p_120" style="padding-bottom: 0px;">
        	<div class="mini-testimonial-block" style="width:80%;text-align: center;margin: auto;">
				<h2 style="color:#0070bb">What our customers are saying</h2>
				<div style="height:40px">&nbsp;</div>
        		<div class="small-testimonial">
					<img class="type-b" alt="adesso" src="img/testimonials/matt.<?php echo ($safari ?  'webp' : 'jpg'); ?>" />
					<img class="circle_arrow" alt="adesso" src="img/circle_arrow.<?php echo ($safari ?  'webp' : 'png'); ?>" />
					<p>
						"With ContractComplete the bid review spreadsheet is instantly created when the tender closes, saving me a whole day of work." <br/><a class="tickets_btn_nav" href="testimonials.php">See Testimonial</a>
					</p>
					<span style="font-size:18px; font-weight:550; color:#888888;">MATTHEW MADIGAN</span><br/>
					<span style="font-weight:550; color:#888888;"><em>Landscape Architect</em></span><br/>
					<span><a target="_blank" style="color:#888888; font-size:14px;" href="http://adessodesigninc.ca/">Adesso Design Inc.</a></span>
				</div>
        		<div class="small-testimonial">
					<img class="type-a" alt="stefanie" src="img/testimonials/stefanie.<?php echo ($safari ?  'webp' : 'jpeg'); ?>" />
					<img class="circle_arrow" alt="adesso" src="img/circle_arrow.<?php echo ($safari ?  'webp' : 'png'); ?>" />
					<p>
						"Everything is automated so that bidders can't submit unless it's compliant and the bid review is done instantly." <br/><a class="tickets_btn_nav" href="testimonials.php">See Testimonial</a>
					</p>
					<span style="font-size:18px; font-weight:750; color:#888888;">STEFANIE HOSGOOD</span><br/>
					<span style="font-weight:550; color:#888888;"><em>Reserves & Bidding Manager</em></span><br/>
					<span><a target="_blank" style="color:#888888; font-size:14px;" href="https://www.thefalcongroup.us/index.php">The Falcon Group</a></span>
				</div>
        		<div class="small-testimonial">
					<img class="type-a" alt="marc" src="img/testimonials/marc.<?php echo ($safari ?  'webp' : 'jpg'); ?>" />
					<img class="circle_arrow" alt="adesso" src="img/circle_arrow.<?php echo ($safari ?  'webp' : 'png'); ?>" />
					<p>
						"When the contractor and consultant are both using Contract Complete there is no duplication of effort into two systems." <br/><a class="tickets_btn_nav" href="testimonials.php">See Testimonial</a>
					</p>
					<span style="font-size:18px; font-weight:750; color:#888888;">MARK GARON-NIELSEN</span><br/>
					<span style="font-weight:550; color:#888888;"><em>Chief Executive Officer</em></span><br/>
					<span><a target="_blank" style="color:#888888; font-size:14px;" href="https://www.aboudtng.com/">Aboud & Associates</a></span>
				</div>
        	</div>
			<div style="clear:both">&nbsp;</div>
        </section>



		<div style="clear:both" id="webinar"></div>
		<div class="small-screen-only" style="height:50px"></div>
		<section class="made_life_area p_120 new-feature" style="background-color:white;clear: both;padding-bottom:0px">
			<div style="text-align: center;margin: auto;">
				<div style="width:100%;padding-top:90px;margin-top:-100px;padding-bottom: 30px;">
					<h2 style="color:#0070bb">Free Upcoming Webinars</h2>
					<div class="features-subheader webinar" style="text-align:center;max-width: 350px;margin: auto;padding:30px;clear: both;">
						<h3><img title="Consultants/Contract Administrators" class="webinar-icon" src="img/clipboard.svg" />Bid Calling 101</h3>
						<ul style="text-align:left">
							<li>Tender Preparation</li>
							<li>Line Items &amp; Prov. Items</li>
							<li>Documents</li>
						</ul>
						<script async id="demio-js" type="text/javascript" src="https://cdn.demio.com/production/dashboard/embed.bundle.js"></script> <span class="demio-embed-registration" data-hash="hu7fDHi5LDgr4SIB" data-api="api/v1" data-base-uri="https://my.demio.com/" data-popover="true" data-button-width="150" data-button-size="small" data-button-text="REGISTER" data-link-text="My Popover Link" data-color="#0070bb" data-text="REGISTER" ></span> 
					</div>
					<div class="features-subheader webinar" style="text-align:center;max-width: 350px;margin: auto;padding:30px;clear: both;">
						<h3><img class="webinar-icon" title="Consultants/Contract Administrators" src="img/clipboard.svg" />Bid Calling 201</h3>
						<ul style="text-align:left">
							<li>Automatic Addenda</li>
							<li>Negotiating Post-Tender</li>
							<li>Awarding a Contract</li>
						</ul>
						<script async id="demio-js" type="text/javascript" src="https://cdn.demio.com/production/dashboard/embed.bundle.js"></script> <span class="demio-embed-registration" data-hash="vRD6wJLdmvbyyRIh" data-api="api/v1" data-base-uri="https://my.demio.com/" data-popover="true" data-button-width="150" data-button-size="small" data-button-text="REGISTER" data-link-text="My Popover Link" data-color="#0070bb" data-text="REGISTER" ></span> 
					</div>
					<div class="features-subheader webinar" style="text-align:center;max-width: 350px;margin: auto;padding:30px;clear: both;">
						<h3><img src="img/hardhat.svg" title="Contractors" class="webinar-icon" /><img title="Consultants/Contract Administrators" class="webinar-icon" src="img/clipboard.svg" />Invoicing &amp Change Orders</h3>
						<ul style="text-align:left">
							<li>Creating an Invoice</li>
							<li>Proposing a Change</li>
							<li>Signed Change Orders</li>
						</ul>
						<script async id="demio-js" type="text/javascript" src="https://cdn.demio.com/production/dashboard/embed.bundle.js"></script> <span class="demio-embed-registration" data-hash="zRVmLB7A62PTt3IP" data-api="api/v1" data-base-uri="https://my.demio.com/" data-popover="true" data-button-width="150" data-button-size="small" data-button-text="REGISTER" data-link-text="My Popover Link" data-color="#0070bb" data-text="REGISTER" ></span> 
					</div>
					<div class="features-subheader webinar" style="text-align:center;max-width: 350px;margin: auto;padding:30px;clear: both;">
						<h3><img class="webinar-icon" title="Contractors" src="img/hardhat.svg" />Submit Your Bid Like a Pro</h3>
						<ul style="text-align:left">
							<li>Line Items</li>
							<li>Provisional Items</li>
							<li>Document Submissions</li>
						</ul>
						<script async id="demio-js" type="text/javascript" src="https://cdn.demio.com/production/dashboard/embed.bundle.js"></script> <span class="demio-embed-registration" data-hash="L96Y6io2juI7RuAB" data-api="api/v1" data-base-uri="https://my.demio.com/" data-popover="true" data-button-width="150" data-button-size="small" data-button-text="REGISTER" data-link-text="My Popover Link" data-color="#0070bb" data-text="REGISTER" ></span> 
					</div>
				</div>
			</div>
		</section>
			<div style="clear:both"></div>
		 
		 <?php
			include 'inline_signup.html';
		 ?>
		 
<div style="height:40px;width:100%">

</div>

<?php 

include 'common_scripts.html';
include 'footer.php'; 

?>
		 
		 
		 <script>
			var w = window;
			var loadIframe = function(){
				var vidDefer = document.getElementsByTagName('iframe');
				for (var i=0; i<vidDefer.length; i++) {
					if(vidDefer[i].getAttribute('data-src')) {
						vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
					} 
				} 
			}
			
			if (w.addEventListener) { w.addEventListener("load", loadIframe, false); }
			else if (w.attachEvent) { w.attachEvent("onload",loadIframe); }
			
			window.acceptVideo = function(){
				var email = $("#video-request-email").val();
				
				if(!window.validateEmail(email)){
					alert('Please provide a valid email address');
					return;
				}
				
				window.doVideoRequest({
					displayName: 'Sean Ciampaglia',
					fromEmailAddress: 'sean@contractcomplete.com',
					emailAddress: email,
					pipelineId: '1137200',
					pipelineStageId: '1137201',
					emailTemplateUrl: 'https://www.contractcomplete.com/tpl/bid_mistakes_email1.moustache.html',
					emailSubject: 'The Video you Requested'
				});
			}
			
			var lbEl = document.getElementsByClassName("iframe-lightbox-link")[0];
			lbEl.lightbox = new IframeLightbox(lbEl);
			
			var el2 = document.getElementsByClassName("iframe-lightbox-link")[1];
			if(el2){ // only for AB test version 4
				el2.lightbox = new IframeLightbox(el2);
			}
		 </script>
</html>
