<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
		<link rel="stylesheet" href="css/font-awesome.min.css" />
		<link rel="stylesheet" href="fonts/open_sans/stylesheet.css" />
        <title>ContractComplete - Project Management</title>
		<?php
			include 'https_redirect.php';
			include 'css_common.php';
		?>
    </head>
    <body>

<?php 
	$safari = true;
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')) {
		$safari=false;
	}
	include 'header.html'; 
?>
        
<div class="product-container shaded-bg" style="">	
	<span class="product-subtitle">Project Management Tools</span><br/>
	<span class="product-title">Streamlined RFIs, Submittals,<br/> and Change Events</span>
	<br/>
	<img src="img/project_mgmt_stack.png" style="" class="hero-image product-pdf-img borderless" />
	<!--
	<img src="img/pm_stack_rfi.png" style="max-width:800px;margin-bottom:300px;" class="product-pdf-img" />
	<img src="img/pm_stack_submittal.png" style="max-width:800px;margin-left: -500px;margin-top: 20px;" class="product-pdf-img" />
	<img src="img/pm_stack_changes.png" style="max-width:800px;margin-top: -300px;margin-left: 200px;margin-top: -600px;margin-left: 0px;" class="product-pdf-img" />
	-->
	<div style="clear:both;height:0px;"></div>
	<a class="book-demo" style="background-color:#0070BB" target="_blank" href="https://connect.contractcomplete.com/get-a-demo">Schedule a Demo</a>
</div>
<div class="product-container" style="margin-top:100px">	
	<div class="accent-title">Generate Beautiful<br/>Consistent Documents</div>
	<div class="divider-dot" style="background-color:#0aa89f">&nbsp</div>
	<div class="accent-title">Track Action Items<br/> &amp; Keep Projects<br/>on Schedule</div>
	<div class="divider-dot" style="background-color:#0070BB">&nbsp</div>
	<div class="accent-title">Keep All Project<br/>Files Organized</div><br/>
	<!--
	<img src="img/pm_stack_rfi.png" style="max-width:45%;margin-top:-80px;margin-left:-250px" class="product-pdf-img" />
	<img src="img/rfi_response.jpg" style="max-width: 600px;margin-left: -750px;margin-top: 450px;" class="product-pdf-img" />
	-->
	<img src="img/rfi_stack.png" style="" class="product-pdf-img borderless" />
	<div class="image-caption-wrapper" style="float:right">
		<span class="feature-title">Collaborative</span>
		<div class="accent-title-larger">RFIs the Easy Way</div>
		<p>
			Contractors generate the RFIs, you simply<br/>respond to them. We keep you organized.
		</p>
	</div>
	<div style="clear:both;height:1px"></div>
	<img src="img/features/consultant-3_raw.png" style="" class="small-screen-only product-pdf-img borderless" />
	<div class="image-caption-wrapper" style="float:left;margin-left:25px;text-align:right">
	
		<div class="caption-nested-div">
			<span class="feature-title" style="color:#0aa89f">Fast &amp; Convenient</span>
			<div class="accent-title-larger">Change Orders <Br/>on the Go</div>
			<p>
				From Proposed Changes, CCNs, CCOs<br/>&amp; Change Orders, stay on top of them.
				<br/>Easily approve and sign documents<br/>in the field on your mobile device.
			</p>
		</div>
	</div>
	<img src="img/features/consultant-3_raw.png" style="" class="large-screen-only product-pdf-img borderless" />
	<div style="clear:both;height:80px"></div>

	<?php
		include 'testimonial_adesso2.html'
	?>
	<div style="clear:both;"></div>
	<!--
	<img src="img/pm_stack_submittal.png" style="max-width:700px;margin-top:-200px" class="product-pdf-img" />
	<img src="img/submittal_response_pdf.jpg" style="max-width:400px; margin-left: -500px; margin-top: 200px;" class="product-pdf-img" />
	-->
	<img src="img/submittal_stack.png" style="" class="product-pdf-img borderless" />
	<div class="image-caption-wrapper" style="float:right">
		<span class="feature-title">Powerfully Simple</span>
		<div class="accent-title-larger">Submittals<br/>Centralized</div>
		<p>
			Receive Submittals & easily forward them<br/>
			to sub-consultants for their review. Ensure<br/>
			submittal delays don't become schedule<br/>delays.
		</p>
	</div>

	<div style="clear:both;height:80px"></div>
	<img src="img/mobile_contracts.png" style="max-height: 600px" class="small-screen-only product-pdf-img borderless" />
	<div class="image-caption-wrapper" style="float:left;text-align:right">
		<div class="caption-nested-div">
			<span class="feature-title" style="color:#0aa89f">Mobile Enabled</span>
			<div class="accent-title-larger">Apps Available<Br/>for iOS and Android</div>
			<p>
				Don't miss a beat, even in the field.
			</p>
		</div>
	</div>
	<img src="img/mobile_contracts.png" style="max-height: 600px" class="large-screen-only product-pdf-img borderless" />
	<div style="clear:both;height:80px"></div>
	
	<span class="accent-title-larger">See Other Solutions</span>
	<div style="clear:both;height:60px"></div>
	<div style="clear:both"></div>
	<a href="budgeting.php"><div class="accent-title">Budget Creation</div></a>
	<div class="divider-dot" style="background-color:#0aa89f">&nbsp</div>
	<a href="bidding.php"><div class="accent-title">Bidding Management</div></a>
	<div class="divider-dot" style="background-color:#0070BB">&nbsp</div>
	<a href="progressbilling.php"><div class="accent-title">Progress Billing</div></a><br/>
	
	<div style="clear:both;height:80px"></div>

	<?php
		include 'schedule_demo2.html'
	?>
</div>
<?php 
include 'footer.php'; 
include 'common_scripts.html';
?>
		 
		 <script type="text/javascript">
			var w = window;
			var loadIframe = function(){
				var vidDefer = document.getElementsByTagName('iframe');
				for (var i=0; i<vidDefer.length; i++) {
					if(vidDefer[i].getAttribute('data-src')) {
						vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
					} 
				} 
			}
			
			if (w.addEventListener) { w.addEventListener("load", loadIframe, false); }
			else if (w.attachEvent) { w.attachEvent("onload",loadIframe); }
		 </script>
		 