<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
		<link rel="stylesheet" href="css/font-awesome.min.css" />
		<link rel="stylesheet" href="fonts/open_sans/stylesheet.css" />
        <title>ContractComplete - Bid Management</title>
		<?php
			include 'https_redirect.php';
			include 'css_common.php';
		?>
    </head>
    <body>

<?php 
	$safari = true;
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')) {
		$safari=false;
	}
	include 'header.html'; 
?>
        
<div class="product-container shaded-bg" style="">	
	<span class="product-subtitle">Procurement for Unit Price Contracts</span><br/>
	<span class="product-title">An Efficient &amp; Fair Bidding<br/> Process for All</span>
	<br/>
	<img src="img/bid_comparison.png" class="hero-image product-pdf-img" /><br/>
	<a class="book-demo" style="background-color:#0070BB" target="_blank" href="https://connect.contractcomplete.com/get-a-demo">Schedule a Demo</a>
</div>
<div class="product-container" style="margin-top:100px">	
	<div class="accent-title">Increase Compliant<br/>Bids</div>
	<div class="divider-dot" style="background-color:#0aa89f">&nbsp</div>
	<div class="accent-title">Reduce Bid<br/>Leveling Time</div>
	<div class="divider-dot" style="background-color:#0070BB">&nbsp</div>
	<div class="accent-title">Assess Bidder<br/>Engagement</div>
	<div class="divider-dot" style="background-color:#707070">&nbsp</div>
	<div class="accent-title">Instant<br/>Addenda</div><br/>
	<img src="img/engagement.png" class="product-pdf-img" />
	<div class="image-caption-wrapper" style="float: right;">
		<span class="feature-title">Get More Bids</span>
		<div class="accent-title-larger">Instantly Assess<Br/>Bidder Engagement</div>
		<p>
			Know whether or not bidders have registered<br/>to bid, downloaded the project files or if they've<br/>acknowledged addenda.
		</p>
	</div>
	<div style="clear:both;height:1px"></div>
	
	<img src="img/bidding_stack.png" class="product-pdf-img borderless small-screen-only" />
	<div class="image-caption-wrapper" style="padding-right:64px;display:inline-block">
		<span class="feature-title" style="color:#0aa89f">Save Time</span>
		<div class="accent-title-larger">Increase<Br/>Compliant Bids</div>
		<p>
			You have the option to only allow 100%<br/>complete and compliant bids. Bidders<br/>
			aren't able to change the bid form or edit<br/>the units for their convenience.
			<br/><br/>Make bidders submit, your way.
		</p>
	</div>
	<!--
	<div style="display: inline-block;">
		<img src="img/checklist.png" style="max-width: 600px;display:inline-block;margin-top:-250px" class="product-pdf-img" />
		<img src="img/incomplete.png" style="max-width: 450px;display:inline-block;margin-top:170px;margin-left: -400px;" class="product-pdf-img" />
	</div>
	-->
	<img src="img/bidding_stack.png" class="product-pdf-img borderless large-screen-only" />
	<div style="clear:both;height:80px"></div>

	<?php
		include 'testimonials_falcon.html'
	?>
	<div style="clear:both;height:1px"></div>
	<img src="img/addendum.png" class="product-pdf-img" />
	<div class="image-caption-wrapper" style="float:right;">
		<span class="feature-title">Centralize Bidder Questions</span>
		<div class="accent-title-larger">Instant Addenda</div>
		<p>
			Last minute changes happen. It's ok.<br/><br/>
			During the bidding process, easily compile all<br/>
			changes of scope &amp; bidder Q&amp;A, then generate<br/>
			and distribute addenda in one click.
		</p>
	</div>

	<div style="clear:both;height:1px"></div>
	<img src="img/comparison_pdf.png"  class="product-pdf-img small-screen-only" />
	<div class="image-caption-wrapper" style="padding-right:64px;display:inline-block">
		<span class="feature-title" style="color:#0aa89f">Save Hours per Bid Review</span>
		<div class="accent-title-larger">Instant<Br/>Bid Reviews</div>
		<p>
			Review bids moments after the close,
			<br/>line by line in beautifully generated PDFs
			<br/>or Excel Exports.<br/><br/>Eliminate spreadsheet data entry.
		</p>
	</div>
	<img src="img/comparison_pdf.png" style="float:right" class="product-pdf-img large-screen-only" />
	<div style="clear:both;height:120px"></div>
	<span class="accent-title-larger">See Other Solutions</span>
	<div style="clear:both;height:60px"></div>
	<div style="clear:both"></div>
	<a href="projectmanagement.php"><div class="accent-title">Project Management</div></a>
	<div class="divider-dot" style="background-color:#0aa89f">&nbsp</div>
	<a href="budgeting.php"><div class="accent-title">Budget Creation</div>
	<div class="divider-dot" style="background-color:#0070BB">&nbsp</div>
	<a href="progressbilling.php"><div class="accent-title">Progress Billing</div><br/>
	
	<div style="clear:both;height:80px"></div>
	<?php
		include 'schedule_demo2.html'
	?>
</div>
<?php 
include 'footer.php'; 
include 'common_scripts.html';
?>
		 
		 <script type="text/javascript">
			var w = window;
			var loadIframe = function(){
				var vidDefer = document.getElementsByTagName('iframe');
				for (var i=0; i<vidDefer.length; i++) {
					if(vidDefer[i].getAttribute('data-src')) {
						vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
					} 
				} 
			}
			
			if (w.addEventListener) { w.addEventListener("load", loadIframe, false); }
			else if (w.attachEvent) { w.attachEvent("onload",loadIframe); }
		 </script>
		 